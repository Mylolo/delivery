package com.localvalu.pool.admin.data.model.response

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails
import com.localvalu.deliveryorder.data.model.response.driverdashboard.PoolDriversResult

data class GetPoolDriverListResult(@SerializedName("GetpooldriverlistResult") val poolDriversResult: PoolDriversResult)
{

}