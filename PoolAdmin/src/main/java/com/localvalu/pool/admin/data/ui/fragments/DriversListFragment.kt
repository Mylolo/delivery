package com.localvalu.pool.admin.data.ui.fragments

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.localvalu.deliverycore.constants.Constants
import com.localvalu.deliverycore.constants.Constants.STATUS_ORDER_RECEIVED_FROM_MERCHANT
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.pool.admin.R
import com.localvalu.pool.admin.data.di.driverslist.DriversListDH
import com.localvalu.pool.admin.data.model.response.PoolDriverListResponse
import com.localvalu.pool.admin.data.model.response.PoolDriversData
import kotlinx.android.synthetic.main.fragment_drivers_list.*
import kotlinx.android.synthetic.main.toolbar_driver_list.*
import java.io.IOException
import javax.inject.Inject

class DriversListFragment : Fragment(), DriversListAdapter.DriverItemClickListener
{

    private val TAG =  "DLF"

    private val B_DRIVERS_LIST = "B_DRIVERS_LIST"

    private val component by lazy { DriversListDH.driversListComponent() }

    @Inject
    lateinit var viewModelFactory: DriversListViewModelFactory

    @Inject
    lateinit var adapter: DriversListAdapter

    @Inject
    lateinit var contextTool: Context

    private val viewModel: DriversListViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(DriversListViewModel::class.java)
    }

    private var poolName = ""

    private var listShown = false;

    private lateinit var driversListGlobal: MutableList<PoolDriversData>

    private lateinit var selectedDriver: PoolDriversData

    private var selectedPosition: Int = -1


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setUpResultListener()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_drivers_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        adapter.driverItemClickListener = this
        rvDriversList.layoutManager = LinearLayoutManager(context);
        rvDriversList.adapter = adapter
        btnRetry.setOnClickListener {
            viewModel.driversList(poolName)
        }
        observeDriversListDataListener();
        Log.d(TAG, "arguments ")
        arguments?.getString(BundleUtils.BUNDLE_POOL_NAME)?.let {
            poolName = it
            viewModel.driversList(poolName)
        }
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("driversListFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),
            R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_drivers)
        toolbar.title=""
    }

    private fun setUpResultListener()
    {
        setFragmentResultListener(BundleUtils.KEY_ORDER_DECLINED_REQUEST) { requestKey, result ->
            onFragmentResult(requestKey, result)
        }
    }

    private fun onFragmentResult(requestKey: String, result: Bundle)
    {
        val orderDeclined = result.getBoolean(BundleUtils.BUNDLE_ORDER_DECLINED);
        if (orderDeclined)
        {
            //viewModel.driverDashboardOutcome.removeObservers(viewLifecycleOwner)
            //initiateDriverDashboardDataListener()
            //viewModel.driverDashboard(poolName)
        }
    }

    private fun observeDriversListDataListener()
    {
        viewModel.driversListOutcome.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.driversListOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<PoolDriverListResponse>> { outcome ->
                Log.d(TAG, "observeDriversListDataListener: $outcome")
                when (outcome)
                {
                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            showProgress()
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "observeDriversListDataListener: Successfully loaded data")
                        if (outcome.data.getPoolDriverListResult.poolDriversResult.errorDetails.errorCode == 0)
                        {
                            setList(outcome.data.getPoolDriverListResult.poolDriversResult.poolDriversList);
                        }
                        else
                        {
                            //showErrorMessage(outcome.data.driverDashboardResult.errorDetails.errorMessage,false,1)
                            showInitialError(
                                outcome.data.getPoolDriverListResult.poolDriversResult.errorDetails.errorMessage,
                                false
                            );
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            //showErrorMessage(getString(R.string.msg_no_internet_connection),true,1)
                            showInitialError(getString(R.string.msg_no_internet_connection), true);
                        }
                        else
                        {
                            //showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,1)
                            showInitialError(getString(R.string.msg_unknown_error_try_again), true);
                        }
                    }
                }
            })
    }

    private fun showProgress()
    {
        rvDriversList.visibility = View.GONE
        progressBarFragmentDriversList.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
    }

    private fun setList(driversList: List<PoolDriversData>)
    {
        driversListGlobal = driversList as MutableList<PoolDriversData>
        adapter.swapData(driversListGlobal)
        showList()
    }

    private fun showList()
    {
        listShown = true;
        rvDriversList.visibility = View.VISIBLE
        progressBarFragmentDriversList.visibility = View.GONE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialError(message: String, retry: Boolean)
    {
        progressBarFragmentDriversList.visibility = View.GONE
        errorContainer.visibility = View.VISIBLE
        rvDriversList.visibility = View.GONE
        tvError.text = message;
        if (retry) btnRetry.visibility = View.VISIBLE
        else btnRetry.visibility = View.GONE
    }

    private fun showErrorMessage(message: String, retry: Boolean, apiResponseFrom: Int)
    {
        val mySnackbar = Snackbar.make(
            clFragmentOrderList,
            message, Snackbar.LENGTH_INDEFINITE
        )
        mySnackbar.setBackgroundTint(
            ContextCompat.getColor(
                contextTool,
                R.color.colorPrimaryDark
            )
        )
        if (retry) mySnackbar.setAction(R.string.lbl_retry, View.OnClickListener { v ->
            when (apiResponseFrom)
            {
                1 -> viewModel.driversList(poolName)
            }
            mySnackbar.dismiss()
        })
        else mySnackbar.setAction(R.string.lbl_ok, View.OnClickListener { v ->
            mySnackbar.dismiss()
        })
        mySnackbar.show()
        selectedDriver = PoolDriversData()
        selectedDriver?.let {
            //it.performActionRequest = false
        }
    }

    override fun itemClicked(poolDriversData: PoolDriversData, position: Int)
    {

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.driver_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item!!.itemId
        //handle item clicks
        if (id == R.id.refresh)
        {
            viewModel.driversList(poolName)
        }
        return super.onOptionsItemSelected(item)
    }

}



