package com.localvalu.pool.admin.data.driverslist

import com.localvalu.pool.admin.data.DriversListDataContract
import com.localvalu.pool.admin.data.model.request.DriversListRequest
import io.reactivex.Single

class DriversListRemoteData(private val driversListService: DriversListService) : DriversListDataContract.Remote
{
    override fun driversList(driversListRequest: DriversListRequest)=
        driversListService.driversList(driversListRequest)
}