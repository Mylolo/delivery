package com.localvalu.pool.admin.data

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.pool.admin.data.model.request.DriversListRequest
import com.localvalu.pool.admin.data.model.response.PoolDriverListResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface DriversListDataContract
{
    interface Repository
    {
        //Drivers List
        val driversListFetchOutcome: PublishSubject<Outcome<PoolDriverListResponse>>
        fun getRemoteDriversList(driversListRequest: DriversListRequest)
        fun handleDriversListError(error: Throwable)
    }

    interface Remote
    {
        fun driversList(driversListRequest: DriversListRequest): Single<PoolDriverListResponse>
    }
}