package com.localvalu.pool.admin.data.di.driverslist

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object DriversListDH
{
    private var driversListComponent:DriversListComponent? = null

    fun driversListComponent(): DriversListComponent
    {
        if (driversListComponent == null)
            driversListComponent =
                DaggerDriversListComponent.builder().coreComponent(CoreApp.coreComponent)
                    .build()
        return driversListComponent as DriversListComponent
    }

    fun destroyDriversListComponent()
    {
        driversListComponent = null
    }
}