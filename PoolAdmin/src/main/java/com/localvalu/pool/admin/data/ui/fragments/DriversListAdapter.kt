package com.localvalu.pool.admin.data.ui.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.deliverycore.constants.Constants
import com.localvalu.pool.admin.R
import com.localvalu.pool.admin.data.model.response.PoolDriversData
import kotlinx.android.synthetic.main.view_item_drivers.view.*


class DriversListAdapter() :
    ListAdapter<PoolDriversData, DriversListAdapter.DriverListViewHolder>(PoolDriversListListDC())
{
    var driverItemClickListener: DriverItemClickListener? = null

    private lateinit var context: Context;

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context;

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverListViewHolder =
        DriverListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_item_drivers,
                parent, false
            ), driverItemClickListener
        )

    override fun onBindViewHolder(holder: DriverListViewHolder, position: Int) =
        holder.bind(getItem(position))


    inner class DriverListViewHolder(
        itemView: View,
        private val driverItemClickListener: DriverItemClickListener?
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        private lateinit var tempPoolDriversData: PoolDriversData;

        init
        {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?)
        {
            val clicked = getItem(adapterPosition)
            driverItemClickListener?.itemClicked(clicked,adapterPosition)
        }

        fun bind(poolDriversData: PoolDriversData) = with(itemView) {
            tempPoolDriversData = poolDriversData
            itemView.tvDriverName.text = poolDriversData.driverName.capitalize()
            val strShortName = StringBuilder()
            if(poolDriversData.driverName.contains(" "))
            {
                val names = poolDriversData.driverName.split(" ")
                if(names!=null)
                {
                    if(names.isNotEmpty())
                    {
                        if(names.size>1)
                        {
                            strShortName.append(names[0][0].toUpperCase()).append(names[1][0].toUpperCase())
                        }
                        else
                        {
                            strShortName.append(names[0][0].toUpperCase())
                        }
                    }
                    else
                    {
                        strShortName.append(names[0][0].toUpperCase())
                    }
                }
                else
                {
                    strShortName.append(poolDriversData.driverName[0].toUpperCase())
                }
            }
            else
            {
                strShortName.append(poolDriversData.driverName[0].toUpperCase())
            }
            itemView.tvDriverShortName.text = strShortName.toString()
        }
    }

    fun swapData(data: List<PoolDriversData>)
    {
        submitList(data.toMutableList())
    }

    interface DriverItemClickListener
    {
        fun itemClicked(PoolDriversData: PoolDriversData, position: Int)
    }

    private class PoolDriversListListDC : DiffUtil.ItemCallback<PoolDriversData>()
    {
        override fun areItemsTheSame(oldItem: PoolDriversData, newItem: PoolDriversData) =
            oldItem.driverId == newItem.driverId

        override fun areContentsTheSame(oldItem: PoolDriversData, newItem: PoolDriversData) =
            oldItem == newItem
    }

}