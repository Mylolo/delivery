package com.localvalu.pool.admin.data.model.response

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryorder.data.model.response.driverdashboard.PoolDriversResult

data class PoolDriverListResponse(@SerializedName("DriverAppResult") val getPoolDriverListResult: GetPoolDriverListResult)
{

}