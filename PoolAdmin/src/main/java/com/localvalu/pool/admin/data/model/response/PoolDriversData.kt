package com.localvalu.pool.admin.data.model.response

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PoolDriversData(@SerializedName("bankAccountdetails") val bankAccountdetails: String="",
                           @SerializedName("deliveryfee") val deliveryfee: String="",
                           @SerializedName("driverId") val driverId: String="",
                           @SerializedName("driverName") val driverName: String="",
                           @SerializedName("poolName") val poolName: String="",
                           @SerializedName("postCode") val postCode: String=""):Parcelable
{


}

