package com.localvalu.pool.admin.data.ui.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.pool.admin.data.DriversListDataContract
import com.localvalu.pool.admin.data.di.driverslist.DriversListDH
import com.localvalu.pool.admin.data.model.request.DriversListRequest
import com.localvalu.pool.admin.data.model.response.PoolDriverListResponse
import io.reactivex.disposables.CompositeDisposable

class DriversListViewModel(
    private val driversListRepository: DriversListDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    //Drivers List
    val driversListOutcome: LiveData<Outcome<PoolDriverListResponse>> by lazy {
        //Convert publish subject to livedata
        driversListRepository.driversListFetchOutcome.toLiveData(compositeDisposable)
    }

    fun driversList(poolName: String)
    {
        driversListRepository.getRemoteDriversList(DriversListRequest("",poolName))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        DriversListDH.destroyDriversListComponent()
    }
}