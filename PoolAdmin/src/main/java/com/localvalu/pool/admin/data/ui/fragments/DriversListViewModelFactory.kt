package com.localvalu.pool.admin.data.ui.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.pool.admin.data.DriversListDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ForgetPasswordViewModel.
 * Required given ForgetPasswordViewModel has a non-empty constructor
 */
class DriversListViewModelFactory(
    private val repository: DriversListDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return DriversListViewModel(
            repository,
            compositeDisposable
        ) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
