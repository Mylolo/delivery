package com.localvalu.pool.admin.data.di.driverslist

import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.pool.admin.data.DriversListDataContract
import com.localvalu.pool.admin.data.driverslist.DriversListRemoteData
import com.localvalu.pool.admin.data.driverslist.DriversListRepository
import com.localvalu.pool.admin.data.driverslist.DriversListService
import com.localvalu.pool.admin.data.ui.fragments.DriversListAdapter
import com.localvalu.pool.admin.data.ui.fragments.DriversListViewModelFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class DriversListModule
{
    /*ViewModel*/
    @Provides
    @DriversListScope
    fun driversListViewModelFactory(repository: DriversListDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            DriversListViewModelFactory = DriversListViewModelFactory( repository, compositeDisposable)

    /*Repository*/
    @Provides
    @DriversListScope
    fun driversListRepo(remote: DriversListDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            DriversListDataContract.Repository = DriversListRepository( remote, scheduler, compositeDisposable)

    @Provides
    @DriversListScope
    fun driversListRemoteData(driversListService: DriversListService):
            DriversListDataContract.Remote = DriversListRemoteData(driversListService)

    @Provides
    @DriversListScope
    fun acceptedOrderListService(@Named("DotNetServer")retrofit: Retrofit): DriversListService  = retrofit.create(DriversListService::class.java)

    /*Adapter*/
    @Provides
    @DriversListScope
    fun adapter(): DriversListAdapter = DriversListAdapter()

    @Provides
    @DriversListScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}