package com.localvalu.pool.admin.data.di.driverslist

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.pool.admin.data.ui.fragments.DriversListFragment
import com.squareup.picasso.Picasso
import dagger.Component

@DriversListScope
@Component(dependencies = [CoreComponent::class],modules = [DriversListModule::class])
interface DriversListComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun inject(driversListFragment: DriversListFragment)

}

