package com.localvalu.pool.admin.data.driverslist

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.pool.admin.data.DriversListDataContract
import com.localvalu.pool.admin.data.model.request.DriversListRequest
import com.localvalu.pool.admin.data.model.response.PoolDriverListResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests drivers list data
 *
 */

class DriversListRepository(
    private val remote: DriversListDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : DriversListDataContract.Repository
{
    //Drivers List
    override val driversListFetchOutcome: PublishSubject<Outcome<PoolDriverListResponse>> =
        PublishSubject.create<Outcome<PoolDriverListResponse>>()

    override fun getRemoteDriversList(driversListRequest: DriversListRequest)
    {
        driversListFetchOutcome.loading(true)
        Single.wrap(remote.driversList(driversListRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({poolDriversListResponse ->  driversListFetchOutcome.success(poolDriversListResponse)},
                { error -> handleDriversListError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleDriversListError(error: Throwable)
    {
        driversListFetchOutcome.failed(error)
    }
}

