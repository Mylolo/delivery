package com.localvalu.pool.admin.data.driverslist

import com.localvalu.pool.admin.data.model.request.DriversListRequest
import com.localvalu.pool.admin.data.model.response.PoolDriverListResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface DriversListService
{
    @POST("GetDriverpoolList")
    fun driversList(@Body driversListRequest: DriversListRequest): Single<PoolDriverListResponse>
}
