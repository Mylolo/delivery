package com.localvalu.pool.admin.data.di.driverslist

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DriversListScope