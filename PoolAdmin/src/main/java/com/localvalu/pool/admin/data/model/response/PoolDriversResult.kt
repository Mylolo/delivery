package com.localvalu.deliveryorder.data.model.response.driverdashboard

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcel
import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails
import com.localvalu.pool.admin.data.model.response.PoolDriversData
import kotlinx.android.parcel.Parcelize


data class PoolDriversResult(@SerializedName("poolDriverlist") val poolDriversList:List<PoolDriversData>,
                             @SerializedName("ErrorDetails") val errorDetails: ErrorDetails)
{


}

