package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliveryorder.data.model.request.CompleteDeliveryRequest
import com.localvalu.deliveryorder.data.model.response.deliverycomplete.CompleteDeliveryResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import javax.inject.Named

interface CompletedDeliveryService
{
    @POST("MerchantAppV2/CompleteStatus")
    fun completeTheDelivery(@Body completeDeliveryRequest: CompleteDeliveryRequest): Single<CompleteDeliveryResponse>
}
