package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData

data class DriverDashboardResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                 @SerializedName("DriverDashboard")  val driverDashboardList: List<DriverDashboardData>)

{

}