package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class OrderStatusChangeRequest(@SerializedName("Token") var token: String,
                               @SerializedName("Driverid") val driverId: Int,
                               @SerializedName("OrderId") val orderId: Int,
                               @SerializedName("Statusid") val statusId: Int,
                               @SerializedName("Merchantid") val merchantId: Int)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}