package com.localvalu.deliveryorder.data.model.response.fullorderdetails

import com.google.gson.annotations.SerializedName

data class OrderItemDetails(@SerializedName("OrderDetailsId") val orderDetailsId: String,
                               @SerializedName("item_id")  val itemId: String,
                               @SerializedName("item_name")  val itemName: String,
                               @SerializedName("quantity")  val quantity: String,
                               @SerializedName("single_quantity_price")  val singleQuantityPrice: String,
                               @SerializedName("item_total_price")  val itemTotalPrice: String,
                               @SerializedName("order_id")  val orderId: String,
                               @SerializedName("createdDate")  val createdDate: String,
                               @SerializedName("isActive")  val isActive: String)

{

}