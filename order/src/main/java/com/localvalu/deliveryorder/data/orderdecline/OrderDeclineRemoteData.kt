package com.localvalu.deliveryorder.data.orderdecline

import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne

class OrderDeclineRemoteData(private val orderDeclineService:OrderDeclineService) : OrderDeclineDataContract.Remote
{
    override fun declineOrder(orderAcceptRequestOne: OrderAcceptRequestOne)= orderDeclineService.declineTheOrder(orderAcceptRequestOne)
}