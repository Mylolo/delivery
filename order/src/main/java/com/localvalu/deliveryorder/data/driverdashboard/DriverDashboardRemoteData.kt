package com.localvalu.deliveryorder.data.driverdashboard

import com.localvalu.deliveryorder.data.model.request.DriverDashboardRequest
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestTwo

class DriverDashboardRemoteData(private val driverDashboardService: DriverDashboardService,
                                private val orderAcceptServiceOne: OrderAcceptServiceOne,
                                private val orderAcceptServiceTwo: OrderAcceptServiceTwo) : DriverDashboardDataContract.Remote
{
    override fun driverDashboard(driverDashboardRequest: DriverDashboardRequest)= driverDashboardService.driverDashboard(driverDashboardRequest)
    override fun acceptOrderOne(orderAcceptRequestOne: OrderAcceptRequestOne)= orderAcceptServiceOne.acceptTheOrderOne(orderAcceptRequestOne)
    override fun acceptOrderTwo(orderAcceptRequestTwo: OrderAcceptRequestTwo)= orderAcceptServiceTwo.acceptTheOrderTwo(orderAcceptRequestTwo)
}