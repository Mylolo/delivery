package com.localvalu.deliveryorder.data.fullorderdetails

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.model.request.FullOrderDetailsRequest
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.FullOrderDetailsResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class FullOrderDetailsRepository(private val remote: FullOrderDetailsDataContract.Remote,
                                 private val scheduler: Scheduler,
                                 private val compositeDisposable: CompositeDisposable) : FullOrderDetailsDataContract.Repository

{
        override val fullOrderDetailsFetchOutcome: PublishSubject<Outcome<FullOrderDetailsResponse>> =
            PublishSubject.create<Outcome<FullOrderDetailsResponse>>()

        override fun getFullOrderDetails(fullOrderDetailsRequest: FullOrderDetailsRequest)
        {
            fullOrderDetailsFetchOutcome.loading(true)
            Single.wrap(remote.fullOrderDetailsRemote(fullOrderDetailsRequest)).performOnBackOutOnMain(scheduler)
                .subscribe({fullOrderDetailsResponse ->  fullOrderDetailsFetchOutcome.success(fullOrderDetailsResponse)},
                    { error -> handleFullOrderDetailsError(error) })
                .addTo(compositeDisposable)
        }

        override fun handleFullOrderDetailsError(error: Throwable)
        {
            fullOrderDetailsFetchOutcome.failed(error)
        }
}