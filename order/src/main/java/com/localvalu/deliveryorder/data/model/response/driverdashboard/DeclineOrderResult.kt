package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import java.sql.ClientInfoStatus

data class DeclineOrderResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                              @SerializedName("status")  val status:String,
                              @SerializedName("message")  val message:String)

{

}