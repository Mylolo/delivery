package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliveryorder.data.model.request.AcceptOrderListRequest
import com.localvalu.deliveryorder.data.model.request.CompleteDeliveryRequest
import com.localvalu.deliveryorder.data.model.request.OrderStatusChangeRequest
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeResponse
import io.reactivex.Single

class AcceptedOrdersListRemoteData(private val acceptedOrderListService: AcceptedOrderListService,
                                   private val orderStatusChangeService: OrderStatusChangeService,
                                   private val completedDeliveryService: CompletedDeliveryService) : AcceptedOrderListDataContract.Remote
{
    override fun acceptedOrderList(acceptedOrderListRequest: AcceptOrderListRequest)=
        acceptedOrderListService.acceptedOrderList(acceptedOrderListRequest)
    override fun orderStatusChangeToReceivedFromMerchant(orderStatusChangeRequest: OrderStatusChangeRequest) =
        orderStatusChangeService.orderStatusChange(orderStatusChangeRequest)
    override fun orderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest: OrderStatusChangeRequest) =
        orderStatusChangeService.orderStatusChange(orderStatusChangeRequest)
    override fun completeDelivery(completeDeliveryRequest: CompleteDeliveryRequest)=
        completedDeliveryService.completeTheDelivery(completeDeliveryRequest)
}