package com.localvalu.deliveryorder.data.deliveryhistory

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.model.request.DeliveryHistoryRequest
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryResponse
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryRootResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface DeliveryHistoryDataContract
{
    interface Repository
    {
        //Delivery History Details
        val deliveryHistoryFetchOutcome: PublishSubject<Outcome<DeliveryHistoryResponse>>
        fun getRemoteDeliveryHistory(deliveryHistoryRequest: DeliveryHistoryRequest)
        fun handleDeliveryHistoryError(error: Throwable)
    }

    interface Remote
    {
        fun getDeliveryHistory(deliveryHistoryRequest: DeliveryHistoryRequest): Single<DeliveryHistoryRootResponse>
    }
}