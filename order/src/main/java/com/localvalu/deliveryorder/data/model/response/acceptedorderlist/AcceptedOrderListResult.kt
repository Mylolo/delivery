package com.localvalu.deliveryorder.data.model.response.acceptedorderlist

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails


data class AcceptedOrderListResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                   @SerializedName("DriverAcceptOrders")  val acceptedOrderDetails: List<AcceptedOrderDetails>,
                                   @SerializedName("status")  val status:String,
                                   @SerializedName("message")  val message:String)
{


}

