package com.localvalu.deliveryorder.data.driverdashboard

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverycore.networkhandler.Event
import com.localvalu.deliveryorder.data.model.request.DriverDashboardRequest
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests dashboard data which contains the latest orders assigned by merchant to drivers and
 *
 */

class DriverDashboardRepository(
    private val remote: DriverDashboardDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : DriverDashboardDataContract.Repository

{
    //DriverDashboard
    override val driverDashboardFetchOutcome: PublishSubject<Outcome<DriverDashboardResponse>> =
        PublishSubject.create<Outcome<DriverDashboardResponse>>()

    //AcceptOrder One
    override val acceptOrderFetchOutcomeOne:  PublishSubject<Outcome<AcceptOrderResponseOne>> =
        PublishSubject.create<Outcome<AcceptOrderResponseOne>>()

    //AcceptOrder Two
    override val acceptOrderFetchOutcomeTwo: PublishSubject<Outcome<AcceptOrderResponseTwo>> =
        PublishSubject.create<Outcome<AcceptOrderResponseTwo>>()

    //DriverDashboard
    override fun getRemoteDriverDashboard(driverDashboardReqest: DriverDashboardRequest)
    {
        driverDashboardFetchOutcome.loading(true)
        Single.wrap(remote.driverDashboard(driverDashboardReqest)).performOnBackOutOnMain(scheduler)
            .subscribe({driverDashboardResponse ->  driverDashboardFetchOutcome.success(driverDashboardResponse)},
                { error -> handleDriverDashboardError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleDriverDashboardError(error: Throwable)
    {
        driverDashboardFetchOutcome.failed(error)
    }

    //AcceptOrder One
    override fun requestAcceptOrderOne(orderAcceptRequestOne: OrderAcceptRequestOne)
    {
        acceptOrderFetchOutcomeOne.loading(true)
        Single.wrap(remote.acceptOrderOne(orderAcceptRequestOne)).performOnBackOutOnMain(scheduler)
            .subscribe({acceptOrderResponseOne-> acceptOrderFetchOutcomeOne.success(acceptOrderResponseOne)},
                { error -> handleAcceptOrderRequestErrorOne(error)})
    }

    //AcceptOrder Two
    override fun requestAcceptOrderTwo(orderAcceptRequestTwo: OrderAcceptRequestTwo)
    {
        acceptOrderFetchOutcomeTwo.loading(true)
        Single.wrap(remote.acceptOrderTwo(orderAcceptRequestTwo)).performOnBackOutOnMain(scheduler)
            .subscribe({acceptOrderRootResponseTwo-> acceptOrderFetchOutcomeTwo.success(acceptOrderRootResponseTwo.acceptOrderResponseTwo)},
                { error -> handleAcceptOrderRequestErrorTwo(error)})
    }

    override fun handleAcceptOrderRequestErrorOne(error: Throwable)
    {
        acceptOrderFetchOutcomeOne.failed(error)
    }

    override fun handleAcceptOrderRequestErrorTwo(error: Throwable)
    {
        acceptOrderFetchOutcomeOne.failed(error)
    }

}


