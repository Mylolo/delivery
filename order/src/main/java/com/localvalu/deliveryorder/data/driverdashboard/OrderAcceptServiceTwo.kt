package com.localvalu.deliveryorder.data.driverdashboard

import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderRootResponseTwo
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface OrderAcceptServiceTwo
{
    @POST("Acceptorder")
    fun acceptTheOrderTwo(@Body orderAcceptRequestTwo: OrderAcceptRequestTwo): Single<AcceptOrderRootResponseTwo>

}