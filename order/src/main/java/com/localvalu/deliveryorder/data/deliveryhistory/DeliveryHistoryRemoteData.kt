package com.localvalu.deliveryorder.data.deliveryhistory

import com.localvalu.deliveryorder.data.model.request.DeliveryHistoryRequest

class DeliveryHistoryRemoteData(private val deliveryHistoryService: DeliveryHistoryService) : DeliveryHistoryDataContract.Remote
{
    override fun getDeliveryHistory(deliveryHistoryRequest: DeliveryHistoryRequest)= deliveryHistoryService.getTheDeliveryHistory(deliveryHistoryRequest)
}