package com.localvalu.deliveryorder.data.orderdecline

import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface OrderDeclineService
{
    @POST("MerchantAppV2/changeDriverStatus")
    fun declineTheOrder(@Body orderAcceptRequestOne: OrderAcceptRequestOne): Single<AcceptOrderResponseOne>
}
