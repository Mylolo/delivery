package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class OrderDeclineRequest(@SerializedName("Token") var token: String,
                          @SerializedName("driverid") val driverId: String,
                          @SerializedName("orderid") val orderId: String,
                          @SerializedName("merchantid") val merchantId: String,
                          @SerializedName("Reason") val reason: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}