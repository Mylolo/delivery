package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.BaseResponse

data class DeclineOrderResponse(@SerializedName("OrderdeclineResult") val declineOrderResult: DeclineOrderResult)
{

}