package com.localvalu.deliveryorder.data.model.response.deliverycomplete

import com.google.gson.annotations.SerializedName

data class CompleteDeliveryResponse(@SerializedName("CompleteStatusResult") val completeDeliveryResult: CompleteDeliveryResult)
{

}