package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class DeliveryHistoryRequest(@SerializedName("Token") var token: String,
                             @SerializedName("Driverid") val driverId: String,
                             @SerializedName("Fromdate") val fromDate: String,
                             @SerializedName("Todate") val toDate: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}