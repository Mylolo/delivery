package com.localvalu.deliveryorder.data.model.response.notifytodrivers

import com.google.gson.annotations.SerializedName

data class NotifyToDriverResponse(@SerializedName("DriverNotificationResult") val notifyToDriverResult: NotifyToDriverResult)
{

}