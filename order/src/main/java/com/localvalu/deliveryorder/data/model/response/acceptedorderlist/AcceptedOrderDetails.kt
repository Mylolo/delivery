package com.localvalu.deliveryorder.data.model.response.acceptedorderlist

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AcceptedOrderDetails(@SerializedName("driverId") val driverId: String="",
                                @SerializedName("orderId") val orderId: String="",
                                @SerializedName("createdDate") val createdDate: String="",
                                @SerializedName("driverNotifyStatus") val driverNotifyStatus:String="",
                                @SerializedName("tradingname") val tradingName: String="",
                                @SerializedName("Orderstatusid") var orderStatusId: String="",
                                @SerializedName("merchantid") val merchantId: String="",
                                var performActionRequest:Boolean=false):Parcelable
{


}

