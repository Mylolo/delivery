package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class AcceptOrderResultOne(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                @SerializedName("status")  val status:String,
                                @SerializedName("message",alternate = ["msg"])  val message:String)

{

}