package com.localvalu.deliveryorder.data.model.response.fullorderdetails

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class FullOrderResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                           @SerializedName("FullOrderDetailsResult")  val fullOrderDetailsResult: FullOrderDetailsResults)

{

}