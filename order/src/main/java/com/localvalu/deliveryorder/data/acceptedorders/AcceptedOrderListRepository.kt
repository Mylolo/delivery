package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.model.request.*
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderListResponse
import com.localvalu.deliveryorder.data.model.response.deliverycomplete.CompleteDeliveryResponse
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests accepted order list data which contains the latest orders accepted by drivers
 *
 */

class AcceptedOrderListRepository(
    private val remote: AcceptedOrderListDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : AcceptedOrderListDataContract.Repository
{
    //Accepted Order List
    override val acceptedOrderListFetchOutcome: PublishSubject<Outcome<AcceptedOrderListResponse>> =
        PublishSubject.create<Outcome<AcceptedOrderListResponse>>()

    //Order Status Received From Merchant
    override val orderReceivedFromMerchantOutcome: PublishSubject<Outcome<OrderStatusChangeResponse>> =
        PublishSubject.create<Outcome<OrderStatusChangeResponse>>()

    //Order Status Delivered To Customer
    override val orderDeliveredToCustomerOutcome: PublishSubject<Outcome<OrderStatusChangeResponse>> =
        PublishSubject.create<Outcome<OrderStatusChangeResponse>>()

    //Complete The Delivery Order Status- PhpServer
    override val completeDeliveryOrderFetchOutcome: PublishSubject<Outcome<CompleteDeliveryResponse>> =
        PublishSubject.create<Outcome<CompleteDeliveryResponse>>()

    //Accepted Order List
    override fun getRemoteAcceptedOrderList(acceptedOrderListRequest: AcceptOrderListRequest)
    {
        acceptedOrderListFetchOutcome.loading(true)
        Single.wrap(remote.acceptedOrderList(acceptedOrderListRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({acceptedOrderListResponse ->  acceptedOrderListFetchOutcome.success(acceptedOrderListResponse)},
                { error -> handleAcceptedOrderListError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleAcceptedOrderListError(error: Throwable)
    {
        acceptedOrderListFetchOutcome.failed(error)
    }

    //Order Status Received From Merchant
    override fun requestOrderStatusChangeToReceivedFromMerchant(orderStatusChangeRequest: OrderStatusChangeRequest)
    {
        orderReceivedFromMerchantOutcome.loading(true)
        Single.wrap(remote.orderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({orderStatusChangeRootResponse-> orderReceivedFromMerchantOutcome.success(orderStatusChangeRootResponse.orderStatusChangeResponse)},
                { error -> handleOrderStatusChangeToReceivedFromMerchantError(error)})
    }

    override fun handleOrderStatusChangeToReceivedFromMerchantError(error: Throwable)
    {
        orderReceivedFromMerchantOutcome.failed(error)
    }

    //Order Status Delivered To Customer
    override fun requestOrderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest: OrderStatusChangeRequest)
    {
        orderDeliveredToCustomerOutcome.loading(true)
        Single.wrap(remote.orderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({orderStatusChangeRootResponse-> orderDeliveredToCustomerOutcome.success(orderStatusChangeRootResponse.orderStatusChangeResponse)},
                { error -> handleOrderedDeliveredToCustomerError(error)})
    }

    override fun handleOrderedDeliveredToCustomerError(error: Throwable)
    {
        orderDeliveredToCustomerOutcome.failed(error)
    }

    //Complete The Delivery Order Status- PhpServer
    override fun requestCompleteDelivery(completeDeliveryRequest: CompleteDeliveryRequest)
    {
        completeDeliveryOrderFetchOutcome.loading(true)
        Single.wrap(remote.completeDelivery(completeDeliveryRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({completeDeliveryResponse-> completeDeliveryOrderFetchOutcome.success(completeDeliveryResponse)},
                { error -> handleCompleteDeliveryRequestError(error)})
    }

    override fun handleCompleteDeliveryRequestError(error: Throwable)
    {
        completeDeliveryOrderFetchOutcome.failed(error)
    }
}

