package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliveryorder.data.model.request.OrderStatusChangeRequest
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeRootResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import javax.inject.Named

interface OrderStatusChangeService
{
    @POST("OrderStatuschange")
    fun orderStatusChange(@Body orderStatusChangeRequest: OrderStatusChangeRequest): Single<OrderStatusChangeRootResponse>
}
