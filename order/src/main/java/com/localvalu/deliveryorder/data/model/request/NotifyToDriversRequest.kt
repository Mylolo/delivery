package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class NotifyToDriversRequest(@SerializedName("Token") var token: String,
                             @SerializedName("retailerId") val retailerId: String,
                             @SerializedName("orderId") val orderId: String,
                             @SerializedName("DriverId") val driverId: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}