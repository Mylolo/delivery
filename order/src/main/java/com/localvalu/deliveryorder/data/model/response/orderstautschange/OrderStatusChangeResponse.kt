package com.localvalu.deliveryorder.data.model.response.orderstautschange

import com.google.gson.annotations.SerializedName

data class OrderStatusChangeResponse(@SerializedName("Change_DeliveryStatusResult") val orderStatusChangeResult: OrderStatusChangeResult)
{

}