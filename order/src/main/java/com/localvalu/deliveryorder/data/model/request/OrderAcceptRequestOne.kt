package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class OrderAcceptRequestOne(@SerializedName("Token") var token: String,
                            @SerializedName("Driverid") val driverId: String,
                            @SerializedName("OrderId") val orderId: String,
                            @SerializedName("statusId") val statusId: Int,
                            @SerializedName("Reason") val reason: String,
                            @SerializedName("Merchantid") val merchantId: Int)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}