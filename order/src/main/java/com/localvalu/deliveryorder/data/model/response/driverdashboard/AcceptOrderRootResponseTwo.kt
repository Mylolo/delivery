package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName

data class AcceptOrderRootResponseTwo(@SerializedName("DriverAppResult") val acceptOrderResponseTwo: AcceptOrderResponseTwo)
{

}