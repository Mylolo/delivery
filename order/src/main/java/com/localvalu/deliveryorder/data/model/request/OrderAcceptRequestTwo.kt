package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

class OrderAcceptRequestTwo(@SerializedName("Token") var token: String,
                            @SerializedName("Driverid") val driverId: String,
                            @SerializedName("Orderid") val orderId: String,
                            @SerializedName("Merchantid") val merchantId: String,
                            @SerializedName("AccountId") val accountId: String,
                            @SerializedName("km") val kiloMeter: String,
                            @SerializedName("Amount") val amount: String,
                            @SerializedName("paymode") val payMode: String,
                            @SerializedName("deliveryamt") val deliveryAmount: String,
                            @SerializedName("duration") val duration: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}