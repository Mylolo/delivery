package com.localvalu.deliveryorder.data.model.response.driverdashboard

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DriverDashboardData(@SerializedName("OrderId") val orderId: String="",
                               @SerializedName("orderTotal") val orderTotal: String="",
                               @SerializedName("retailerId") val retailerId: String="",
                               @SerializedName("tradingname") val tradingName: String="",
                               @SerializedName("orderStatus") val orderStatus: String="",
                               @SerializedName("DeliveryStatus") val deliveryStatus: String="",
                               @SerializedName("Customername") val CustomerName: String="",
                               @SerializedName("customerAddress") val customerAddress: String="",
                               @SerializedName("customerCity") val customerCity: String="",
                               @SerializedName("customerCountry") val customerCountry: String="",
                               @SerializedName("RestaurantAddress") val restaurantAddress: String="",
                               @SerializedName("RestaurantCity") val restaurantCity: String="",
                               @SerializedName("Restaurantcountry") val restaurantCountry: String="",
                               @SerializedName("RestaurantPostalcode") val restaurantPostalCode: String="",
                               @SerializedName("account_id") val accountId: String="",
                               @SerializedName("userMobileNo") val userMobileNo: String="",
                               @SerializedName("PayableAmount") val payableAmount: String="",
                               @SerializedName("deliveryRate") val deliveryRate: String="",
                               @SerializedName("km") val km: String="",
                               @SerializedName("Duration") val duration: String="",
                               @SerializedName("orderType") val orderType: String="",
                               var performAcceptRequest:Boolean=false,
                               var performDeclineRequest:Boolean=false):Parcelable
{


}

