package com.localvalu.deliveryorder.data.model.response.acceptedorderlist

import com.google.gson.annotations.SerializedName


data class AcceptedOrderListResponse(@SerializedName("DriverAcceptOrderResults")
                                     val acceptedOrderListResult: AcceptedOrderListResult)
{


}

