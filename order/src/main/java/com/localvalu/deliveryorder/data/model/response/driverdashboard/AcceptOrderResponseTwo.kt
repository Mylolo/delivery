package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName

data class AcceptOrderResponseTwo(@SerializedName("AcceptorderResult") val acceptOrderResultTwo:AcceptOrderResultTwo)
{

}