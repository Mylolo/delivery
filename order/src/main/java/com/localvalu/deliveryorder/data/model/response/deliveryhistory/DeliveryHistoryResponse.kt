package com.localvalu.deliveryorder.data.model.response.deliveryhistory

import com.google.gson.annotations.SerializedName

data class DeliveryHistoryResponse(@SerializedName("OrderDetailsResult") val orderDetailsResult: OrderDetailsResult)
{

}