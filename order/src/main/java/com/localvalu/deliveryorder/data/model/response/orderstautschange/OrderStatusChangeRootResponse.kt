package com.localvalu.deliveryorder.data.model.response.orderstautschange

import com.google.gson.annotations.SerializedName

data class OrderStatusChangeRootResponse(@SerializedName("DriverAppResult") val orderStatusChangeResponse: OrderStatusChangeResponse)
{

}