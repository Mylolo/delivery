package com.localvalu.deliveryorder.data.driverdashboard

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Event
import com.localvalu.deliveryorder.data.model.request.DriverDashboardRequest
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderRootResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface DriverDashboardDataContract
{
    interface Repository
    {
        //Driver Dashboard Data
        val driverDashboardFetchOutcome: PublishSubject<Outcome<DriverDashboardResponse>>
        fun getRemoteDriverDashboard(driverDashboardRequest: DriverDashboardRequest)
        fun handleDriverDashboardError(error: Throwable)
        //Accept Order One on PhpServer
        val acceptOrderFetchOutcomeOne: PublishSubject<Outcome<AcceptOrderResponseOne>>
        fun requestAcceptOrderOne(orderAcceptRequestOne: OrderAcceptRequestOne)
        fun handleAcceptOrderRequestErrorOne(error: Throwable)
        //Accept Order Two on DotNetServer
        val acceptOrderFetchOutcomeTwo: PublishSubject<Outcome<AcceptOrderResponseTwo>>
        fun requestAcceptOrderTwo(orderAcceptRequestTwo: OrderAcceptRequestTwo)
        fun handleAcceptOrderRequestErrorTwo(error: Throwable)
    }

    interface Remote
    {
        fun driverDashboard(driverDashboardRequest: DriverDashboardRequest): Single<DriverDashboardResponse>
        fun acceptOrderOne(orderAcceptRequestOne: OrderAcceptRequestOne): Single<AcceptOrderResponseOne>
        fun acceptOrderTwo(orderAcceptRequestTwo: OrderAcceptRequestTwo): Single<AcceptOrderRootResponseTwo>
    }
}