package com.localvalu.deliveryorder.data.model.response.fullorderdetails

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class FullOrderDetailsResults(@SerializedName("OrderAddrDetails")  val orderAddressDetails: List<OrderAddressDetails>,
                                   @SerializedName("OrderItemDetails")  val orderItemDetails: List<OrderItemDetails>)

{

}