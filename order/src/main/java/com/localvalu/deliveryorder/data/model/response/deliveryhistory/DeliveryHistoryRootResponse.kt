package com.localvalu.deliveryorder.data.model.response.deliveryhistory

import com.google.gson.annotations.SerializedName

data class DeliveryHistoryRootResponse(@SerializedName("DriverAppResult") val orderDetailsResponse: DeliveryHistoryResponse)
{

}