package com.localvalu.deliveryorder.data.model.response.fullorderdetails

import com.google.gson.annotations.SerializedName

data class OrderAddressDetails(@SerializedName("tradingname") val tradingName: String,
                               @SerializedName("address1")  val address1: String,
                               @SerializedName("address2")  val address2: String,
                               @SerializedName("postcode")  val postCode: String,
                               @SerializedName("contact_person_phone")  val contactPersonPhone: String,
                               @SerializedName("user_id")  val userId: String,
                               @SerializedName("accountId")  val accountId: String,
                               @SerializedName("loginUsername")  val loginUserName: String,
                               @SerializedName("userAdd1")  val userAddress1: String,
                               @SerializedName("userAdd2")  val userAddress2: String,
                               @SerializedName("orderUsername")  val oderUserName: String,
                               @SerializedName("userCompany")  val userCompany: String,
                               @SerializedName("userCity")  val userCity: String,
                               @SerializedName("userPostcose")  val userPostcode: String,
                               @SerializedName("userLandmark")  val userLandmark: String,
                               @SerializedName("userphone")  val userPhone: String,
                               @SerializedName("OrderId")  val OrderId: String,
                               @SerializedName("ItemCount")  val ItemCount: String,
                               @SerializedName("special_ins")  val specialInstructions:String,
                               @SerializedName("allergy_ins")  val allergyInstructions: String,
                               @SerializedName("createdDate")  val createdDate: String,
                               @SerializedName("deliveryRate")  val deliveryRate: String,
                               @SerializedName("PayableVatAmount")  val payableVatAmount: String,
                               @SerializedName("DiscountLtAmount")  val discountLTAmount: String,
                               @SerializedName("orderType")  val orderType: String)

{

}