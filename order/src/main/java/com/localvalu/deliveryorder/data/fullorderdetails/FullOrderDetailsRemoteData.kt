package com.localvalu.deliveryaccount.data

import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsDataContract
import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsService
import com.localvalu.deliveryorder.data.model.request.FullOrderDetailsRequest

class FullOrderDetailsRemoteData(private val fullOrderDetailsService: FullOrderDetailsService) : FullOrderDetailsDataContract.Remote
{
    override fun fullOrderDetailsRemote(fullOrderDetailsRequest: FullOrderDetailsRequest)= fullOrderDetailsService.fullOrderDetails(fullOrderDetailsRequest)
}