package com.localvalu.deliveryorder.data.model.response.deliveryhistory

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DriverOrderDetails(@SerializedName("DriverName") val driverName: String="",
                              @SerializedName("MerchantName") val merchantName: String="",
                              @SerializedName("Mileage") val mileage: Double=0.0,
                              @SerializedName("TranscationAmt") val transactionAmount:Double=0.0,
                              @SerializedName("Paymode") val payMode: String="",
                              @SerializedName("DeliveryAmt") val deliveryAmount: Double=0.0,
                              @SerializedName("Duration") val duration: String="",
                              @SerializedName("OrderStatus") val orderStatus: String="",
                              @SerializedName("OrderId") val orderId: String="",
                              @SerializedName("DeliveredDate") val deliveredDate: String):Parcelable
{


}

