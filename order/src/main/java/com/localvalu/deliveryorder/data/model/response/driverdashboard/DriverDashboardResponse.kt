package com.localvalu.deliveryorder.data.model.response.driverdashboard

import com.google.gson.annotations.SerializedName

data class DriverDashboardResponse(@SerializedName("DriverDashboardResult") val driverDashboardResult: DriverDashboardResult)
{

}