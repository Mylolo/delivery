package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliveryorder.data.model.request.AcceptOrderListRequest
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderListResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AcceptedOrderListService
{
    @POST("MerchantAppV2/LoadAcceptOrderdetails")
    fun acceptedOrderList(@Body acceptOrderListRequest: AcceptOrderListRequest): Single<AcceptedOrderListResponse>
}
