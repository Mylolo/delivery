package com.localvalu.deliveryorder.data.model.response.fullorderdetails

import com.google.gson.annotations.SerializedName

data class FullOrderDetailsResponse(@SerializedName("FullOrderResults") val fullOrderResults: FullOrderResult)
{

}