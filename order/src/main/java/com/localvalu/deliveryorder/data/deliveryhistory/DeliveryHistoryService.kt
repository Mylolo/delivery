package com.localvalu.deliveryorder.data.deliveryhistory

import com.localvalu.deliveryorder.data.model.request.DeliveryHistoryRequest
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryRootResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface DeliveryHistoryService
{
    @POST("OrderDetails")
    fun getTheDeliveryHistory(@Body deliveryHistoryRequest: DeliveryHistoryRequest): Single<DeliveryHistoryRootResponse>
}