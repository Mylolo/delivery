package com.localvalu.deliveryorder.data.orderdecline

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests dashboard data which contains the latest orders assigned by merchant to drivers and
 *
 */

class OrderDeclineRepository(
    private val remote: OrderDeclineDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : OrderDeclineDataContract.Repository

{

    //DeclineOrder
    override val declineOrderFetchOutcomeOne: PublishSubject<Outcome<AcceptOrderResponseOne>> =
        PublishSubject.create<Outcome<AcceptOrderResponseOne>>()

    //DeclineOrder
    override fun requestDeclineOrder(orderAcceptRequestOne: OrderAcceptRequestOne)
    {
        declineOrderFetchOutcomeOne.loading(true)
        Single.wrap(remote.declineOrder(orderAcceptRequestOne)).performOnBackOutOnMain(scheduler)
            .subscribe({declineOrderResponse-> declineOrderFetchOutcomeOne.success(declineOrderResponse)},
                { error -> handleDeclineOrderRequestError(error)})
    }

    override fun handleDeclineOrderRequestError(error: Throwable)
    {
        declineOrderFetchOutcomeOne.failed(error)
    }




}

