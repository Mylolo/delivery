package com.localvalu.deliveryorder.data.fullorderdetails

import com.localvalu.deliveryorder.data.model.request.FullOrderDetailsRequest
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.FullOrderDetailsResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface FullOrderDetailsService
{
    @POST("MerchantAppV2/LoadFullOrderdetails")
    fun fullOrderDetails(@Body fullOrderDetailsRequest: FullOrderDetailsRequest): Single<FullOrderDetailsResponse>
}