package com.localvalu.deliveryorder.data.driverdashboard

import com.localvalu.deliveryorder.data.model.request.DriverDashboardRequest
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import javax.inject.Named

interface DriverDashboardService
{
    @Named("PhpServer")
    @POST("MerchantAppV2/DriversDashboard")
    fun driverDashboard(@Body driverDashboardRequest: DriverDashboardRequest): Single<DriverDashboardResponse>
}