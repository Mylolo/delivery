package com.localvalu.deliveryorder.data.model.response.deliveryhistory

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import java.sql.ClientInfoStatus

data class OrderDetailsResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                              @SerializedName("DriverOrderDetails")  val driverOrderDetails: List<DriverOrderDetails>,
                              @SerializedName("status")  val status:String,
                              @SerializedName("message")  val message:String)

{

}