package com.localvalu.deliveryorder.data.model.response.notifytodrivers

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcel
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DriverStatus(@SerializedName("Driverid") val driverId: String = "",
                        @SerializedName("retailerId") val retailerId: String = "",
                        @SerializedName("orderId") val orderId: String ="",
                        @SerializedName("driverNotifyStatus") val driverNotifyStatus: String="") : Parcelable
{


}

