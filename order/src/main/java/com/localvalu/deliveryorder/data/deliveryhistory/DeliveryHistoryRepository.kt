package com.localvalu.deliveryorder.data.deliveryhistory

import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.model.request.DeliveryHistoryRequest
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryResponse
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DriverOrderDetails
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.operators.observable.ObservableCache.from
import io.reactivex.subjects.PublishSubject


/**
 * Class that requests the list of  deliveries between two dates from remote data source
 *
 */

class DeliveryHistoryRepository(
    private val remote: DeliveryHistoryDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : DeliveryHistoryDataContract.Repository

{

    override val deliveryHistoryFetchOutcome: PublishSubject<Outcome<DeliveryHistoryResponse>> =
        PublishSubject.create<Outcome<DeliveryHistoryResponse>>()

    override fun getRemoteDeliveryHistory(deliveryHistoryRequest: DeliveryHistoryRequest)
    {
        deliveryHistoryFetchOutcome.loading(true)
        Single.wrap(remote.getDeliveryHistory(deliveryHistoryRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({deliveryHistoryRootResponse -> deliveryHistoryFetchOutcome.success(deliveryHistoryRootResponse.orderDetailsResponse) },
                { error -> handleDeliveryHistoryError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleDeliveryHistoryError(error: Throwable)
    {
        deliveryHistoryFetchOutcome.failed(error)
    }

    private fun alteredList(deliveryHistoryResponse:DeliveryHistoryResponse)
    {
        if(deliveryHistoryResponse!=null)
        {
            if(deliveryHistoryResponse.orderDetailsResult!=null)
            {
                if(deliveryHistoryResponse.orderDetailsResult.driverOrderDetails!=null)
                {
                    var list:List<DriverOrderDetails> = deliveryHistoryResponse.orderDetailsResult.driverOrderDetails;
                    var i:Int = 1;
                    for(item in list)
                    {
                        //item.id = i
                        i++
                    }
                }
            }
        }


    }

}

