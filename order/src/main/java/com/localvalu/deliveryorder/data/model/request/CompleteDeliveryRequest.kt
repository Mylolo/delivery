package com.localvalu.deliveryorder.data.model.request

import com.google.gson.annotations.SerializedName

/*{
    "Token":"/3+YFd5QZdSK9EKsB8+TlA==",
    "Driverid":"68",
    "OrderId":"1400",
    "statusId":3
}*/
class CompleteDeliveryRequest(@SerializedName("Token") var token: String,
                              @SerializedName("Driverid") val driverId: String,
                              @SerializedName("OrderId") val orderId: String,
                              @SerializedName("statusId") val statusId: Int)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}