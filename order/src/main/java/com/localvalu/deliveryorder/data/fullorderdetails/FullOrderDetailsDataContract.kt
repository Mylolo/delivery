package com.localvalu.deliveryorder.data.fullorderdetails

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.model.request.FullOrderDetailsRequest
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.FullOrderDetailsResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface FullOrderDetailsDataContract
{
    interface Repository
    {
        val fullOrderDetailsFetchOutcome: PublishSubject<Outcome<FullOrderDetailsResponse>>
        fun getFullOrderDetails(fullOrderDetailsRequest: FullOrderDetailsRequest)
        fun handleFullOrderDetailsError(error: Throwable)
    }

    interface Remote
    {
        fun fullOrderDetailsRemote(fullOrderDetailsRequest: FullOrderDetailsRequest): Single<FullOrderDetailsResponse>
    }
}