package com.localvalu.deliveryorder.data.orderdecline

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface OrderDeclineDataContract
{
    interface Repository
    {
        //Decline Order
        val declineOrderFetchOutcomeOne: PublishSubject<Outcome<AcceptOrderResponseOne>>
        fun requestDeclineOrder(orderAcceptRequestOne: OrderAcceptRequestOne)
        fun handleDeclineOrderRequestError(error: Throwable)
    }

    interface Remote
    {
        fun declineOrder(orderAcceptRequestOne: OrderAcceptRequestOne): Single<AcceptOrderResponseOne>
    }
}