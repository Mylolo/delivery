package com.localvalu.deliveryorder.data.acceptedorders

import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.model.request.*
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderListResponse
import com.localvalu.deliveryorder.data.model.response.deliverycomplete.CompleteDeliveryResponse
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeResponse
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeRootResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface AcceptedOrderListDataContract
{
    interface Repository
    {
        //Accepted Order List
        val acceptedOrderListFetchOutcome: PublishSubject<Outcome<AcceptedOrderListResponse>>
        fun getRemoteAcceptedOrderList(acceptedOrderListRequest: AcceptOrderListRequest)
        fun handleAcceptedOrderListError(error: Throwable)

        //Order Status Received From Merchant
        val orderReceivedFromMerchantOutcome: PublishSubject<Outcome<OrderStatusChangeResponse>>
        fun requestOrderStatusChangeToReceivedFromMerchant(orderStatusChangeRequest: OrderStatusChangeRequest)
        fun handleOrderStatusChangeToReceivedFromMerchantError(error: Throwable)

        //Order Status Delivered To Customer
        val orderDeliveredToCustomerOutcome: PublishSubject<Outcome<OrderStatusChangeResponse>>
        fun requestOrderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest: OrderStatusChangeRequest)
        fun handleOrderedDeliveredToCustomerError(error: Throwable)

        //Order Status Delivered To Customer - PhpServer
        val completeDeliveryOrderFetchOutcome: PublishSubject<Outcome<CompleteDeliveryResponse>>
        fun requestCompleteDelivery(completeDeliveryRequest: CompleteDeliveryRequest)
        fun handleCompleteDeliveryRequestError(error: Throwable)
    }

    interface Remote
    {
        fun acceptedOrderList(acceptedOrderListRequest: AcceptOrderListRequest): Single<AcceptedOrderListResponse>
        fun orderStatusChangeToReceivedFromMerchant(orderStatusChangeRequest: OrderStatusChangeRequest): Single<OrderStatusChangeRootResponse>
        fun orderStatusChangeToDeliveredToCustomer(orderStatusChangeRequest: OrderStatusChangeRequest): Single<OrderStatusChangeRootResponse>
        fun completeDelivery(completeDeliveryRequest: CompleteDeliveryRequest): Single<CompleteDeliveryResponse>
    }
}