package com.localvalu.deliveryorder.ui.fragments.orderdecline

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doAfterTextChanged
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import com.localvalu.deliveryorder.di.orderdecline.OrderDeclineDH
import kotlinx.android.synthetic.main.fragment_decline_order.*
import kotlinx.android.synthetic.main.toolbar_order.*
import java.io.IOException
import javax.inject.Inject

/**
 *  Accept = status = 1
 *  Decline = status = 2
 */

class OrderDeclineFragment : Fragment()
{

    private val TAG = "OrderDeclineFragment"

    private val component by lazy { OrderDeclineDH.orderDeclineComponent() }

    @Inject
    lateinit var viewModelFactory: OrderDeclineViewModelFactory

    private lateinit var driverDashboardData:DriverDashboardData

    private val viewModel: OrderDeclineViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(OrderDeclineViewModel::class.java)
    }

    private lateinit var strCurrencySymbol:String

    private var driverId = "-1"
    private var orderId = "-1"
    private var declineSuccessMessage:String="";
    private var validReason = false;


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_decline_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        Log.d(TAG, "arguments ")

        setProperties()
    }

    private fun setProperties()
    {
        val fontType = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular)
        etDeclineOrderReason.typeface = fontType;
        etDeclineOrderReason.doAfterTextChanged { viewModel.isReasonValid(etDeclineOrderReason.text.toString()) }

        arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let {
            driverId=it;
        }
        arguments?.getParcelable<DriverDashboardData>(BundleUtils.BUNDLE_DRIVER_DASHBOARD_DATA)?.let {
            driverDashboardData = it;
        }
        btnDeclineFragmentDelcineOrder.setOnClickListener(View.OnClickListener {
            it.hideKeyboard()
            initOrderDeclineListener()
            viewModel.declineOrder(driverId,driverDashboardData.orderId,driverDashboardData.retailerId,etDeclineOrderReason.text.toString())
        })

        initReasonValidateListener()
    }

    private  fun initReasonValidateListener()
    {
        viewModel.validReason?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult)
            {
                tilDeclineOrderReason.isErrorEnabled = true;
                tilDeclineOrderReason.error = getString(R.string.lbl_reason_empty);
                validReason=false;
                enableDisableSignInButton()
            }
            else
            {
                tilDeclineOrderReason.error = null
                tilDeclineOrderReason.isErrorEnabled = false
                validReason=true;
                enableDisableSignInButton()

            }
        })
    }

    private fun initOrderDeclineListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.declineOrderOutcomeOne.observe(viewLifecycleOwner, Observer<Outcome<AcceptOrderResponseOne>> { outcome ->
            Log.d(TAG, "initOrderDeclineListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {
                        cnlFragmentDeclineOrderContent.visibility = View.GONE
                        progressBarFragmentOrderDecline.visibility = View.VISIBLE
                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "initiateLoginDataListener: Successfully loaded data")
                    progressBarFragmentOrderDecline.visibility = View.GONE
                    if(outcome.data.acceptOrderResultOne.errorDetails.errorCode == 0)
                    {
                        declineSuccessMessage = getString(R.string.lbl_you_have_declined_the_order)
                        val messageFromServer = StringBuilder();
                        messageFromServer.append(declineSuccessMessage).append(" ");
                        Toast.makeText(context,messageFromServer.toString(),Toast.LENGTH_LONG).show()
                        viewModel.updateData("New data for list fragment")

                        // Use the Kotlin extension in the fragment-ktx artifact
                        val result = Bundle()
                        result.putBoolean(BundleUtils.BUNDLE_ORDER_DECLINED, true)
                        setFragmentResult(BundleUtils.KEY_ORDER_DECLINED_REQUEST, result);
                        findNavController().popBackStack()
                    }
                    else
                    {
                        Toast.makeText(context,""+outcome.data.acceptOrderResultOne.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                        cnlFragmentDeclineOrderContent.visibility = View.VISIBLE
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {
                        progressBarFragmentOrderDecline.visibility = View.GONE
                        cnlFragmentDeclineOrderContent.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_no_internet_connection),Toast.LENGTH_LONG).show()
                    }

                    else
                    {
                        progressBarFragmentOrderDecline.visibility = View.GONE
                        cnlFragmentDeclineOrderContent.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_unknown_error_try_again),Toast.LENGTH_LONG).show()
                    }
                }

            }
        })
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("driverDashboardFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_order_decline)
    }

    private fun enableDisableSignInButton()
    {
        btnDeclineFragmentDelcineOrder.isEnabled = validReason;
    }

    private fun View.hideKeyboard()
    {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onDestroyView()
    {
        this.view?.hideKeyboard()
        super.onDestroyView()
    }

}