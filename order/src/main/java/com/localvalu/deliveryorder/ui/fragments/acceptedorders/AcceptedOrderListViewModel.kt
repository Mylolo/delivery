package com.localvalu.deliveryorder.ui.fragments.acceptedorders

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.constants.Constants.DRIVER_ORDER_COMPLETE_DELIVER
import com.localvalu.deliverycore.constants.Constants.STATUS_ORDER_DELIVERED_TO_CONSUMER
import com.localvalu.deliverycore.constants.Constants.STATUS_ORDER_RECEIVED_FROM_MERCHANT
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.acceptedorders.AcceptedOrderListDataContract
import com.localvalu.deliveryorder.data.model.request.AcceptOrderListRequest
import com.localvalu.deliveryorder.data.model.request.CompleteDeliveryRequest
import com.localvalu.deliveryorder.data.model.request.OrderStatusChangeRequest
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderListResponse
import com.localvalu.deliveryorder.data.model.response.deliverycomplete.CompleteDeliveryResponse
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeResponse
import com.localvalu.deliveryorder.di.driverdashboard.DriverDashboardDH
import io.reactivex.disposables.CompositeDisposable

class AcceptedOrderListViewModel(
    private val acceptedOrderListRepository: AcceptedOrderListDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    //Accepted Order List
    val acceptedOrderListOutcome: LiveData<Outcome<AcceptedOrderListResponse>> by lazy {
        //Convert publish subject to livedata
        acceptedOrderListRepository.acceptedOrderListFetchOutcome.toLiveData(compositeDisposable)
    }

    //Received Order From Merchant
    val receivedOrderFromMerchantOutcome: LiveData<Outcome<OrderStatusChangeResponse>> by lazy {
        //Convert publish subject to livedata
        acceptedOrderListRepository.orderReceivedFromMerchantOutcome.toLiveData(compositeDisposable)
    }

    //Received Order From Merchant
    val deliveredToCustomerOutcome: LiveData<Outcome<OrderStatusChangeResponse>> by lazy {
        //Convert publish subject to livedata
        acceptedOrderListRepository.orderDeliveredToCustomerOutcome.toLiveData(compositeDisposable)
    }

    //Complete Order
    val completeDeliveryOutcome: LiveData<Outcome<CompleteDeliveryResponse>> by lazy {
        //Convert publish subject to livedata
        acceptedOrderListRepository.completeDeliveryOrderFetchOutcome.toLiveData(compositeDisposable)
    }

    // Update if order declined in list page:
    val dataToShare = MutableLiveData<String>()

    fun acceptedOrderList(driverId: String)
    {
        acceptedOrderListRepository.getRemoteAcceptedOrderList(AcceptOrderListRequest("",driverId))
    }

    fun orderStatusChangeToReceivedFromMerchant(driverId: Int,orderId: Int,merchantId:Int)
    {
        acceptedOrderListRepository.requestOrderStatusChangeToReceivedFromMerchant(
            OrderStatusChangeRequest("",driverId,orderId, STATUS_ORDER_RECEIVED_FROM_MERCHANT,merchantId)
        )
    }

    fun orderStatusChangeToDeliveredToCustomer(driverId: Int,orderId: Int,merchantId:Int)
    {
        acceptedOrderListRepository.requestOrderStatusChangeToDeliveredToCustomer(
            OrderStatusChangeRequest("",driverId,orderId, STATUS_ORDER_DELIVERED_TO_CONSUMER,merchantId)
        )
    }

    fun completeDelivery(driverId: String,orderId:String)
    {
        acceptedOrderListRepository.requestCompleteDelivery(
            CompleteDeliveryRequest("",driverId,orderId, DRIVER_ORDER_COMPLETE_DELIVER)
        )
    }

    fun updateData(data: String) {
        dataToShare.value = data
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        DriverDashboardDH.destroyDriverDashboardComponent()
    }
}