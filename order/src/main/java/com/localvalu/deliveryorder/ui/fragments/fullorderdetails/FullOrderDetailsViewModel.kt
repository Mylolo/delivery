package com.localvalu.deliveryorder.ui.fragments.fullorderdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsDataContract
import com.localvalu.deliveryorder.data.model.request.FullOrderDetailsRequest
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.FullOrderDetailsResponse
import com.localvalu.deliveryorder.di.fullorderdetails.FullOrderDetailsDH
import io.reactivex.disposables.CompositeDisposable

class FullOrderDetailsViewModel(
    private val fullOrderDetailsRepository: FullOrderDetailsDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    val fullOrderDetailsOutcome: LiveData<Outcome<FullOrderDetailsResponse>> by lazy {
        //Convert publish subject to livedata
        fullOrderDetailsRepository.fullOrderDetailsFetchOutcome.toLiveData(compositeDisposable)
    }

    fun fullOrderDetails(driverId: String,orderId:String)
    {
        fullOrderDetailsRepository.getFullOrderDetails(FullOrderDetailsRequest("",driverId,orderId))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        FullOrderDetailsDH.destroyFullOrderDetailsComponent()
    }
}