package com.localvalu.deliveryorder.ui.fragments.acceptedorders

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.localvalu.deliverycore.constants.Constants
import com.localvalu.deliverycore.constants.Constants.STATUS_ORDER_RECEIVED_FROM_MERCHANT
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderDetails
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderListResponse
import com.localvalu.deliveryorder.data.model.response.deliverycomplete.CompleteDeliveryResponse
import com.localvalu.deliveryorder.data.model.response.orderstautschange.OrderStatusChangeResponse
import com.localvalu.deliveryorder.di.acceptedorders.AcceptedOrdersListDH
import kotlinx.android.synthetic.main.fragment_order_list.*
import kotlinx.android.synthetic.main.fragment_order_list.btnRetry
import kotlinx.android.synthetic.main.fragment_order_list.errorContainer
import kotlinx.android.synthetic.main.fragment_order_list.tvError
import kotlinx.android.synthetic.main.toolbar_order.toolbar
import kotlinx.android.synthetic.main.toolbar_order.tvToolBarTitle
import java.io.IOException
import javax.inject.Inject


/* 0 - Order initial stage
   1 - Order Accepted by Driver
   2 - Order Received from Merchant
   3 - Order Delivered to Consumer
   4 - Driver Accepted order not delivered yet*/

class AcceptedOrderListFragment : Fragment(), AcceptedOrderListAdapter.OrderItemClickListener
{

    private val TAG =  "AOLF"

    private val B_ORDER_LIST = "B_ORDER_LIST"

    private val component by lazy { AcceptedOrdersListDH.acceptedOrdersListComponent() }

    @Inject
    lateinit var viewModelFactory: AcceptedOrderListViewModelFactory

    @Inject
    lateinit var adapter: AcceptedOrderListAdapter

    @Inject
    lateinit var contextTool: Context

    private val viewModel: AcceptedOrderListViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(AcceptedOrderListViewModel::class.java)
    }

    private var driverId = "-1"

    private var listShown = false;

    private lateinit var orderListGlobal: MutableList<AcceptedOrderDetails>

    private lateinit var selectedOrder: AcceptedOrderDetails

    private var selectedPosition: Int = -1


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setUpResultListener()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        adapter.orderItemClickListener = this
        rvOrderList.layoutManager = LinearLayoutManager(context);
        rvOrderList.adapter = adapter
        btnRetry.setOnClickListener {
            viewModel.acceptedOrderList(driverId)
        }
        initAcceptedOrdersListDataListener();
        initReceivedFromMerchantListener()
        initDeliveredToCustomerListener();
        initCompleteDeliveryListener()
        Log.d(TAG, "arguments ")
        arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let {
            driverId = it
            viewModel.acceptedOrderList(driverId)
        }
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("deliveryHistoryListFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_accepted_orders)
        toolbar.title=""
    }

    private fun setUpResultListener()
    {
        setFragmentResultListener(BundleUtils.KEY_ORDER_DECLINED_REQUEST) { requestKey, result ->
            onFragmentResult(requestKey, result)
        }
    }

    private fun onFragmentResult(requestKey: String, result: Bundle)
    {
        val orderDeclined = result.getBoolean(BundleUtils.BUNDLE_ORDER_DECLINED);
        if (orderDeclined)
        {
            //viewModel.driverDashboardOutcome.removeObservers(viewLifecycleOwner)
            //initiateDriverDashboardDataListener()
            //viewModel.driverDashboard(driverId)
        }
    }

    private fun initAcceptedOrdersListDataListener()
    {
        viewModel.acceptedOrderListOutcome.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.acceptedOrderListOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<AcceptedOrderListResponse>> { outcome ->
                Log.d(TAG, "initAcceptedOrdersListDataListener: $outcome")
                when (outcome)
                {
                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            showProgress()
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initiateDriverDashboardDataListener: Successfully loaded data")
                        if (outcome.data.acceptedOrderListResult.errorDetails.errorCode == 0)
                        {
                            setList(outcome.data.acceptedOrderListResult.acceptedOrderDetails);
                        }
                        else
                        {
                            //showErrorMessage(outcome.data.driverDashboardResult.errorDetails.errorMessage,false,1)
                            showInitialError(
                                outcome.data.acceptedOrderListResult.errorDetails.errorMessage,
                                false
                            );
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            //showErrorMessage(getString(R.string.msg_no_internet_connection),true,1)
                            showInitialError(getString(R.string.msg_no_internet_connection), true);
                        }
                        else
                        {
                            //showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,1)
                            showInitialError(getString(R.string.msg_unknown_error_try_again), true);
                        }
                    }
                }
            })
    }

    private fun initReceivedFromMerchantListener()
    {
        viewModel.receivedOrderFromMerchantOutcome.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.receivedOrderFromMerchantOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<OrderStatusChangeResponse>> { outcome ->
                Log.d(TAG, "initReceivedFromMerchantListener: $outcome")
                when (outcome)
                {
                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            actionProgress(true)
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initReceivedFromMerchantListener: Successfully loaded data")
                        actionProgress(false)
                        if (outcome.data.orderStatusChangeResult.errorDetails.errorCode == 0)
                        {
                            if(selectedOrder!=null)
                            {
                                selectedOrder.orderStatusId=STATUS_ORDER_RECEIVED_FROM_MERCHANT.toString()
                                orderListGlobal[selectedPosition]=selectedOrder
                                adapter.swapData(orderListGlobal)
                            }
                        }
                        else
                        {
                            showErrorMessage(outcome.data.orderStatusChangeResult.errorDetails.errorMessage,false,1)
                        }
                    }

                    is Outcome.Failure ->
                    {
                        actionProgress(false)
                        if (outcome.e is IOException)
                        {
                            showErrorMessage(getString(R.string.msg_no_internet_connection),true,1)
                        }
                        else
                        {
                            showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,1)
                        }
                    }
                }
            })
    }

    private fun initDeliveredToCustomerListener()
    {
        viewModel.deliveredToCustomerOutcome.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.deliveredToCustomerOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<OrderStatusChangeResponse>> { outcome ->
                Log.d(TAG, "initDeliveredToCustomerListener: $outcome")
                when (outcome)
                {
                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            actionProgress(false)
                        }
                    }

                    is Outcome.Success ->
                    {
                        actionProgress(true)
                        Log.d(TAG, "initDeliveredToCustomerListener: Successfully loaded data")
                        if (outcome.data.orderStatusChangeResult.errorDetails.errorCode == 0)
                        {
                            completeOrder()
                        }
                        else
                        {
                            showErrorMessage(getString(R.string.msg_no_internet_connection),false,3)
                        }
                    }

                    is Outcome.Failure ->
                    {
                        actionProgress(false)
                        if (outcome.e is IOException)
                        {
                            showErrorMessage(getString(R.string.msg_no_internet_connection),true,3)
                        }
                        else
                        {
                            showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,3)
                        }
                    }
                }
            })
    }

    private fun initCompleteDeliveryListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.completeDeliveryOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<CompleteDeliveryResponse>> { outcome ->
                Log.d(TAG, "initiateAcceptDeclineDataListener: $outcome")
                when (outcome)
                {
                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            actionProgress(true)
                        }
                    }
                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initiateAcceptDeclineDataListener: Successfully loaded data")

                        actionProgress(true)

                        if (outcome.data.completeDeliveryResult.errorDetails.errorCode == 0)
                        {
                            completeDeliverySuccess(getString(R.string.lbl_you_have_completed_the_order))
                        }
                        else
                        {
                            showErrorMessage(
                                outcome.data.completeDeliveryResult.errorDetails.errorMessage,
                                false,
                                3
                            )
                        }
                    }
                    is Outcome.Failure ->
                    {

                        actionProgress(false)

                        if (outcome.e is IOException)
                        {
                            showErrorMessage(
                                getString(R.string.msg_no_internet_connection),
                                true,
                                3
                            )
                        }
                        else
                        {
                            showErrorMessage(
                                getString(R.string.msg_unknown_error_try_again),
                                true,
                                3
                            )
                        }
                    }
                }
            })
    }

    private fun showProgress()
    {
        rvOrderList.visibility = View.GONE
        progressBarFragmentOrderList.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
    }

    private fun setList(acceptedOrderList: List<AcceptedOrderDetails>)
    {
        orderListGlobal = acceptedOrderList as MutableList<AcceptedOrderDetails>
        adapter.swapData(orderListGlobal)
        showList()
    }

    private fun showList()
    {
        listShown = true;
        rvOrderList.visibility = View.VISIBLE
        progressBarFragmentOrderList.visibility = View.GONE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialError(message: String, retry: Boolean)
    {
        progressBarFragmentOrderList.visibility = View.GONE
        errorContainer.visibility = View.VISIBLE
        rvOrderList.visibility = View.GONE
        tvError.text = message;
        if (retry) btnRetry.visibility = View.VISIBLE
        else btnRetry.visibility = View.GONE
    }

    private fun showErrorMessage(message: String, retry: Boolean, apiResponseFrom: Int)
    {
        val mySnackbar = Snackbar.make(
            clFragmentOrderList,
            message, Snackbar.LENGTH_INDEFINITE
        )
        mySnackbar.setBackgroundTint(
            ContextCompat.getColor(
                contextTool,
                R.color.colorPrimaryDark
            )
        )
        if (retry) mySnackbar.setAction(R.string.lbl_retry, View.OnClickListener { v ->
            when (apiResponseFrom)
            {
                1 -> viewModel.acceptedOrderList(driverId)
                2 -> completeOrder()
            }
            mySnackbar.dismiss()
        })
        else mySnackbar.setAction(R.string.lbl_ok, View.OnClickListener { v ->
            mySnackbar.dismiss()
        })
        mySnackbar.show()
        selectedOrder = AcceptedOrderDetails()
        selectedOrder?.let {
            it.performActionRequest = false
        }
    }

    private fun actionProgress(isUpdating: Boolean)
    {
        selectedOrder.let {
            it.performActionRequest = isUpdating
            if(orderListGlobal.size>0)
            {
                if(selectedPosition<orderListGlobal.size)
                {
                    orderListGlobal[selectedPosition] = it
                    adapter.swapData(orderListGlobal)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun completeDeliverySuccess(message: String)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        viewModel.acceptedOrderList(driverId)
    }

    override fun receiveOrderFromMerchantClicked(acceptedOrderDetails: AcceptedOrderDetails,position: Int)
    {
        selectedOrder = acceptedOrderDetails;
        selectedPosition = position;
        changeStatusToReceivedFromMerchant()
    }

    override fun completeClicked(acceptedOrderDetails: AcceptedOrderDetails, position: Int)
    {
        selectedOrder = acceptedOrderDetails;
        selectedPosition = position;
        changeStatusToDelivered()
    }

    private fun changeStatusToReceivedFromMerchant()
    {
        viewModel.orderStatusChangeToReceivedFromMerchant(driverId.toInt(), selectedOrder.orderId.toInt(),selectedOrder.merchantId.toInt())
    }

    private fun changeStatusToDelivered()
    {
        viewModel.orderStatusChangeToDeliveredToCustomer(driverId.toInt(), selectedOrder.orderId.toInt(),selectedOrder.merchantId.toInt())
    }

    private fun completeOrder()
    {
        viewModel.completeDelivery(driverId, selectedOrder.orderId)
    }

    override fun itemClicked(acceptedOrderDetails: AcceptedOrderDetails, position: Int)
    {
        gotoFullOrderDetailsFragment(acceptedOrderDetails.orderId);
    }

    private fun gotoFullOrderDetailsFragment(orderId: String)
    {
        val navGraphId =
            resources.getIdentifier("nav_graph", "navigation", "com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier(
            "action_acceptedOrderListFragment_to_actionFullOrderDetailsFragment",
            "id", "com.localvalu.deliveryapp"
        )
        var bundle = bundleOf(
            BundleUtils.BUNDLE_DRIVER_ID to driverId,
            BundleUtils.BUNDLE_ORDER_ID to orderId
        )
        if (navGraphId != 0)
        {
            findNavController().navigate(actionId, bundle)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.order_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item!!.itemId
        //handle item clicks
        if (id == R.id.refresh)
        {
            viewModel.acceptedOrderList(driverId)
        }
        return super.onOptionsItemSelected(item)
    }

}



