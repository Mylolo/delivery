package com.localvalu.deliveryorder.ui.fragments.deliveryhistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryDataContract
import com.localvalu.deliveryorder.data.model.request.DeliveryHistoryRequest
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryResponse
import com.localvalu.deliveryorder.di.deliveryhistory.DeliveryHistoryDH
import io.reactivex.disposables.CompositeDisposable

class DeliveryHistoryViewModel(
    private val deliveryHistoryRepository: DeliveryHistoryDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    //DeliveryHistory
    val deliveryHistoryOutcome: LiveData<Outcome<DeliveryHistoryResponse>> by lazy {
        //Convert publish subject to livedata
        deliveryHistoryRepository.deliveryHistoryFetchOutcome.toLiveData(compositeDisposable)
    }

    /**
     * @param - fromDate - 2020-03-12 - yyyy-MM-dd
     * @param - toDate - 2020-03-13 - yyyy-MM-dd
     */
    fun deliveryHistory(driverId: String,fromDate:String,toDate:String)
    {
        deliveryHistoryRepository.getRemoteDeliveryHistory(DeliveryHistoryRequest("",driverId,fromDate,toDate))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        DeliveryHistoryDH.deliveryHistoryComponent()
    }
}