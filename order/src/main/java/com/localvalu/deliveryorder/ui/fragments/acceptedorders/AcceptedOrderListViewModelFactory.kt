package com.localvalu.deliveryorder.ui.fragments.acceptedorders

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryorder.data.acceptedorders.AcceptedOrderListDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ForgetPasswordViewModel.
 * Required given ForgetPasswordViewModel has a non-empty constructor
 */
class AcceptedOrderListViewModelFactory(
    private val repository: AcceptedOrderListDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return AcceptedOrderListViewModel(
            repository,
            compositeDisposable
        ) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
