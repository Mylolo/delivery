package com.localvalu.deliveryorder.ui.fragments.fullorderdetails

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.OrderItemDetails
import kotlinx.android.synthetic.main.view_item_ordered_items.view.*


class OrderedItemsAdapter():ListAdapter<OrderItemDetails,OrderedItemsAdapter.OrderItemViewHolder>(OrderItemDetailsDC())
{
    private lateinit var context:Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder =
        OrderItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_item_ordered_items,
            parent,false))

    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int) = holder.bind(getItem(position))

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context=recyclerView.context;
    }

    inner class OrderItemViewHolder( itemView: View)
        :RecyclerView.ViewHolder(itemView)
    {
        private val strCurrencySymbol = context.getString(R.string.lbl_currency_symbol_euro);

        fun bind(orderItemDetails:OrderItemDetails) = with(itemView) {
            Log.d("OrderItemViewHolder","bindItem")
            itemView.tvItemName.text = (orderItemDetails.itemName)
            val singleItemPrice:Double = orderItemDetails.singleQuantityPrice.toDouble()
            val strSingleItemPrice = "%.2f".format(singleItemPrice)
            val strQtySinglePrice:StringBuilder  = StringBuilder();
            strQtySinglePrice.append(strCurrencySymbol).append(strSingleItemPrice).append(" x ").append(orderItemDetails.quantity);
            itemView.tvQuantity.text = (strQtySinglePrice.toString());

            val itemTotalPrice:Double = orderItemDetails.itemTotalPrice.toDouble()
            val strItemTotalPrice = "%.2f".format(itemTotalPrice)
            val strTotalPrice:StringBuilder  = StringBuilder();
            strTotalPrice.append(strCurrencySymbol).append(strItemTotalPrice);
            itemView.tvPrice.text = (strTotalPrice.toString());
        }
    }

    fun swapData(data: List<OrderItemDetails>) {
        submitList(data.toMutableList())
    }

    private class OrderItemDetailsDC : DiffUtil.ItemCallback<OrderItemDetails>()
    {
        override fun areItemsTheSame(oldItem: OrderItemDetails, newItem: OrderItemDetails) = oldItem.orderDetailsId == newItem.orderDetailsId

        override fun areContentsTheSame(oldItem: OrderItemDetails, newItem: OrderItemDetails) = oldItem == newItem
    }

}