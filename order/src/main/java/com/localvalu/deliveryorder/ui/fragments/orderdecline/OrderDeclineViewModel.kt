package com.localvalu.deliveryorder.ui.fragments.orderdecline

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.constants.Constants.DRIVER_ORDER_DECLINE
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineDataContract
import com.localvalu.deliveryorder.di.orderdecline.OrderDeclineDH
import io.reactivex.disposables.CompositeDisposable

class OrderDeclineViewModel(
    private val orderDeclineRepository: OrderDeclineDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    //Decline Order
    val declineOrderOutcomeOne: LiveData<Outcome<AcceptOrderResponseOne>> by lazy {
        //Convert publish subject to livedata
        orderDeclineRepository.declineOrderFetchOutcomeOne.toLiveData(compositeDisposable)
    }
    // Create a LiveData with a String
    var validReason: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    // Update if order declined in list page:
    val dataToShare = MutableLiveData<String>()

    fun declineOrder(driverId: String, orderId: String, merchantId: String, reason: String)
    {
        orderDeclineRepository.requestDeclineOrder(
            OrderAcceptRequestOne(
                "", driverId, orderId, DRIVER_ORDER_DECLINE,
                reason, merchantId.toInt()
            )
        )
    }

    fun updateData(data: String)
    {
        dataToShare.value = data
    }

    // A placeholder username validation check
    public fun isReasonValid(reason: String)
    {
        validReason.value = reason.isEmpty()
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        OrderDeclineDH.destroyOrderDeclineComponent()
    }
}