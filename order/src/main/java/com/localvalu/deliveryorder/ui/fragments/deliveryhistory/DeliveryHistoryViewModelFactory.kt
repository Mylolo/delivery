package com.localvalu.deliveryorder.ui.fragments.deliveryhistory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryDataContract
import com.localvalu.deliveryorder.data.driverdashboard.DriverDashboardDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate DeliveryHistoryViewModelFactory.
 * Required given DeliveryHistoryViewModelFactory has a non-empty constructor
 */
class DeliveryHistoryViewModelFactory(
    private val repository: DeliveryHistoryDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return DeliveryHistoryViewModel(
            repository,
            compositeDisposable
        ) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
