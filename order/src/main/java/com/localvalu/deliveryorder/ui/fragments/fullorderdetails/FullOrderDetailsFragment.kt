package com.localvalu.deliveryorder.ui.fragments.fullorderdetails

import android.Manifest
import android.Manifest.permission.CALL_PHONE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.Uri
import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.localvalu.deliverycore.extensions.getDisplayFormatDateFromString
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.FullOrderDetailsResponse
import com.localvalu.deliveryorder.data.model.response.fullorderdetails.OrderAddressDetails
import com.localvalu.deliveryorder.di.fullorderdetails.FullOrderDetailsDH
import kotlinx.android.synthetic.main.fragment_order_details.*
import kotlinx.android.synthetic.main.toolbar_order.*
import java.io.IOException
import javax.inject.Inject

/* 0 - Order initial stage
1 - Order Accepted by Driver
2 - Order Received from Merchant
3 - Order Delivered to Consumer
4 - Driver Accepted order not delivered yet */
class FullOrderDetailsFragment : Fragment()
{

    private val TAG = "OrderDetailsFragment"

    private val component by lazy { FullOrderDetailsDH.fullOrderDetailsComponent() }

    @Inject
    lateinit var viewModelFactory: FullOrderDetailsViewModelFactory

    @Inject
    lateinit var orderedItemAdapter: OrderedItemsAdapter

    private val viewModel: FullOrderDetailsViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(FullOrderDetailsViewModel::class.java)
    }

    private val MULTIPLE_PERMISSIONS = 10 // code you want.


    private lateinit var strCurrencySymbol:String

    private var driverId = "-1"
    private var orderId = "-1"
    private var PERMISSION_ALL = 1
    private var PERMISSIONS = arrayOf<String>(Manifest.permission.CALL_PHONE)
    private var permissionGranted=false;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro)
        if(checkAndRequestPermissions())
        {
            // carry on the normal flow, as the case of  permissions  granted.
            permissionGranted=true
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_order_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        Log.d(TAG, "arguments ")
        setProperties()
    }

    private fun setProperties()
    {
        rvOrderedItems.apply {
            this.layoutManager = LinearLayoutManager(context)
            this.adapter = orderedItemAdapter
            this.isNestedScrollingEnabled=false
        }
        initiateOrderDetailsListener();
        arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let {
            driverId=it
        }
        arguments?.getString(BundleUtils.BUNDLE_ORDER_ID)?.let {
            orderId=it
            viewModel.fullOrderDetails(driverId,orderId)
        }

    }

    private fun callPhone(strPhoneNumber:String)
    {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + strPhoneNumber))
        startActivity(intent)
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("driverDashboardFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_order_details)
    }

    private fun initiateOrderDetailsListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.fullOrderDetailsOutcome.observe(viewLifecycleOwner, Observer<Outcome<FullOrderDetailsResponse>> { outcome ->
            Log.d(TAG, "initiateOrderDetailsListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {
                        cnlOrderItemDetails.visibility = View.GONE
                        progressBarFragmentOrderDetails.visibility = View.VISIBLE
                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "initiateOrderDetailsListener: Successfully loaded data")
                    progressBarFragmentOrderDetails.visibility = View.GONE
                    if(outcome.data.fullOrderResults.errorDetails.errorCode==0)
                    {
                        cnlOrderItemDetails.visibility=View.VISIBLE
                        orderedItemAdapter.swapData(outcome.data.fullOrderResults.fullOrderDetailsResult.orderItemDetails)
                        setData(outcome.data.fullOrderResults.fullOrderDetailsResult.orderAddressDetails.get(0))
                    }
                    else
                    {
                        Toast.makeText(context,""+outcome.data.fullOrderResults.errorDetails.errorMessage,
                            Toast.LENGTH_LONG).show()
                        cnlOrderItemDetails.visibility=View.VISIBLE;
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {
                        progressBarFragmentOrderDetails.visibility = View.GONE
                        cnlOrderItemDetails.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_no_internet_connection),
                            Toast.LENGTH_LONG).show()
                    }

                    else
                    {
                        progressBarFragmentOrderDetails.visibility = View.GONE
                        cnlOrderItemDetails.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_unknown_error_try_again),
                            Toast.LENGTH_LONG).show()
                    }
                }

            }
        })
    }

    private fun setData(orderAddressDetails: OrderAddressDetails)
    {
       orderAddressDetails.let {
           tvOrderIdFullOrderDetails.text = it.OrderId
           tvOrderDateTimeFullOrderDetails.text = getDisplayFormatDateFromString(it.createdDate)
           tvCustomerNameFullOrderDetails.text=it.loginUserName

           val sbCustomerAddress:StringBuilder = StringBuilder();
           sbCustomerAddress.append(it.userAddress1).append("\n").append(it.userAddress2).append("\n")
               .append(it.userCity).append(it.userPostcode)
           tvCustomerAddressFullOrderDetails.text=sbCustomerAddress.toString()

           tvRestaurantNameFullOrderDetails.text=it.tradingName

           val sbRestaurantAddress:StringBuilder = StringBuilder();
           sbRestaurantAddress.append(it.address1).append("\n").append(it.address2).append("\n").append(it.postCode)
           tvRestaurantAddressFullOrderDetails.text=sbRestaurantAddress.toString()

           val sbTotalAmount:StringBuilder = StringBuilder();
           sbTotalAmount.append(strCurrencySymbol).append(it.payableVatAmount)
           tvTotalAmount.text = sbTotalAmount.toString()

           tvOrderTypeFullOrderDetails.text=it.orderType

           val strCustomerNumber = it.userPhone
           val strRestaurantNumber = it.contactPersonPhone

           btnCallCustomer.setOnClickListener(View.OnClickListener {
               if(permissionGranted)callPhone(strCustomerNumber)
           })
           btnCallRestaurant.setOnClickListener(View.OnClickListener {
               if(permissionGranted)callPhone(strRestaurantNumber)
           })
       }
    }

    private fun checkAndRequestPermissions(): Boolean
    {
        var result: Int
        var listPermissionsNeeded: ArrayList<String> = ArrayList()
        for (p in PERMISSIONS)
        {
            result = ContextCompat.checkSelfPermission(requireActivity(), p)

            if (result != PackageManager.PERMISSION_GRANTED)
            {
                listPermissionsNeeded.add(p)
            }
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            // converting ArrayList to Array
            val permissionsArray: Array<String> = listPermissionsNeeded.toTypedArray()
            ActivityCompat.requestPermissions(requireActivity(),permissionsArray,MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissionsList: Array<String>,
        grantResults: IntArray
    )
    {
        when (requestCode)
        {
            MULTIPLE_PERMISSIONS ->
            {
                val perms: MutableMap<String, Int> = HashMap()
                // Initialize the map with both permissions
                // Initialize the map with both permissions
                perms[Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED

                if (grantResults.size > 0)
                {
                    var i: Int = 0;

                    for (perm in permissionsList)
                    {
                        perms.put(permissionsList[i], grantResults[i])
                    }
                    if (perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                    {
                        Log.d(TAG, "sms & location services permission granted");
                        permissionGranted = true
                    }
                    else
                    {

                        Log.d(TAG, "Some permissions are not granted ask again ");
                        permissionGranted = false
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale( requireActivity(), Manifest.permission.CALL_PHONE))
                        {

                        }
                        else
                        {
                            Toast.makeText(requireContext(), "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
                return
            }
        }
    }
}