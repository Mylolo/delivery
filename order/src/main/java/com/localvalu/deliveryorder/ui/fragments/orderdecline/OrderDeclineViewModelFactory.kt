package com.localvalu.deliveryorder.ui.fragments.orderdecline

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryorder.data.driverdashboard.DriverDashboardDataContract
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ForgetPasswordViewModel.
 * Required given ForgetPasswordViewModel has a non-empty constructor
 */
class OrderDeclineViewModelFactory(
    private val repository: OrderDeclineDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return OrderDeclineViewModel(
            repository,
            compositeDisposable
        ) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
