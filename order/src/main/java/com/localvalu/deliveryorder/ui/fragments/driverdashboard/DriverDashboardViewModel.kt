package com.localvalu.deliveryorder.ui.fragments.driverdashboard

import androidx.arch.core.util.Function
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.localvalu.deliverycore.constants.Constants.DRIVER_ORDER_ACCEPT
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Event
import com.localvalu.deliveryorder.data.driverdashboard.DriverDashboardDataContract
import com.localvalu.deliveryorder.data.model.request.DriverDashboardRequest
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestOne
import com.localvalu.deliveryorder.data.model.request.OrderAcceptRequestTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardResponse
import com.localvalu.deliveryorder.di.driverdashboard.DriverDashboardDH
import io.reactivex.disposables.CompositeDisposable

class DriverDashboardViewModel(
    private val driverDashboardRepository: DriverDashboardDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{

    //Driver Dashboard
    private val driverDashboardOutcome: LiveData<Outcome<DriverDashboardResponse>> by lazy {
        //Convert publish subject to livedata
        driverDashboardRepository.driverDashboardFetchOutcome.toLiveData(compositeDisposable)
    }

    val transformedDriverDashboardOutcome: LiveData<Event<Outcome<DriverDashboardResponse>>> =
        Transformations.map(driverDashboardOutcome, Function {
            Event(it)
        })

    //Accept Order One
    val acceptOrderOutcomeOne: LiveData<Outcome<AcceptOrderResponseOne>> by lazy {
        //Convert publish subject to livedata
        driverDashboardRepository.acceptOrderFetchOutcomeOne.toLiveData(compositeDisposable)
    }

    val transformedAcceptOrderOutcomeOne: LiveData<Event<Outcome<AcceptOrderResponseOne>>> =
        Transformations.map(acceptOrderOutcomeOne, Function {
            Event(it)
        })

    //Accept Order Two
    val acceptOrderOutcomeTwo: LiveData<Outcome<AcceptOrderResponseTwo>> by lazy {
        //Convert publish subject to livedata
        driverDashboardRepository.acceptOrderFetchOutcomeTwo.toLiveData(compositeDisposable)
    }

    val transformedAcceptOrderOutcomeTwo: LiveData<Event<Outcome<AcceptOrderResponseTwo>>> =
        Transformations.map(acceptOrderOutcomeTwo, Function {
            Event(it)
        })

    // Update if order declined in list page:
    val dataToShare = MutableLiveData<String>()

    fun driverDashboard(driverId: String)
    {
        driverDashboardRepository.getRemoteDriverDashboard(DriverDashboardRequest("",driverId))
    }

    fun acceptOrderOne(driverId: String,orderId:String,merchantId:String)
    {
        driverDashboardRepository.requestAcceptOrderOne(OrderAcceptRequestOne("",driverId,orderId,DRIVER_ORDER_ACCEPT,
            "",merchantId.toInt()))
    }

    fun acceptOrderTwo(driverId: String,orderId:String,merchantId:String,accountId:String,kiloMeter:String,amount:String,
                       payMode:String,deliveryAmount:String,duration:String)
    {
        driverDashboardRepository.requestAcceptOrderTwo(OrderAcceptRequestTwo("",driverId,orderId,merchantId,
            accountId,kiloMeter,amount,payMode,deliveryAmount,duration))
    }

    fun updateData(data: String) {
        dataToShare.value = data
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        DriverDashboardDH.destroyDriverDashboardComponent()
    }
}