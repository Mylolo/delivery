package com.localvalu.deliveryorder.ui.fragments.acceptedorders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.deliverycore.constants.Constants
import com.localvalu.deliverycore.extensions.getDisplayFormatDateFromString
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.acceptedorderlist.AcceptedOrderDetails
import kotlinx.android.synthetic.main.view_item_accepted_order_list.view.*

class AcceptedOrderListAdapter():ListAdapter<AcceptedOrderDetails,AcceptedOrderListAdapter.AcceptedOrderViewHolder>(AcceptedOrderListDC())
{
    var orderItemClickListener: OrderItemClickListener? = null

    private lateinit var context:Context;

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context=recyclerView.context;

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AcceptedOrderViewHolder =
        AcceptedOrderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_item_accepted_order_list,
            parent,false),orderItemClickListener)

    override fun onBindViewHolder(holder: AcceptedOrderViewHolder, position: Int) = holder.bind(getItem(position))


    inner class AcceptedOrderViewHolder( itemView: View,private val orderItemClickListener: OrderItemClickListener?)
        :RecyclerView.ViewHolder(itemView),View.OnClickListener
    {
        private lateinit var tempAcceptedOrderDetails:AcceptedOrderDetails;

        init
        {
            itemView.btnActionAcceptedOrderListItem.setOnClickListener(this)
            itemView.cnlOrderDetails.setOnClickListener(this)
        }
        override fun onClick(v: View?)
        {
            val clicked = getItem(adapterPosition)

            when(v?.id)
            {
                R.id.btnActionAcceptedOrderListItem ->
                {
                    when(tempAcceptedOrderDetails?.orderStatusId?.toInt())
                    {
                        Constants.STATUS_ORDER_ACCEPTED_BY_DRIVER ->
                        {
                            orderItemClickListener?.receiveOrderFromMerchantClicked(clicked,adapterPosition)
                        }
                        Constants.STATUS_ORDER_RECEIVED_FROM_MERCHANT ->
                        {
                            orderItemClickListener?.completeClicked(clicked,adapterPosition)
                        }
                    }

                }
                R.id.cnlOrderDetails -> orderItemClickListener?.itemClicked(clicked,adapterPosition)
            }
        }
        fun bind(acceptedOrderDetails:AcceptedOrderDetails) = with(itemView) {
            tempAcceptedOrderDetails = acceptedOrderDetails
            itemView.tvOrderId.text = acceptedOrderDetails.orderId
            itemView.tvMerchantName.text = acceptedOrderDetails.tradingName
            itemView.tvAcceptedDateTime.text = getDisplayFormatDateFromString(acceptedOrderDetails.createdDate)
            when(acceptedOrderDetails.orderStatusId.toInt())
            {
                Constants.STATUS_ORDER_ACCEPTED_BY_DRIVER ->
                {
                    itemView.btnActionAcceptedOrderListItem.text = context.getString(R.string.lbl_order_received_from_merchant)
                }
                Constants.STATUS_ORDER_RECEIVED_FROM_MERCHANT ->
                {
                    itemView.btnActionAcceptedOrderListItem.text = context.getString(R.string.lbl_complete)
                }
            }

            if(acceptedOrderDetails.performActionRequest)
            {
                itemView.btnActionAcceptedOrderListItem.isEnabled=false
                itemView.progressBarActionAcceptedOrderListItem.visibility=View.VISIBLE
            }
            else
            {
                itemView.btnActionAcceptedOrderListItem.isEnabled=true
                itemView.progressBarActionAcceptedOrderListItem.visibility=View.INVISIBLE
            }
        }
    }

    fun swapData(data: List<AcceptedOrderDetails>) {
        submitList(data.toMutableList())
    }

    interface OrderItemClickListener
    {
        fun completeClicked(acceptedOrderDetails: AcceptedOrderDetails, position:Int)
        fun receiveOrderFromMerchantClicked(acceptedOrderDetails: AcceptedOrderDetails, position:Int)
        fun itemClicked(acceptedOrderDetails: AcceptedOrderDetails, position:Int)
    }

    private class AcceptedOrderListDC : DiffUtil.ItemCallback<AcceptedOrderDetails>()
    {
        override fun areItemsTheSame(oldItem: AcceptedOrderDetails, newItem: AcceptedOrderDetails) = oldItem.orderId == newItem.orderId

        override fun areContentsTheSame(oldItem: AcceptedOrderDetails, newItem: AcceptedOrderDetails) = oldItem == newItem
    }

}