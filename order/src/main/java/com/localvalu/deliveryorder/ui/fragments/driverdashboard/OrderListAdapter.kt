package com.localvalu.deliveryorder.ui.fragments.driverdashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.deliverycore.constants.Constants
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import kotlinx.android.synthetic.main.view_item_order_list.view.*
import kotlinx.android.synthetic.main.view_item_order_list.view.tvCustomerName
import kotlinx.android.synthetic.main.view_item_order_list.view.tvStatus
import java.lang.StringBuilder

/* 0 - Order initial stage
1 - Order Accepted by Driver
2 - Order Received from Merchant
3 - Order Delivered to Consumer
4 - Driver Accepted order not delivered yet */
class OrderListAdapter():ListAdapter<DriverDashboardData,OrderListAdapter.OrderViewHolder>(DriverDashboardDC())
{
    var orderItemClickListener: OrderItemClickListener? = null

    private lateinit var context:Context;

    private lateinit var acceptingOrder:String;
    private lateinit var acceptOrder:String;
    private lateinit var decliningOrder:String;
    private lateinit var declineOrder:String;


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context=recyclerView.context;
        acceptingOrder=context.getString(R.string.lbl_accepting_order)
        acceptOrder=context.getString(R.string.lbl_accept)
        decliningOrder=context.getString(R.string.lbl_declining_order)
        declineOrder=context.getString(R.string.lbl_decline)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder =
        OrderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_item_order_list,
            parent,false),orderItemClickListener)

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) = holder.bind(getItem(position))

    inner class OrderViewHolder( itemView: View,private val orderItemClickListener: OrderItemClickListener?)
        :RecyclerView.ViewHolder(itemView),View.OnClickListener
    {
        init
        {
            itemView.btnAccept.setOnClickListener(this)
            itemView.btnDecline.setOnClickListener(this)
            itemView.cvOrderItemDetails.setOnClickListener(this)
        }
        override fun onClick(v: View?)
        {
            val clicked = getItem(adapterPosition)

            when(v?.id)
            {
                R.id.btnAccept -> orderItemClickListener?.acceptClicked(clicked,adapterPosition)
                R.id.btnDecline -> orderItemClickListener?.rejectClicked(clicked,adapterPosition)
                R.id.cvOrderItemDetails -> orderItemClickListener?.itemClicked(clicked,adapterPosition)
            }
        }
        fun bind(driverDashboardData: DriverDashboardData) = with(itemView) {

            itemView.tvOrderId.text = driverDashboardData.orderId
            when(driverDashboardData.orderStatus)
            {
                Constants.ORDER_PLACED -> itemView.tvStatus.text =context.getString(R.string.lbl_order_placed);
                Constants.ORDER_PENDING -> itemView.tvStatus.text =context.getString(R.string.lbl_pending);
                Constants.ORDER_ACCEPTED -> itemView.tvStatus.text = context.getString(R.string.lbl_accepted);
                Constants.ORDER_DELIVERED -> itemView.tvStatus.text =context.getString(R.string.lbl_delivered);
            }

            itemView.tvCustomerName.text = driverDashboardData.CustomerName
            itemView.tvRestaurantName.text = driverDashboardData.tradingName
            val sbCustomerAddress = StringBuilder()
            sbCustomerAddress.append(driverDashboardData.customerAddress)
            itemView.tvCustomerAddressDashboard.text = sbCustomerAddress.toString()

            val sbRestaurantAddress = StringBuilder()
            sbRestaurantAddress.append(driverDashboardData.restaurantAddress).append(" ")
                .append(driverDashboardData.restaurantPostalCode);
            itemView.tvRestaurantAddress.text = sbRestaurantAddress.toString()

            if(driverDashboardData.performAcceptRequest)
            {
                itemView.btnAccept.isEnabled=false
                itemView.btnDecline.isEnabled=false
            }
            else if(driverDashboardData.performDeclineRequest)
            {
                itemView.btnAccept.isEnabled=false
                itemView.btnDecline.isEnabled=false
            }
            else
            {
                itemView.btnAccept.isEnabled=true
                itemView.btnDecline.isEnabled=true
            }
        }
    }

    fun swapData(data: List<DriverDashboardData>) {
        submitList(data.toMutableList())
    }

    interface OrderItemClickListener
    {
        fun acceptClicked(driverDashboardData: DriverDashboardData, position:Int)
        fun rejectClicked(driverDashboardData: DriverDashboardData, position:Int)
        fun itemClicked(driverDashboardData: DriverDashboardData, position:Int)
    }

    private class DriverDashboardDC : DiffUtil.ItemCallback<DriverDashboardData>()
    {
        override fun areItemsTheSame(oldItem: DriverDashboardData, newItem: DriverDashboardData) = oldItem.orderId == newItem.orderId

        override fun areContentsTheSame(oldItem: DriverDashboardData, newItem: DriverDashboardData) = oldItem == newItem
    }

}