package com.localvalu.deliveryorder.ui.fragments.driverdashboard

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverycore.networkhandler.Event
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseOne
import com.localvalu.deliveryorder.data.model.response.driverdashboard.AcceptOrderResponseTwo
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardResponse
import com.localvalu.deliveryorder.di.driverdashboard.DriverDashboardDH
import kotlinx.android.synthetic.main.fragment_order_list.*
import kotlinx.android.synthetic.main.toolbar_order.*
import java.io.IOException
import java.lang.Runnable
import javax.inject.Inject

/**
 *  Accept = status = 2
 *  Decline = status = 5
 */
class OrderListFragment : Fragment(), OrderListAdapter.OrderItemClickListener
{

    private val TAG = "OrderListFragment"

    private val B_ORDER_LIST = "B_ORDER_LIST"

    private val component by lazy { DriverDashboardDH.driverDashboardComponent() }

    @Inject
    lateinit var viewModelFactory: DriverDashboardViewModelFactory

    @Inject
    lateinit var adapter: OrderListAdapter

    @Inject
    lateinit var contextTool: Context

    private val viewModel: DriverDashboardViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(DriverDashboardViewModel::class.java)
    }

    private var driverId = "-1"

    private var listShown = false;



    private lateinit var orderListGlobal: MutableList<DriverDashboardData>

    private lateinit var selectedOrder: DriverDashboardData

    private var selectedPosition: Int = -1

    private val mHandler = object : Handler(Looper.getMainLooper())
    {

    }

    private val runnable = Runnable {
        getOrdersList()
    }

    private var currentOrderListSize:Int=0

    private var previousOrderListSize:Int=0

    private var firstTime = true

    private fun getOrdersList()
    {
        viewModel.driverDashboard(driverId)
        mHandler.postDelayed(runnable,10000)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        setUpResultListener()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        adapter.orderItemClickListener = this
        rvOrderList.layoutManager = LinearLayoutManager(context);
        rvOrderList.adapter = adapter
        btnRetry.setOnClickListener {
            viewModel.driverDashboard(driverId)
        }

        initDriverDashboardDataListener()
        initAcceptOrderTwoDataListener()
        initAcceptOrderOneDataListener()
        Log.d(TAG, "arguments ")
        arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let {
            driverId = it
            viewModel.driverDashboard(driverId)
        }
        //mHandler.postDelayed(runnable,10000)
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId =
            resources.getIdentifier("driverDashboardFragment", "id", "com.localvalu.deliveryapp")
        val drawerLayoutID =
            resources.getIdentifier("drawer_layout", "id", "com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        navController.graph.startDestination = fragmentId;
        val appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(
            ContextCompat.getColor(requireContext(), R.color.colorWhite),
            PorterDuff.Mode.SRC_ATOP
        )
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        val drawableAction: Drawable? =
            ContextCompat.getDrawable(contextTool, R.drawable.ic_baseline_refresh_24);
        drawableAction?.colorFilter = porterDuffColorFilter
        actionButton.setImageDrawable(drawableAction)
        //actionButton.visibility = View.GONE
        actionButton.setOnClickListener {
            viewModel.driverDashboard(driverId)
        }
        tvToolBarTitle.text = getString(R.string.lbl_dashboard)
    }

    private fun setUpResultListener()
    {
        setFragmentResultListener(BundleUtils.KEY_ORDER_DECLINED_REQUEST) { requestKey, result ->
            onFragmentResult(requestKey, result)
        }
    }

    private fun onFragmentResult(requestKey: String, result: Bundle)
    {
        val orderDeclined = result.getBoolean(BundleUtils.BUNDLE_ORDER_DECLINED);
        if (orderDeclined)
        {
            //viewModel.driverDashboardOutcome.removeObservers(viewLifecycleOwner)
            //initiateDriverDashboardDataListener()
            //viewModel.driverDashboard(driverId)
        }
    }

    private fun initDriverDashboardDataListener()
    {
        viewModel.transformedDriverDashboardOutcome.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.transformedDriverDashboardOutcome.observe(
            viewLifecycleOwner,
            Observer<Event<Outcome<DriverDashboardResponse>>> {
                it.getContentIfNotHandled()?.let {
                    Log.d(TAG, "initDriverDashboardDataListener: $it")
                    when (it)
                    {
                        is Outcome.Progress ->
                        {
                            if (it.loading)
                            {
                                showProgress()
                            }
                        }

                        is Outcome.Success ->
                        {
                            Log.d(TAG, "initDriverDashboardDataListener: Successfully loaded data")
                            if (it.data.driverDashboardResult.errorDetails.errorCode == 0)
                            {
                                setList(it.data.driverDashboardResult.driverDashboardList);
                            }
                            else
                            {
                                //showErrorMessage(outcome.data.driverDashboardResult.errorDetails.errorMessage,false,1)
                                showInitialError(
                                    it.data.driverDashboardResult.errorDetails.errorMessage,
                                    false
                                );
                            }
                        }

                        is Outcome.Failure ->
                        {

                            if (it.e is IOException)
                            {
                                //showErrorMessage(getString(R.string.msg_no_internet_connection),true,1)
                                showInitialError(
                                    getString(R.string.msg_no_internet_connection),
                                    true
                                );
                            }
                            else
                            {
                                //showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,1)
                                showInitialError(
                                    getString(R.string.msg_unknown_error_try_again),
                                    true
                                );
                            }
                        }
                    }
                }

            })
    }

    private fun initAcceptOrderOneDataListener()
    {
        viewModel.transformedAcceptOrderOutcomeOne.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.transformedAcceptOrderOutcomeOne.observe(viewLifecycleOwner,
            Observer<Event<Outcome<AcceptOrderResponseOne>>> {
                it.getContentIfNotHandled().let {
                    when (it)
                    {
                        is Outcome.Progress ->
                        {
                            if (it.loading)
                            {
                                acceptProgress(true)
                            }
                        }
                        is Outcome.Success ->
                        {
                            Log.d(TAG, "initAcceptOrderTwoDataListener: Successfully loaded data")

                            acceptProgress(false)

                            if (it.data.acceptOrderResultOne.errorDetails.errorCode == 0)
                            {
                                //acceptSuccess(getString(R.string.lbl_you_have_accepted_the_order))
                                acceptOrderTwo()
                            }
                            else
                            {
                                showErrorMessage(
                                    it.data.acceptOrderResultOne.errorDetails.errorMessage,
                                    false,
                                    2
                                )
                            }
                        }
                        is Outcome.Failure ->
                        {

                            acceptProgress(false)

                            if (it.e is IOException)
                            {
                                showErrorMessage(
                                    getString(R.string.msg_no_internet_connection),
                                    true,
                                    2
                                )
                            }
                            else
                            {
                                showErrorMessage(
                                    getString(R.string.msg_unknown_error_try_again),
                                    true,
                                    2
                                )
                            }
                        }
                    }
                }


            })
    }

    private fun initAcceptOrderTwoDataListener()
    {
        viewModel.transformedAcceptOrderOutcomeTwo.removeObservers(viewLifecycleOwner)
        //Observe the outcome and update state of the screen  accordingly
        viewModel.transformedAcceptOrderOutcomeTwo.observe(
            viewLifecycleOwner,
            Observer<Event<Outcome<AcceptOrderResponseTwo>>> { it.getContentIfNotHandled().let {
                Log.d(TAG, "initAcceptOrderTwoDataListener: $it")
                when (it)
                {
                    is Outcome.Progress ->
                    {
                        if (it.loading)
                        {
                            acceptProgress(true)
                        }
                    }
                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initAcceptOrderTwoDataListener: Successfully loaded data")

                        acceptProgress(false)

                        if (it.data.acceptOrderResultTwo.errorDetails.errorCode == 0)
                        {
                            acceptSuccess(getString(R.string.lbl_you_have_accepted_the_order))
                        }
                        else
                        {
                            showErrorMessage(
                                it.data.acceptOrderResultTwo.errorDetails.errorMessage,
                                false,
                                3
                            )
                        }
                    }
                    is Outcome.Failure ->
                    {

                        acceptProgress(false)

                        if (it.e is IOException)
                        {
                            showErrorMessage(
                                getString(R.string.msg_no_internet_connection),
                                true,
                                3
                            )
                        }
                        else
                        {
                            showErrorMessage(
                                getString(R.string.msg_unknown_error_try_again),
                                true,
                                3
                            )
                        }
                    }
                }
            }

            })
    }

    private fun showProgress()
    {
        rvOrderList.visibility = View.GONE
        progressBarFragmentOrderList.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
    }

    private fun setList(driverDashboardDataList: List<DriverDashboardData>)
    {
        Log.d(TAG,"setList")
        //previousOrderListSize=currentOrderListSize
        orderListGlobal = driverDashboardDataList as MutableList<DriverDashboardData>
        /*currentOrderListSize = orderListGlobal.size
        if(previousOrderListSize==currentOrderListSize && firstTime)
        {
            Log.d(TAG,"setList - firstTime->" + firstTime)
            adapter.swapData(orderListGlobal)
            firstTime = false
            showList()
        }
        else if(previousOrderListSize!=currentOrderListSize)
        {
            Log.d(TAG,"setList -previousOrderListSize!=currentOrderListSize")
            adapter.swapData(orderListGlobal)
            showList()
        }
        else if(previousOrderListSize==0 && currentOrderListSize==0)
        {
            Log.d(TAG,"setList -previousOrderListSize==0 && currentOrderListSize==0")
            showInitialError(getString(R.string.lbl_no_orders),false)
        }*/
        adapter.swapData(orderListGlobal)
        showList()
    }

    private fun showList()
    {
        listShown = true;
        rvOrderList.visibility = View.VISIBLE
        progressBarFragmentOrderList.visibility = View.GONE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialError(message: String, retry: Boolean)
    {
        progressBarFragmentOrderList.visibility = View.GONE
        errorContainer.visibility = View.VISIBLE
        rvOrderList.visibility = View.GONE
        tvError.text = message;
        if (retry) btnRetry.visibility = View.VISIBLE
        else btnRetry.visibility = View.GONE
    }

    private fun showErrorMessage(message: String, retry: Boolean, apiResponseFrom: Int)
    {
        val mySnackbar = Snackbar.make(
            clFragmentOrderList,
            message, Snackbar.LENGTH_INDEFINITE
        )
        mySnackbar.setBackgroundTint(
            ContextCompat.getColor(
                contextTool,
                R.color.colorBackground
            )
        )
        if (retry) mySnackbar.setAction(R.string.lbl_retry, View.OnClickListener { v ->
            when (apiResponseFrom)
            {
                1 -> viewModel.driverDashboard(driverId)
                2 -> acceptOrderOne()
                3 -> acceptOrderTwo()
            }
            mySnackbar.dismiss()
        })
        else mySnackbar.setAction(R.string.lbl_ok, View.OnClickListener { v ->
            mySnackbar.dismiss()
        })
        mySnackbar.show()
        selectedOrder = DriverDashboardData()
        selectedOrder?.let {
            it.performAcceptRequest = false
            it.performDeclineRequest = false
        }
    }

    private fun acceptProgress(isUpdating: Boolean)
    {
        selectedOrder.let {
            it.performAcceptRequest = isUpdating
            it.performDeclineRequest = false

            if (orderListGlobal.size > 0)
            {
                if (selectedPosition < orderListGlobal.size)
                {
                    orderListGlobal[selectedPosition] = it
                    adapter.swapData(orderListGlobal)
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun acceptSuccess(message: String)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        viewModel.driverDashboard(driverId)
    }

    override fun acceptClicked(driverDashboardData: DriverDashboardData, position: Int)
    {
        selectedOrder = driverDashboardData;
        selectedPosition = position;
        acceptOrderOne()
    }

    private fun acceptOrderOne()
    {
        viewModel.acceptOrderOne(driverId, selectedOrder.orderId, selectedOrder.retailerId)
    }

    private fun acceptOrderTwo()
    {
        viewModel.acceptOrderTwo(
            driverId, selectedOrder.orderId, selectedOrder.retailerId,
            selectedOrder.accountId, selectedOrder.km, selectedOrder.payableAmount,
            selectedOrder.orderType, selectedOrder.deliveryRate, selectedOrder.duration
        )
    }

    override fun rejectClicked(driverDashboardData: DriverDashboardData, position: Int)
    {
        selectedOrder = driverDashboardData;
        selectedPosition = position;
        gotoOrderDeclineFragment()
    }

    override fun itemClicked(driverDashboardData: DriverDashboardData, position: Int)
    {
        gotoFullOrderDetailsFragment(driverDashboardData.orderId);
    }

    private fun gotoFullOrderDetailsFragment(orderId: String)
    {
        val navGraphId =
            resources.getIdentifier("nav_graph", "navigation", "com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier(
            "action_driverDashboardFragment_to_actionFullOrderDetailsFragment",
            "id", "com.localvalu.deliveryapp"
        )
        var bundle = bundleOf(
            BundleUtils.BUNDLE_DRIVER_ID to driverId,
            BundleUtils.BUNDLE_ORDER_ID to orderId
        )
        if (navGraphId != 0)
        {
            findNavController().navigate(actionId, bundle)
        }
    }

    private fun gotoOrderDeclineFragment()
    {

        val navGraphId =
            resources.getIdentifier("nav_graph", "navigation", "com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier(
            "action_driverDashboardFragment_to_actionOrderDeclineFragment",
            "id", "com.localvalu.deliveryapp"
        )
        var bundle = bundleOf(
            BundleUtils.BUNDLE_DRIVER_ID to driverId,
            BundleUtils.BUNDLE_DRIVER_DASHBOARD_DATA to selectedOrder
        )
        if (navGraphId != 0)
        {
            findNavController().navigate(actionId, bundle)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.order_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val id = item!!.itemId
        //handle item clicks
        if (id == R.id.refresh)
        {
            viewModel.driverDashboard(driverId)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView()
    {
        //mHandler.removeCallbacks(runnable)
        super.onDestroyView()
    }

}



