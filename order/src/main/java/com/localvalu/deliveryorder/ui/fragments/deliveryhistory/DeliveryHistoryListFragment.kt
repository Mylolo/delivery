package com.localvalu.deliveryorder.ui.fragments.deliveryhistory

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.localvalu.deliverycore.extensions.getDisplayFormatDate
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliveryorder.BuildConfig
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DeliveryHistoryResponse
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DriverOrderDetails
import com.localvalu.deliveryorder.data.model.response.driverdashboard.DriverDashboardData
import com.localvalu.deliveryorder.di.deliveryhistory.DeliveryHistoryDH
import kotlinx.android.synthetic.main.fragment_delivery_history.*
import kotlinx.android.synthetic.main.fragment_delivery_history.etFrom
import kotlinx.android.synthetic.main.fragment_delivery_history.etTo
import kotlinx.android.synthetic.main.fragment_delivery_history.tilFrom
import kotlinx.android.synthetic.main.fragment_delivery_history.tilTo
import kotlinx.android.synthetic.main.fragment_delivery_history.toolbar
import kotlinx.android.synthetic.main.fragment_delivery_history.tvToolBarTitle
import kotlinx.android.synthetic.main.toolbar_delivery_history.*
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DeliveryHistoryListFragment : Fragment()
{

    private val TAG = "DHL"

    private val B_DELVIVERY_HISTORY_LIST= "B_DELIVERY_HISTORY_LIST"

    private val component by lazy { DeliveryHistoryDH.deliveryHistoryComponent() }

    @Inject
    lateinit var viewModelFactory: DeliveryHistoryViewModelFactory

    @Inject
    lateinit var adapter:DeliveryHistoryListAdapter

    @Inject
    lateinit var contextTool: Context

    private val viewModel: DeliveryHistoryViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(DeliveryHistoryViewModel::class.java)
    }

    private var driverId = "-1"

    private var listShown=false;

    private lateinit var orderListGlobal:MutableList<DriverOrderDetails>

    private lateinit var selectedOrder:DriverDashboardData

    private var selectedPosition:Int=-1

    private var fromDate: Date=Date();

    private var toDate:Date= Date();

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_delivery_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        setProperties();
        if(listShown) showList()
        else
        {
            Log.d(TAG, "arguments ")
            initiateDeliveryHistoryListDataListener();
            arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let {
                driverId=it
                getDeliveryHistoryList()
            }
        }
    }

    private fun setProperties()
    {
        rvDeliveryHistory.layoutManager = LinearLayoutManager(context);
        rvDeliveryHistory.adapter = adapter
        etFrom.setText(getDisplayFormatDate(fromDate))
        etTo.setText(getDisplayFormatDate(toDate))
        etFrom.setOnClickListener{
            showDatePicker(true)
        }
        etTo.setOnClickListener{
            showDatePicker(false)
        }
        apply {
            tilFrom.typeface = context?.let { ResourcesCompat.getFont(it,R.font.comfortaa_regular) };
            tilTo.typeface = context?.let { ResourcesCompat.getFont(it,R.font.comfortaa_regular) };
            etFrom.typeface = context?.let { ResourcesCompat.getFont(it,R.font.comfortaa_regular) };
            etTo.typeface = context?.let { ResourcesCompat.getFont(it,R.font.comfortaa_regular) };
        }
        btnHistoryView.setOnClickListener{
            getDeliveryHistoryList()
        }
    }

    private fun showDatePicker(isFromDate: Boolean)
    {
        val builder = MaterialDatePicker.Builder.datePicker()
        val picker = builder.build()
        picker.addOnPositiveButtonClickListener {
            var calendar = Calendar.getInstance();
            calendar.timeInMillis=it
            if(isFromDate)
            {
                fromDate=calendar.time
                etFrom.setText(getDisplayFormatDate(fromDate))
            }
            else
            {
                toDate = calendar.time
                etTo.setText(getDisplayFormatDate(toDate))
            }
        }
        activity?.supportFragmentManager?.let { picker.show(it, picker.toString()) }
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("deliveryHistoryListFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_delivery_history)
        toolbar.title=""
    }

    private fun initiateDeliveryHistoryListDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.deliveryHistoryOutcome.observe(viewLifecycleOwner, Observer<Outcome<DeliveryHistoryResponse>> { outcome ->

           if(BuildConfig.DEBUG)
           {
               Log.d(TAG, "deliveryHistoryListListener: $outcome")
           }
            when (outcome)
            {
                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {
                        showProgress()
                    }
                }

                is Outcome.Success ->
                {
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG, "deliveryHistoryListListener: Successfully loaded data")
                    }

                    if(outcome.data.orderDetailsResult.errorDetails.errorCode==0)
                    {
                        setList(outcome.data.orderDetailsResult.driverOrderDetails);
                    }
                    else
                    {
                        //showErrorMessage(outcome.data.driverDashboardResult.errorDetails.errorMessage,false,1)
                        showInitialError(outcome.data.orderDetailsResult.errorDetails.errorMessage,false);
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {
                        //showErrorMessage(getString(R.string.msg_no_internet_connection),true,1)
                        showInitialError(getString(R.string.msg_no_internet_connection),true);
                    }

                    else
                    {
                        //showErrorMessage(getString(R.string.msg_unknown_error_try_again),true,1)
                        showInitialError(getString(R.string.msg_unknown_error_try_again),true);
                    }
                }
            }
        })
    }

    private fun  getDeliveryHistoryList()
    {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd");
        val strFromDate: String = simpleDateFormat.format(fromDate)
        val strToDate: String = simpleDateFormat.format(toDate)
        viewModel.deliveryHistory(driverId,strFromDate,strToDate)
    }

    private fun showProgress()
    {
        rvDeliveryHistory.visibility = View.GONE
        progressBarFragmentDeliveryHistoryList.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
    }

    private fun setList(deliverHistoryList:List<DriverOrderDetails>)
    {
        orderListGlobal = deliverHistoryList as MutableList<DriverOrderDetails>
        adapter.swapData(orderListGlobal)
        showList()
    }

    private fun showList()
    {
        listShown=true;
        rvDeliveryHistory.visibility=View.VISIBLE
        progressBarFragmentDeliveryHistoryList.visibility = View.GONE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialError(message:String,retry: Boolean)
    {
        progressBarFragmentDeliveryHistoryList.visibility = View.GONE
        errorContainer.visibility = View.VISIBLE
        rvDeliveryHistory.visibility=View.GONE
        tvError.text = message;
        if(retry) btnRetry.visibility = View.VISIBLE
        else btnRetry.visibility=View.GONE
    }

    private fun showErrorMessage(message:String,retry: Boolean,apiResponseFrom:Int)
    {
        val mySnackbar = Snackbar.make(clFragmentDeliveryHistoryList,
            message, Snackbar.LENGTH_INDEFINITE)
        mySnackbar.setBackgroundTint(ContextCompat.getColor(contextTool,R.color.colorBackground))
        if(retry) mySnackbar.setAction(R.string.lbl_retry, View.OnClickListener {v->
            when(apiResponseFrom)
            {
                1-> getDeliveryHistoryList()

            }
            mySnackbar.dismiss()
        })
        else mySnackbar.setAction(R.string.lbl_ok, View.OnClickListener {v->
           mySnackbar.dismiss()
        })
        mySnackbar.show()
        selectedOrder = DriverDashboardData()
        selectedOrder?.let {
            it.performAcceptRequest=false
            it.performDeclineRequest=false
        }
    }

}



