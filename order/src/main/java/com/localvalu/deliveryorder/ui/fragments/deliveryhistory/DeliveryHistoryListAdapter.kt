package com.localvalu.deliveryorder.ui.fragments.deliveryhistory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.deliveryorder.R
import com.localvalu.deliveryorder.data.model.response.deliveryhistory.DriverOrderDetails
import kotlinx.android.synthetic.main.view_item_history_list.view.*

/* 0 - Order initial stage
1 - Order Accepted by Driver
2 - Order Received from Merchant
3 - Order Delivered to Consumer
4 - Driver Accepted order not delivered yet */
class DeliveryHistoryListAdapter():ListAdapter<DriverOrderDetails,DeliveryHistoryListAdapter.HistoryViewHolder>(DriverOrderDetailsDC())
{
    private lateinit var context:Context;

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context=recyclerView.context;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder =
        HistoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_item_history_list,
            parent,false))

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) = holder.bind(getItem(position))


    inner class HistoryViewHolder( itemView: View)
        :RecyclerView.ViewHolder(itemView)
    {


        fun bind(driverOrderDetails: DriverOrderDetails) = with(itemView)
        {

            itemView?.tvOrderId.text = driverOrderDetails.orderId
            itemView?.tvDeliveredTime.text = driverOrderDetails.deliveredDate
            itemView?.tvMerchantName?.text = driverOrderDetails.merchantName
            itemView?.tvMileage?.text = String.format("%.2f",driverOrderDetails.mileage)
            itemView?.tvTransactionAmount?.text =String.format("%.2f",driverOrderDetails.transactionAmount)
            itemView?.tvPayMode?.text = driverOrderDetails.payMode
            itemView?.tvDeliveryAmount?.text = String.format("%.2f",driverOrderDetails.deliveryAmount)
            itemView?.tvDuration?.text = driverOrderDetails.duration
            itemView?.tvOrderStatus.text = driverOrderDetails.orderStatus

        }
    }

    fun swapData(data: List<DriverOrderDetails>) {
        submitList(data.toMutableList())
    }


    private class DriverOrderDetailsDC : DiffUtil.ItemCallback<DriverOrderDetails>()
    {
        override fun areItemsTheSame(oldItem: DriverOrderDetails, newItem: DriverOrderDetails) = oldItem.orderId == newItem.orderId

        override fun areContentsTheSame(oldItem: DriverOrderDetails, newItem: DriverOrderDetails) =  oldItem.orderId.equals(newItem.orderId)

    }

}