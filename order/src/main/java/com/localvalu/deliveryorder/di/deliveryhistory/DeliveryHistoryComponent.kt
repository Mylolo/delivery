package com.localvalu.deliveryorder.di.deliveryhistory

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.ui.fragments.deliveryhistory.DeliveryHistoryListFragment
import com.squareup.picasso.Picasso
import dagger.Component

@DeliveryHistoryScope
@Component(dependencies = [CoreComponent::class],modules = [DeliveryHistoryModule::class])
interface DeliveryHistoryComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun inject(deliveryHistoryListFragment: DeliveryHistoryListFragment)
}

