package com.localvalu.deliveryorder.di.fullorderdetails

import com.localvalu.deliveryaccount.data.*
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsDataContract
import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsRepository
import com.localvalu.deliveryorder.data.fullorderdetails.FullOrderDetailsService
import com.localvalu.deliveryorder.di.driverdashboard.DriverDashboardScope
import com.localvalu.deliveryorder.ui.fragments.fullorderdetails.FullOrderDetailsViewModelFactory
import com.localvalu.deliveryorder.ui.fragments.fullorderdetails.OrderedItemsAdapter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class FullOrderDetailsModule
{
    /*ViewModel*/
    @Provides
    @FullOrderDetailsScope
    fun fullOrderDetailsViewModelFactory(repository: FullOrderDetailsDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            FullOrderDetailsViewModelFactory =
        FullOrderDetailsViewModelFactory(
            repository,
            compositeDisposable
        )

    /*Repository*/
    @Provides
    @FullOrderDetailsScope
    fun fullOrderDetailsRepo(remote: FullOrderDetailsDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            FullOrderDetailsDataContract.Repository = FullOrderDetailsRepository( remote, scheduler, compositeDisposable)

    @Provides
    @FullOrderDetailsScope
    fun fullOrderDetailRemoteData(fullOrderDetailsService: FullOrderDetailsService):
            FullOrderDetailsDataContract.Remote = FullOrderDetailsRemoteData(fullOrderDetailsService)

    @Provides
    @FullOrderDetailsScope
    fun fullOrderDetailsService(@Named("PhpServer")retrofit: Retrofit): FullOrderDetailsService = retrofit.create(FullOrderDetailsService::class.java)

    /*Adapter*/
    @Provides
    @FullOrderDetailsScope
    fun orderedItemAdapter(): OrderedItemsAdapter = OrderedItemsAdapter()

    @Provides
    @FullOrderDetailsScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}