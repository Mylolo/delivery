package com.localvalu.deliveryorder.di.orderdecline

import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineDataContract
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineRemoteData
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineRepository
import com.localvalu.deliveryorder.data.orderdecline.OrderDeclineService
import com.localvalu.deliveryorder.ui.fragments.orderdecline.OrderDeclineViewModelFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class OrderDeclineModule
{
    /*ViewModel*/
    @Provides
    @OrderDeclineScope
    fun orderDeclineViewModelFactory(repository: OrderDeclineDataContract.Repository, compositeDisposable: CompositeDisposable):
            OrderDeclineViewModelFactory = OrderDeclineViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @OrderDeclineScope
    fun orderDeclineRepo(remote: OrderDeclineDataContract.Remote,scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            OrderDeclineDataContract.Repository = OrderDeclineRepository(remote, scheduler, compositeDisposable)

    @Provides
    @OrderDeclineScope
    fun orderDeclineRemoteData(orderDeclineService: OrderDeclineService):
            OrderDeclineDataContract.Remote = OrderDeclineRemoteData(orderDeclineService)

    @Provides
    @OrderDeclineScope
    fun orderDeclineService(@Named("PhpServer")retrofit: Retrofit): OrderDeclineService = retrofit.create(OrderDeclineService::class.java)

    @Provides
    @OrderDeclineScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}