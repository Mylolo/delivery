package com.localvalu.deliveryorder.di.acceptedorders

import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.acceptedorders.*
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryDataContract
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryRemoteData
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryRepository
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryService
import com.localvalu.deliveryorder.ui.fragments.acceptedorders.AcceptedOrderListAdapter
import com.localvalu.deliveryorder.ui.fragments.acceptedorders.AcceptedOrderListViewModelFactory
import com.localvalu.deliveryorder.ui.fragments.deliveryhistory.DeliveryHistoryListAdapter
import com.localvalu.deliveryorder.ui.fragments.deliveryhistory.DeliveryHistoryViewModelFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class AcceptedOrdersListModule
{
    /*ViewModel*/
    @Provides
    @AcceptedOrdersListScope
    fun acceptedOrderedListViewModelFactory(repository: AcceptedOrderListDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            AcceptedOrderListViewModelFactory = AcceptedOrderListViewModelFactory( repository, compositeDisposable)

    /*Repository*/
    @Provides
    @AcceptedOrdersListScope
    fun acceptedOrdersListRepo(remote: AcceptedOrderListDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            AcceptedOrderListDataContract.Repository = AcceptedOrderListRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AcceptedOrdersListScope
    fun acceptedOrdersListRemoteData(acceptedOrderListService: AcceptedOrderListService,
                                     orderStatusChangeService:OrderStatusChangeService,
                                     deliveryService: CompletedDeliveryService):
            AcceptedOrderListDataContract.Remote = AcceptedOrdersListRemoteData(acceptedOrderListService,orderStatusChangeService,deliveryService)

    @Provides
    @AcceptedOrdersListScope
    fun acceptedOrderListService(@Named("PhpServer")retrofit: Retrofit): AcceptedOrderListService  = retrofit.create(AcceptedOrderListService::class.java)

    @Provides
    @AcceptedOrdersListScope
    fun orderStatusChangeService(@Named("DotNetServer")retrofit: Retrofit): OrderStatusChangeService  = retrofit.create(OrderStatusChangeService::class.java)

    @Provides
    @AcceptedOrdersListScope
    fun completeDeliveryService(@Named("PhpServer")retrofit: Retrofit): CompletedDeliveryService  = retrofit.create(CompletedDeliveryService::class.java)

    /*Adapter*/
    @Provides
    @AcceptedOrdersListScope
    fun adapter(): AcceptedOrderListAdapter = AcceptedOrderListAdapter()

    @Provides
    @AcceptedOrdersListScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}