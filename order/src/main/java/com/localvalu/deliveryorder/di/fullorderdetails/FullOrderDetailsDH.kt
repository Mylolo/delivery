package com.localvalu.deliveryorder.di.fullorderdetails

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object FullOrderDetailsDH
{
    private var fullOrderDetailsComponent:FullOrderDetailsComponent?=null

    fun fullOrderDetailsComponent():FullOrderDetailsComponent
    {
        if(fullOrderDetailsComponent==null)
            fullOrderDetailsComponent = DaggerFullOrderDetailsComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return fullOrderDetailsComponent as FullOrderDetailsComponent

    }

    fun destroyFullOrderDetailsComponent() {
        fullOrderDetailsComponent = null
    }
}