package com.localvalu.deliveryorder.di.fullorderdetails

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.ui.fragments.driverdashboard.OrderListFragment
import com.localvalu.deliveryorder.ui.fragments.fullorderdetails.FullOrderDetailsFragment
import com.squareup.picasso.Picasso
import dagger.Component

@FullOrderDetailsScope
@Component(dependencies = [CoreComponent::class],modules = [FullOrderDetailsModule::class])
interface FullOrderDetailsComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun inject(fullOrderDetailsFragment: FullOrderDetailsFragment)
}

