package com.localvalu.deliveryorder.di.orderdecline

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class OrderDeclineScope