package com.localvalu.deliveryorder.di.driverdashboard

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.ui.fragments.orderdecline.OrderDeclineFragment
import com.localvalu.deliveryorder.ui.fragments.driverdashboard.OrderListFragment
import com.squareup.picasso.Picasso
import dagger.Component

@DriverDashboardScope
@Component(dependencies = [CoreComponent::class],modules = [DriverDashboardModule::class])
interface DriverDashboardComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun inject(orderListFragment: OrderListFragment)
}

