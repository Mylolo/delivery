package com.localvalu.deliveryorder.di.acceptedorders

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object AcceptedOrdersListDH
{
    private var acceptedOrdersListComponent:AcceptedOrdersListComponent? = null

    fun acceptedOrdersListComponent(): AcceptedOrdersListComponent
    {
        if (acceptedOrdersListComponent == null)
            acceptedOrdersListComponent =
                DaggerAcceptedOrdersListComponent.builder().coreComponent(CoreApp.coreComponent)
                    .build()
        return acceptedOrdersListComponent as AcceptedOrdersListComponent
    }

    fun destroyAcceptedOrdersListComponent()
    {
        acceptedOrdersListComponent = null
    }
}