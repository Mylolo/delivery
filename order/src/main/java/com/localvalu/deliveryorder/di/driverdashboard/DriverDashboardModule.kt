package com.localvalu.deliveryorder.di.driverdashboard

import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.driverdashboard.*
import com.localvalu.deliveryorder.ui.fragments.driverdashboard.DriverDashboardViewModelFactory
import com.localvalu.deliveryorder.ui.fragments.driverdashboard.OrderListAdapter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class DriverDashboardModule
{
    /*ViewModel*/
    @Provides
    @DriverDashboardScope
    fun driverDashboardViewModelFactory(repository: DriverDashboardDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            DriverDashboardViewModelFactory =
        DriverDashboardViewModelFactory(
            repository,
            compositeDisposable
        )

    /*Repository*/
    @Provides
    @DriverDashboardScope
    fun driverDashboardRepo(remote: DriverDashboardDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            DriverDashboardDataContract.Repository = DriverDashboardRepository( remote, scheduler, compositeDisposable)

    @Provides
    @DriverDashboardScope
    fun driverDashboardRemoteData(driverDashboardService: DriverDashboardService,
                                  orderAcceptServiceOne: OrderAcceptServiceOne,orderAcceptServiceTwo: OrderAcceptServiceTwo):
            DriverDashboardDataContract.Remote = DriverDashboardRemoteData(driverDashboardService,orderAcceptServiceOne,orderAcceptServiceTwo)

    @Provides
    @DriverDashboardScope
    fun driverDashboardService(@Named("PhpServer")retrofit: Retrofit): DriverDashboardService = retrofit.create(DriverDashboardService::class.java)

    @Provides
    @DriverDashboardScope
    fun orderAcceptServiceOne(@Named("PhpServer")retrofit: Retrofit): OrderAcceptServiceOne = retrofit.create(OrderAcceptServiceOne::class.java)

    @Provides
    @DriverDashboardScope
    fun orderAcceptServiceTwo(@Named("DotNetServer")retrofit: Retrofit): OrderAcceptServiceTwo = retrofit.create(OrderAcceptServiceTwo::class.java)


    /*Adapter*/
    @Provides
    @DriverDashboardScope
    fun adapter(): OrderListAdapter = OrderListAdapter()

    @Provides
    @DriverDashboardScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}