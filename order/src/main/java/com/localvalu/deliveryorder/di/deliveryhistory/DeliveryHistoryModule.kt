package com.localvalu.deliveryorder.di.deliveryhistory

import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryDataContract
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryRemoteData
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryRepository
import com.localvalu.deliveryorder.data.deliveryhistory.DeliveryHistoryService
import com.localvalu.deliveryorder.ui.fragments.deliveryhistory.DeliveryHistoryListAdapter
import com.localvalu.deliveryorder.ui.fragments.deliveryhistory.DeliveryHistoryViewModelFactory
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class DeliveryHistoryModule
{
    /*ViewModel*/
    @Provides
    @DeliveryHistoryScope
    fun deliveryHistoryViewModelFactory(repository: DeliveryHistoryDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            DeliveryHistoryViewModelFactory =
        DeliveryHistoryViewModelFactory(
            repository,
            compositeDisposable
        )

    /*Repository*/
    @Provides
    @DeliveryHistoryScope
    fun deliveryHistoryRepo(remote: DeliveryHistoryDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            DeliveryHistoryDataContract.Repository = DeliveryHistoryRepository( remote, scheduler, compositeDisposable)

    @Provides
    @DeliveryHistoryScope
    fun deliveryHistoryRemoteData(deliveryHistoryService: DeliveryHistoryService):
            DeliveryHistoryDataContract.Remote = DeliveryHistoryRemoteData(deliveryHistoryService)

    @Provides
    @DeliveryHistoryScope
    fun deliveryHistoryService(@Named("DotNetServer")retrofit: Retrofit): DeliveryHistoryService = retrofit.create(DeliveryHistoryService::class.java)

    /*Adapter*/
    @Provides
    @DeliveryHistoryScope
    fun adapter(): DeliveryHistoryListAdapter = DeliveryHistoryListAdapter()

    @Provides
    @DeliveryHistoryScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

}