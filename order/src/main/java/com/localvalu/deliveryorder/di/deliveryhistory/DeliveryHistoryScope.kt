package com.localvalu.deliveryorder.di.deliveryhistory

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DeliveryHistoryScope