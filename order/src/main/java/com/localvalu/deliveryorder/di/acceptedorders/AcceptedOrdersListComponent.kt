package com.localvalu.deliveryorder.di.acceptedorders

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliveryorder.ui.fragments.acceptedorders.AcceptedOrderListFragment
import com.localvalu.deliveryorder.ui.fragments.orderdecline.OrderDeclineFragment
import com.localvalu.deliveryorder.ui.fragments.driverdashboard.OrderListFragment
import com.squareup.picasso.Picasso
import dagger.Component

@AcceptedOrdersListScope
@Component(dependencies = [CoreComponent::class],modules = [AcceptedOrdersListModule::class])
interface AcceptedOrdersListComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun inject(acceptedOrdersListFragment: AcceptedOrderListFragment)

}

