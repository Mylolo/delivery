package com.localvalu.deliveryorder.di.orderdecline

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object OrderDeclineDH
{
    private var orderDeclineComponent: OrderDeclineComponent? = null

    fun orderDeclineComponent(): OrderDeclineComponent {
        if (orderDeclineComponent == null)
            orderDeclineComponent = DaggerOrderDeclineComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return orderDeclineComponent as OrderDeclineComponent
    }

    fun destroyOrderDeclineComponent() {
        orderDeclineComponent = null
    }
}