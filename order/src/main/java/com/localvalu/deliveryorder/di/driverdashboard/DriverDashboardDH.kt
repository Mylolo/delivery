package com.localvalu.deliveryorder.di.driverdashboard

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object DriverDashboardDH
{
    private var driverDashboardComponent: DriverDashboardComponent? = null

    fun driverDashboardComponent(): DriverDashboardComponent {
        if (driverDashboardComponent == null)
            driverDashboardComponent = DaggerDriverDashboardComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return driverDashboardComponent as DriverDashboardComponent
    }

    fun destroyDriverDashboardComponent() {
        driverDashboardComponent = null
    }
}