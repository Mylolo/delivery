package com.localvalu.deliveryorder.di.deliveryhistory

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton

@Singleton
object DeliveryHistoryDH
{
    private var deliveryHistoryComponent: DeliveryHistoryComponent? = null

    fun deliveryHistoryComponent(): DeliveryHistoryComponent
    {
        if (deliveryHistoryComponent == null)
            deliveryHistoryComponent =
                DaggerDeliveryHistoryComponent.builder().coreComponent(CoreApp.coreComponent)
                    .build()
        return deliveryHistoryComponent as DeliveryHistoryComponent
    }

    fun destroyDeliveryHistoryComponent()
    {
        deliveryHistoryComponent = null
    }
}