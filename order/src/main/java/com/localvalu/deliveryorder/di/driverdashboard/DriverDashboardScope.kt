package com.localvalu.deliveryorder.di.driverdashboard

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DriverDashboardScope