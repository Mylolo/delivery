package com.localvalu.deliveryorder.di.fullorderdetails

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FullOrderDetailsScope