package com.localvalu.deliveryorder.di.acceptedorders

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AcceptedOrdersListScope