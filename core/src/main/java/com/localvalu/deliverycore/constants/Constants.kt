package com.localvalu.deliverycore.constants

import com.localvalu.deliverycore.BuildConfig

/* 0 - Order initial stage
1 - Order Accepted by Driver
2 - Order Received from Merchant
3 - Order Delivered to Consumer
4 - Driver Accepted order not delivered yet */
object Constants
{
    val API_URL_DOTNET = BuildConfig.BASE_URL_DOTNET

    val API_URL_PHP = BuildConfig.BASE_URL_PHP

    val SHARED_PREFERENCE_NAME="localvaluPreference"

    val B_LOCAL_USER = "B_Local_User"

    val B_LOCAL_USER_TYPE = "B_Local_User_Type"

    val B_FCM_ID = "B_FCMID"

    val LOGOUT =" BuildConfig.BASE_URL_PHP"

    //Services
    private const val TAG = "FOLocationService"

    private const val PACKAGE_NAME = "com.localvalu.deliveryapp"
    internal const val ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST =
        "$PACKAGE_NAME.action.FOREGROUND_ONLY_LOCATION_BROADCAST"

    internal const val EXTRA_LOCATION = "$PACKAGE_NAME.extra.LOCATION"

    private const val EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION =
        "$PACKAGE_NAME.extra.CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION"

    private const val NOTIFICATION_ID = 12345678

    private const val NOTIFICATION_CHANNEL_ID = "localvalu_while_in_use_channel_01"

    //Order Related
    val ORDER_PLACED="0"
    val ORDER_PENDING = "2"
    val ORDER_ACCEPTED = "1"
    val ORDER_DELIVERED = "3"

    val STATUS_ORDER_PLACED=0
    val STATUS_ORDER_ACCEPTED_BY_DRIVER = 1
    val STATUS_ORDER_RECEIVED_FROM_MERCHANT = 2
    val STATUS_ORDER_DELIVERED_TO_CONSUMER = 3
    val STATUS_ORDER_DRIVER_ACCEPTED_NOT_DELIVERED = 4

    val DRIVER_ORDER_ACCEPT = 1
    val DRIVER_ORDER_DECLINE = 5
    val DRIVER_ORDER_COMPLETE_DELIVER = 3

}