package com.localvalu.deliverycore.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

class ValidationUtils
{
    fun isValidMobileNo(mobileNumber: String?): Boolean
    {
        val regexMobileNo =
            "^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$"
        val p: Pattern = Pattern.compile(regexMobileNo)

        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        val m: Matcher = p.matcher(mobileNumber)
        return m.find() && m.group().equals(mobileNumber)
    }

    fun checkPasswordRegex(password: String): Boolean
    {
        // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        val PASSWORD_PATTERN =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[~`#@$!%*?^&])[A-Za-z\\d~`#@$!%*?&]{8,}$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        val matcher = pattern.matcher(password.trim { it <= ' ' })
        return matcher.matches()
    }

    fun isEmptyPassword(password: String): Boolean
    {
        return password.isEmpty()
    }

    fun isMatchingPassword(password: String,confirmPassword: String): Boolean
    {
        return password.equals(confirmPassword)
    }

}