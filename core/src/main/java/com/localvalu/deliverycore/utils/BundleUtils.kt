package com.localvalu.deliverycore.utils

import android.os.Bundle

class BundleUtils
{
    companion object
    {
        val BUNDLE_DRIVER_ID="Bundle_Driver_Id"
        val BUNDLE_POOL_NAME = "Bundle_Pool_Name"
        val BUNDLE_MOBILE_NUMBER="Bundle_Mobile_Number"
        val BUNDLE_OTP="Bundle_OTP"
        val BUNDLE_ORDER_ID="Bundle_Order_Id"
        val BUNDLE_LOGOUT="Bundle_LOGOUT"
        val BUNDLE_DRIVER_DASHBOARD_DATA="Bundle_Driver_Dashboard_Data"
        val BUNDLE_ORDER_DECLINED="Bundle_Order_Declined"

        val BUNDLE_NAVIGATE_ACTION_ID="Bundle_Navigation_Action_Id"

        val KEY_ORDER_DECLINED_REQUEST="Key_Order_Declined_Request"


    }
}