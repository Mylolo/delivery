package com.localvalu.deliverycore.extensions

import com.localvalu.deliverycore.BuildConfig
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

fun getServerFormatDate(date: Date):String
{
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd");
    return simpleDateFormat.format(date)
}

fun getDisplayFormatDate(date:Date): String
{
    val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy");
    return simpleDateFormat.format(date)
}

fun getDisplayFormatDateFromString(strDate:String): String
{
    //2021-05-24 18:07:09
    val simpleDateFormatInput = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val simpleDateFormatOutput = SimpleDateFormat("dd/MM/yyyy hh:mm:ss a")
    try
    {
        val date:Date = simpleDateFormatInput.parse(strDate)
        return simpleDateFormatOutput.format(date)
    }
    catch (ex:Exception)
    {
        if(BuildConfig.DEBUG)
        {
            ex.printStackTrace()
        }
    }
    return ""
}
