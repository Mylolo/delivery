package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.DriverLoginResult
import java.io.Serializable

/**
 * Data class that captures user information for logged in pool user retrieved from LoginRepository
 */
data class PoolLogInResponse(@SerializedName("PoolNameResult") val poolLoginResult:PoolLoginResult):Serializable
