package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class AppLoggedInUser(val userLoggedIn:Boolean,
                           val userType:Int,
                           val driverUser: DriverUser,
                           val poolUser: PoolUser,
                           val fcmId:String):Serializable
