package com.localvalu.deliverycore.base

import android.view.MenuItem

interface MainHandler
{
    fun logOut()
    fun navigationItemSelected(item:MenuItem)
}