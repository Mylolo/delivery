package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.DriverLoginResult
import java.io.Serializable

/**
 * Data class that captures user information for logged in driver user retrieved from LoginRepository
 */
data class DriverLogInResponse(@SerializedName("DriverAppResult") val driverLoginResult:DriverLoginResult):Serializable
