package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.BaseResponse
import com.localvalu.deliverycore.base.model.ErrorDetails
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class DriverUser(@SerializedName("Name") val name: String,
                      @SerializedName("MobileNo") val mobileNo: String,
                      @SerializedName("DriverArea") val driverArea: String,
                      @SerializedName("HomePostcode") val homePostCode: String,
                      @SerializedName("Gender") val gender: String,
                      @SerializedName("DriverStatus") val driverStatus: String,
                      @SerializedName("DriverId") val driverId: String,
                      @SerializedName("ErrorDetails") val errorDetails: ErrorDetails
): Serializable
