package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data class that captures user information for logged in driver user retrieved from LoginRepository
 */
class DriverLoginResult(@SerializedName("DriverLoginResult") val user: DriverUser):Serializable
