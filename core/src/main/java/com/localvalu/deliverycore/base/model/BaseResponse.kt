package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseResponse(@SerializedName("status") val status:String,
                        @SerializedName("msg") val message: String,
                        @SerializedName("ErrorDetails") val errorDetails: ErrorDetails? = null)
{

}


