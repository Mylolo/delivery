package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.BaseResponse
import com.localvalu.deliverycore.base.model.ErrorDetails
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class PoolUser(@SerializedName("PoolName") val poolName: String,
                    @SerializedName("PoolNameId") val poolNameId: Int,
                    @SerializedName("ErrorDetails") val errorDetails: ErrorDetails
): Serializable
