package com.localvalu.deliverycore.base.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

open class ErrorDetails(@SerializedName("ErrorCode") var errorCode:Int =-1,
                         @SerializedName("ErrorMessage")  var errorMessage:String="")


