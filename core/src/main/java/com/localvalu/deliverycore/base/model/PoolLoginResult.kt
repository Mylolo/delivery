package com.localvalu.deliverycore.base.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data class that captures user information for logged in pool user retrieved from LoginRepository
 */
class PoolLoginResult(@SerializedName("PoolNameLoginResult") val user: PoolUser):Serializable
