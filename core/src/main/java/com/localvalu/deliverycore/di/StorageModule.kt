package com.localvalu.deliverycore.di

import android.content.Context
import android.content.SharedPreferences
import com.localvalu.deliverycore.constants.Constants.SHARED_PREFERENCE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule
{
    @Provides
    @Singleton
    fun providesSharedPreferences(context: Context):SharedPreferences
    {
        return context.getSharedPreferences(SHARED_PREFERENCE_NAME,Context.MODE_PRIVATE);
    }
}