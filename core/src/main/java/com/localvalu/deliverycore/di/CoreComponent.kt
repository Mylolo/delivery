package com.localvalu.deliverycore.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.AppModule
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.squareup.picasso.Picasso
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,NetworkModule::class,StorageModule::class,ImageModule::class])
interface CoreComponent
{
    fun context() : Context

    @Named("DotNetServer")
    fun retrofitDotNet() : Retrofit

    @Named("PhpServer")
    fun retrofitPhp() : Retrofit

    fun picasso() : Picasso

    fun sharedPreferences() : SharedPreferences

    fun scheduler() : Scheduler

    fun gson() : Gson

}