package com.localvalu.deliverycore.di

import android.content.Context
import com.localvalu.deliverycore.networkhandler.AppScheduler
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule (val context: Context)
{
    @Provides
    @Singleton
    fun providesContext():Context
    {
        return context
    }

    @Provides
    @Singleton
    fun providesScheduler():Scheduler
    {
        return AppScheduler();
    }

}