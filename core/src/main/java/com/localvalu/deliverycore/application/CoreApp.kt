package com.localvalu.deliverycore.application

import android.app.Application
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.FirebaseApp
import com.localvalu.deliverycore.di.AppModule
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.di.DaggerCoreComponent
import java.security.NoSuchAlgorithmException
import javax.net.ssl.SSLContext

open class CoreApp : Application()
{
    companion object
    {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate()
    {
        super.onCreate()
        initDI()
    }

    fun initDI()
    {
        coreComponent = DaggerCoreComponent.builder().appModule(AppModule(this)).build()
    }

}