package com.localvalu.deliveryapp.data.model.response

import com.google.gson.annotations.SerializedName

data class GetDriverLocationResponse(@SerializedName("GetDriverLocationResult") val getDriverLocationResult: GetDriverLocationResult)
{

}