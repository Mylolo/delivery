package com.localvalu.deliveryapp.data
import com.localvalu.deliveryapp.data.model.request.LocationTrackRequest
import com.localvalu.deliveryapp.data.model.response.GetDriverLocationResponse
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

open class MainRepository(
    private val local: MainDataContract.Local,
    private val remote: MainDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : MainDataContract.Repository
{
    override val loginFetchOutcome: PublishSubject<Outcome<AppLoggedInUser>> =
        PublishSubject.create<Outcome<AppLoggedInUser>>()

    override val fcmFetchOutcome: PublishSubject<Outcome<String>> =
        PublishSubject.create<Outcome<String>>()

    override val logoutFetchOutcome:PublishSubject<Outcome<Boolean>> =
        PublishSubject.create<Outcome<Boolean>>()

    override val sendLocationFetchOutcome: PublishSubject<Outcome<GetDriverLocationResponse>> =
        PublishSubject.create<Outcome<GetDriverLocationResponse>>()


    override fun getLocalUser()
    {
        loginFetchOutcome.loading(true)
        //Observe changes to the local
        local.getLocalUser()
            .performOnBackOutOnMain(scheduler)
            .subscribe({appLoggedInUser -> loginFetchOutcome.success(appLoggedInUser)}
                , { error-> handleLoginError(error)})
            .addTo(compositeDisposable)
    }

    override fun getFCMID()
    {
        fcmFetchOutcome.loading(true)
        //Observe changes to the local
        local.getFCMID()
            .performOnBackOutOnMain(scheduler)
            .subscribe({fcmID -> fcmFetchOutcome.success(fcmID)}
                , { error-> handleFCMError(error)})
            .addTo(compositeDisposable)
    }

    override fun saveFCMID(fcmID: String)
    {
        local.saveFCMID(fcmID)
    }

    override fun sendLocation(locationTrackRequest: LocationTrackRequest)
    {
        sendLocationFetchOutcome.loading(true)
        Single.wrap(remote.sendLocation(locationTrackRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({ driverLocationResponse ->
                sendLocationFetchOutcome.success(
                    driverLocationResponse
                )
            },
                { error -> handleSendLocationError(error) })
            .addTo(compositeDisposable)
    }

    override fun logout()
    {
        logoutFetchOutcome.loading(true)
        local.logout().performOnBackOutOnMain(scheduler).
        subscribe({logoutResult ->logoutFetchOutcome.success(logoutResult) },{
                error->handleLogoutError(error)
        })
            .addTo(compositeDisposable);
    }

    override fun handleLoginError(error: Throwable)
    {
        loginFetchOutcome.failed(error)
    }

    override fun handleLogoutError(error: Throwable)
    {
        logoutFetchOutcome.failed(error)
    }

    override fun handleFCMError(error: Throwable)
    {
        fcmFetchOutcome.failed(error)
    }

    override fun handleSendLocationError(error: Throwable)
    {
        sendLocationFetchOutcome.failed(error)
    }

}

