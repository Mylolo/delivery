package com.localvalu.deliveryapp.data.model.response

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class GetDriverLocationResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                   @SerializedName("status")  val status:String,
                                   @SerializedName("message")  val message:String)

{

}