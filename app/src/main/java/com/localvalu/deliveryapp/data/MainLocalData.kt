package com.localvalu.deliveryapp.data

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.base.model.*
import com.localvalu.deliverycore.extensions.performOnBack
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverycore.constants.Constants
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class MainLocalData(
    private val sharedPreferences: SharedPreferences,
    private val scheduler: Scheduler,
    private val gson:Gson
) : MainDataContract.Local
{

    override fun getLocalUser(): Flowable<AppLoggedInUser>
    {
        val localUser:String ? = sharedPreferences.getString(Constants.B_LOCAL_USER,"")
        var appLoggedInUser:AppLoggedInUser? = gson.fromJson(localUser, AppLoggedInUser::class.java)
        if(appLoggedInUser==null) appLoggedInUser=handleAppUserLoggedInError()
        return Flowable.just(appLoggedInUser)
    }

    override fun getFCMID(): Single<String>
    {
        var fcmID: String? = sharedPreferences.getString(Constants.B_FCM_ID, "")
        if (fcmID == null) fcmID = "NFL";
        else if(fcmID.equals("NF")) fcmID = "NFL";
        return Single.just(fcmID)

    }

    override fun logout(): Single<Boolean>
    {
        return  Single.just(sharedPreferences.edit().putString(Constants.B_LOCAL_USER,"").commit());
    }

    private fun handleAppUserLoggedInError():AppLoggedInUser
    {
        return AppLoggedInUser(false,-1,emptyDriverUser(),emptyPoolUser(),"")
    }

    private fun emptyDriverUser():DriverUser
    {
        return DriverUser(
                "",
                "",
                "",
                "", "","","", errorDetails()
            )

    }

    private fun emptyPoolUser(): PoolUser
    {
        return PoolUser("",-1,errorDetails())
    }

    private fun errorDetails():ErrorDetails
    {
        return ErrorDetails(
            -1,
            "Not LoggedIn"
        )
    }

    override fun saveFCMID(fcmID: String)
    {
        Completable.fromAction {
            val editor = sharedPreferences.edit()
            editor.putString(Constants.B_FCM_ID, fcmID).commit()
        }
            .performOnBack(scheduler)
            .subscribe()
    }
}