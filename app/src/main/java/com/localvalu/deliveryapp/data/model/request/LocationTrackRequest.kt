package com.localvalu.deliveryapp.data.model.request

import com.google.gson.annotations.SerializedName

data class LocationTrackRequest(@SerializedName("Token") var token: String,
                                @SerializedName("driverId") val driverId: String,
                                @SerializedName("Latitude") val latitude: String,
                                @SerializedName("Longitude") val longitude: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}