package com.localvalu.deliveryapp.data

import com.localvalu.deliveryapp.data.model.request.LocationTrackRequest

class MainRemoteData(private val locationTrackService:LocationTrackService) : MainDataContract.Remote
{
    override fun sendLocation(locationTrackRequest: LocationTrackRequest) = locationTrackService.sendLocationDetails(locationTrackRequest)
}