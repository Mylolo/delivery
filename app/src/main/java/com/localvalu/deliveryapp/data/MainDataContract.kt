package com.localvalu.deliveryapp.data

import com.localvalu.deliveryapp.data.model.request.LocationTrackRequest
import com.localvalu.deliveryapp.data.model.response.GetDriverLocationResponse
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.networkhandler.Outcome
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface MainDataContract
{
    interface Repository
    {
        val loginFetchOutcome: PublishSubject<Outcome<AppLoggedInUser>>
        val fcmFetchOutcome: PublishSubject<Outcome<String>>
        val logoutFetchOutcome: PublishSubject<Outcome<Boolean>>
        val sendLocationFetchOutcome: PublishSubject<Outcome<GetDriverLocationResponse>>

        fun sendLocation(locationTrackRequest: LocationTrackRequest)
        fun handleSendLocationError(error: Throwable)
        fun getLocalUser()
        fun getFCMID()
        fun saveFCMID(fcmID:String)
        fun logout()
        fun handleLoginError(error: Throwable)
        fun handleLogoutError(error: Throwable)
        fun handleFCMError(error: Throwable)
    }

    interface Local
    {
        fun getLocalUser(): Flowable<AppLoggedInUser>
        fun getFCMID():Single<String>
        fun saveFCMID(fcmID:String)
        fun logout():Single<Boolean>
    }

    interface Remote
    {
        fun sendLocation(locationTrackRequest: LocationTrackRequest) : Single<GetDriverLocationResponse>
    }

}