package com.localvalu.deliveryapp.data

import com.localvalu.deliveryapp.data.model.request.LocationTrackRequest
import com.localvalu.deliveryapp.data.model.response.GetDriverLocationResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LocationTrackService
{
    //@GET("users/{user}")
    //fun getUserInfo(@Path("user") userId:Int): Single<LoggedInUser>

    @POST("GetDriverLocation")
    fun sendLocationDetails(@Body locationTrackRequest: LocationTrackRequest): Single<GetDriverLocationResponse>
}