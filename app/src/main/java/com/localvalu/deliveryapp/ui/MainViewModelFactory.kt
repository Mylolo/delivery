package com.localvalu.deliveryapp.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryapp.data.MainDataContract


import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class MainViewModelFactory(
    private val repository: MainDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
            return MainViewModel(
                repository,
                compositeDisposable
            ) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
