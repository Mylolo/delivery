package com.localvalu.deliveryapp.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryapp.data.MainDataContract
import com.localvalu.deliveryapp.data.model.request.LocationTrackRequest
import com.localvalu.deliveryapp.di.MainDH
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import io.reactivex.disposables.CompositeDisposable

class MainViewModel(
    private val loginRepository: MainDataContract.Repository,
    private val compositeDisposable: CompositeDisposable) : ViewModel()
{

    val loginOutCome: LiveData<Outcome<AppLoggedInUser>> by lazy {
        //Convert publish subject to livedata
        loginRepository.loginFetchOutcome.toLiveData(compositeDisposable)
    }

    val fcmOutCome: LiveData<Outcome<String>> by lazy {
        //Convert publish subject to livedata
        loginRepository.fcmFetchOutcome.toLiveData(compositeDisposable)
    }

    val logoutOutCome: LiveData<Outcome<Boolean>> by lazy {
        //Convert publish subject to livedata
        loginRepository.logoutFetchOutcome.toLiveData(compositeDisposable)
    }

    fun checkLogin()
    {
        if (loginOutCome.value == null)
            loginRepository.getLocalUser()
    }

    fun getFCMID()
    {
        if (fcmOutCome.value == null)
            loginRepository.getFCMID()
    }

    fun saveFCMID(fcmID:String)
    {
        loginRepository.saveFCMID(fcmID)
    }

    fun logout()
    {
        if (logoutOutCome.value == null)
            loginRepository.logout()
    }

    fun sendLocationToServer(driverId:String,latitude:String,longitude:String)
    {
        loginRepository.sendLocation(LocationTrackRequest("",driverId,latitude,longitude))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        MainDH.destroyMainComponent()
    }

}
