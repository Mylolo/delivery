package com.localvalu.deliveryapp.ui

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.StateListDrawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.localvalu.deliveryapp.BuildConfig
import com.localvalu.deliveryapp.R
import com.localvalu.deliveryapp.components.FOLocationService
import com.localvalu.deliveryapp.di.MainDH
import com.localvalu.deliverycore.extensions.toText
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener
{

    private val TAG = "MainActivity"

    private lateinit var appBarConfiguration: AppBarConfiguration

    var textColorStates = arrayOf(
        intArrayOf(-android.R.attr.state_checked),
        intArrayOf(android.R.attr.state_checked)
    )

    private var driverId:String = "-1"

    private var colorBottomMenuNormal = 0
    private var colorLeftNavViewBackgroundNormal = 0
    private var colorLeftNavViewBackgroundSelected = 0

    //Service
    private var foregroundOnlyLocationServiceBound = false

    // Provides location updates for while-in-use feature.
    private var foregroundOnlyLocationService: FOLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    private lateinit var foregroundOnlyBroadcastReceiver: ForegroundOnlyBroadcastReceiver

    private var locationUpdate = false;

    // Monitors connection to the while-in-use service.
    private val foregroundOnlyServiceConnection = object : ServiceConnection
    {

        override fun onServiceConnected(name: ComponentName, service: IBinder)
        {
            val binder = service as FOLocationService.LocalBinder
            foregroundOnlyLocationService = binder.service
            foregroundOnlyLocationServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName)
        {
            foregroundOnlyLocationService = null
            foregroundOnlyLocationServiceBound = false
        }
    }

    private val component by lazy { MainDH.mainComponent() }

    @Inject
    lateinit var viewModelFactory: MainViewModelFactory

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    private var navigateId:Int = -1

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        navigateId = R.id.action_mainFragment_to_actionLoginFragment
        colorBottomMenuNormal = ContextCompat.getColor(this, R.color.colorBlack)
        colorLeftNavViewBackgroundNormal = ContextCompat.getColor(this, R.color.colorTransparent)
        colorLeftNavViewBackgroundSelected = ContextCompat.getColor(this, R.color.colorLightGray)
        nav_view.setNavigationItemSelectedListener(this)
        //Firebase
        FirebaseApp.initializeApp(this);
        //service
        foregroundOnlyBroadcastReceiver = ForegroundOnlyBroadcastReceiver()
        if (foregroundPermissionApproved())
        {
            foregroundOnlyLocationService?.subscribeToLocationUpdates()
                ?: Log.d(TAG, "Service Not Bound")
        }
        else
        {
            requestForegroundPermissions()
        }
        registerMyReceiver()
        initiateFCMDataListener()
        viewModel.getFCMID()
    }

    private fun initiateFCMDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.fcmOutCome.observe(this, Observer<Outcome<String>> { outcome ->
            Log.d(TAG, "initiateFCMDataListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {

                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "initiateDataListener: Successfully loaded data")

                    if (outcome.data.equals("NFL") || outcome.data.equals(""))
                    {
                        Log.d(TAG, "FCMID-NotFoundInLocal")
                        getFCMIDFromApi();
                    }
                    else if (outcome.data.equals("NF"))
                    {
                        Log.d(TAG, "FCMID-NotFound")
                        getFCMIDFromApi();
                    }
                    else
                    {
                        //Toast.makeText(context,"FCMID"+outcome.data,Toast.LENGTH_LONG).show()
                        viewModel.saveFCMID(outcome.data)
                    }
                }

                is Outcome.Failure ->
                {

                    Toast.makeText(
                        this, getString(com.localvalu.deliverylogin.R.string.msg_unknown_error_try_again).toString(),
                        Toast.LENGTH_LONG
                    ).show()
                }

            }
        })
    }

    private fun setLeftNavigationIconColor(color: Int)
    {
        val iconColors = intArrayOf(color, colorBottomMenuNormal)
        val colorStateListIcon = ColorStateList(textColorStates, iconColors)
        nav_view.itemIconTintList = (colorStateListIcon)
        val textColors = intArrayOf(colorBottomMenuNormal, color)
        val colorStateListText = ColorStateList(textColorStates, textColors)
        nav_view.itemTextColor = (colorStateListText)
        nav_view.itemBackground = (makeSelector(colorLeftNavViewBackgroundSelected))
    }

    private fun makeSelector(color: Int): StateListDrawable?
    {
        val res =
            StateListDrawable()
        res.setExitFadeDuration(400)
        res.alpha = 45
        res.addState(intArrayOf(android.R.attr.state_pressed), ColorDrawable(color))
        res.addState(intArrayOf(android.R.attr.state_checked), ColorDrawable(color))
        res.addState(intArrayOf(), ColorDrawable(Color.TRANSPARENT))
        return res
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean
    {

        drawer_layout.closeDrawers()
        val navController: NavController =
            Navigation.findNavController(this, R.id.nav_host_fragment);
        when (item.itemId)
        {
            R.id.logOut ->
            {
                navController.popBackStack()
                navigateId = R.id.action_mainFragment_to_actionLoginFragment;
                gotoDestination(true)
            }
            R.id.profile ->
            {
                navigateId = R.id.action_mainFragment_to_actionProfileFragment
                gotoDestination(false)
            }
            R.id.history ->
            {
                navigateId = R.id.action_mainFragment_to_actionDeliveryHistoryFragment
                gotoDestination(false)
            }
            R.id.acceptedOrdersList ->
            {
                navigateId = R.id.action_mainFragment_to_actionAcceptOrdersListFragment
                gotoDestination(false)
            }
        }
        return true;
    }

    private fun getFCMIDFromApi()
    {
        //Observe changes to the local
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful)
                {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                var token = task.result?.token
                if (token == null) token = "NF"
                //Log.d(TAG, "getFCMIDFromApi-FCMID-"+ token)
                viewModel.saveFCMID(token);
            })
    }

    private fun gotoDestination(logout:Boolean)
    {
        var bundle = bundleOf(BundleUtils.BUNDLE_LOGOUT to logout,
                                     BundleUtils.BUNDLE_NAVIGATE_ACTION_ID to navigateId,
                                     BundleUtils.BUNDLE_DRIVER_ID to driverId)
        val navOptions: NavOptions = NavOptions.Builder()
            .setPopUpTo(R.id.mainFragment, true)
            .build();
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        navController.navigate(R.id.mainFragment,bundle,navOptions)
    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)
    }

    /**
     * This method is responsible to register an action to BroadCastReceiver
     */
    private fun registerMyReceiver()
    {
        try
        {
            val intentFilter = IntentFilter()
            intentFilter.addAction(FOLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST)
            registerReceiver(foregroundOnlyBroadcastReceiver, intentFilter)
        }
        catch (ex: Exception)
        {
            ex.printStackTrace()
        }
    }

    /**
     * Receiver for location broadcasts from [ForegroundOnlyLocationService].
     */
    private inner class ForegroundOnlyBroadcastReceiver : BroadcastReceiver()
    {

        override fun onReceive(context: Context, intent: Intent)
        {
            if (intent.action == FOLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST)
            {
                val location = intent.getParcelableExtra<Location>(
                    FOLocationService.EXTRA_LOCATION
                )

                if (location != null)
                {
                    logResultsToScreen("Foreground location: ${location.toText()}")
                    if(driverId!="-1")
                    {
                        viewModel.sendLocationToServer(driverId,location.latitude.toString(),location.longitude.toString())
                    }
                }
            }
        }
    }

    // TODO: Step 1.0, Review Permissions: Method checks if permissions approved.
    private fun foregroundPermissionApproved(): Boolean
    {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    // TODO: Step 1.0, Review Permissions: Method requests permissions.
    private fun requestForegroundPermissions()
    {
        val provideRationale = foregroundPermissionApproved()

        // If the user denied a previous request, but didn't check "Don't ask again", provide
        // additional rationale.
        if (provideRationale)
        {
            Snackbar.make(
                findViewById(R.id.drawer_layout),
                R.string.permission_rationale,
                Snackbar.LENGTH_LONG
            )
                .setAction(R.string.ok) {
                    // Request permission
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
                    )
                }
                .show()
        }
        else
        {
            Log.d(TAG, "Request foreground only permission")
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    // TODO: Step 1.0, Review Permissions: Handles permission result.
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    )
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d(TAG, "onRequestPermissionResult")

        when (requestCode)
        {
            REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE -> when
            {
                grantResults.isEmpty() ->
                    // If user interaction was interrupted, the permission request
                    // is cancelled and you receive empty arrays.
                    Log.d(TAG, "User interaction was cancelled.")

                grantResults[0] == PackageManager.PERMISSION_GRANTED ->
                    // Permission was granted.
                    foregroundOnlyLocationService?.subscribeToLocationUpdates()

                else ->
                {
                    // Permission denied.
                    setLocationUpdate(false)

                    Snackbar.make(
                        findViewById(R.id.drawer_layout),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_LONG
                    )
                        .setAction(R.string.settings) {
                            // Build intent that displays the App settings screen.
                            val intent = Intent()
                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts(
                                "package",
                                BuildConfig.APPLICATION_ID,
                                null
                            )
                            intent.data = uri
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                        .show()
                }
            }
        }
    }

    private fun setLocationUpdate(trackingLocation: Boolean)
    {
        locationUpdate = trackingLocation;
    }

    private fun logResultsToScreen(output: String)
    {
        val outputWithPreviousLogs = "$output\n"
        Log.d(TAG, "logResultsToScreen($outputWithPreviousLogs)")
    }

    override fun onStart()
    {
        super.onStart()
        val serviceIntent = Intent(this, FOLocationService::class.java)
        bindService(serviceIntent, foregroundOnlyServiceConnection, Context.BIND_AUTO_CREATE)
        Handler(Looper.getMainLooper()).postDelayed({
            foregroundOnlyLocationService?.subscribeToLocationUpdates()
        }, 2000)
    }

    override fun onStop()
    {
        if (foregroundOnlyLocationServiceBound)
        {
            unbindService(foregroundOnlyServiceConnection)
            foregroundOnlyLocationServiceBound = false
        }
        super.onStop()
    }

    override fun onDestroy()
    {
        unregisterReceiver(foregroundOnlyBroadcastReceiver)
        foregroundOnlyLocationService?.unsubscribeToLocationUpdates()
        foregroundOnlyLocationService?.stopSelf()
        super.onDestroy()
    }

    override fun onBackPressed()
    {
        if (drawer_layout.isDrawerOpen(GravityCompat.START))
        {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else
        {
            super.onBackPressed()
        }
    }

}
