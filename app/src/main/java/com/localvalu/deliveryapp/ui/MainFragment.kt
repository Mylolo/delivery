package com.localvalu.deliveryapp.ui

import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.localvalu.deliveryapp.R
import com.localvalu.deliveryapp.di.MainDH
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverycore.utils.UserUtils
import java.io.IOException
import javax.inject.Inject


class MainFragment : Fragment()
{
    private val TAG = "MainFragment"


    companion object
    {
        private const val USER_LOGGED_IN_CHECK = "USER_LOGGED_IN_CHECK"
    }

    private val component by lazy { MainDH.mainComponent() }


    @Inject
    lateinit var viewModelFactory: MainViewModelFactory

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    private var needToLogout = false;

    private var navigateActionId=-1;

    private var driverId="-1";

    private var poolName = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        Log.d(TAG, "arguments ")
        navigateActionId = R.id.action_mainFragment_to_deliveryDashboardFragment
        arguments?.getBoolean(BundleUtils.BUNDLE_LOGOUT)?.let { needToLogout = it }
        arguments?.getInt(BundleUtils.BUNDLE_NAVIGATE_ACTION_ID)?.let { navigateActionId = it}
        arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID)?.let { driverId = it}
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        observeLogoutListener()
        observerLoginListener()
        viewModel.checkLogin()
    }

    /*private fun initiateLoginDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.loginOutCome.observe(viewLifecycleOwner, Observer<Outcome<AppLoggedInUser>> { outcome ->
            Log.d(TAG, "initiateLoginDataListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {

                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "initiateLoginDataListener: Successfully loaded data")
                    if(outcome.data.user.errorDetails.errorCode == 0)
                    {
                        Log.d(TAG, "initiateLoginDataListener: User Logged in")
                        Log.d(TAG, "initiateLoginDataListener: Username " + outcome.data.user.name)
                        /*val welComeString = StringBuilder();
                        welComeString.append(getString(R.string.welcome)).append(" ")
                            .append(outcome.data.user.name).append(" ");
                        Toast.makeText(context,welComeString.toString(),Toast.LENGTH_LONG).show()*/
                        if(needToLogout)
                        {
                            gotoLoginFragment()
                        }
                        else
                        {
                            driverId = outcome.data.user.driverId
                            gotoDestination()
                        }
                    }
                    else
                    {
                        //Toast.makeText(this,""+outcome.data.user.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                        gotoLoginFragment()
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {

                        //Toast.makeText(this,getString(com.localvalu.deliverylogin.R.string.msg_no_internet_connection),Toast.LENGTH_LONG).show()
                    }

                    else
                    {

                        //Toast.makeText(this,getString(com.localvalu.deliverylogin.R.string.msg_unknown_error_try_again),Toast.LENGTH_LONG).show()
                    }
                }

            }
        })
    }*/

    private fun observerLoginListener()
    {
        viewModel.loginOutCome.observe(viewLifecycleOwner, Observer<Outcome<AppLoggedInUser>> { outcome ->
            Log.d(TAG, "observerLoginListener: $outcome")
            when (outcome)
            {
                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {

                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "observerLoginListener: Successfully loaded data")
                    if(outcome.data.userLoggedIn)
                    {
                        Log.d(TAG, "observerLoginListener: User Logged in")
                        Log.d(TAG, "observerLoginListener: User Logged in${outcome.data.userType}")

                        if(needToLogout)
                        {
                            viewModel.logout()

                        }
                        else
                        {
                            when(outcome.data.userType)
                            {
                                UserUtils.USER_TYPE_ADMIN->
                                {
                                    poolName = outcome.data.poolUser.poolName
                                    navigateActionId = R.id.action_mainFragment_to_actionDriversListFragment
                                    gotoDestination(outcome.data.userType)
                                }
                                UserUtils.USER_TYPE_DRIVER->
                                {
                                    driverId = outcome.data.driverUser.driverId
                                    gotoDestination(outcome.data.userType)
                                }
                                else->
                                {
                                    gotoLoginFragment()
                                }
                            }

                        }
                    }
                    else
                    {
                        //Toast.makeText(this,""+outcome.data.user.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                        gotoLoginFragment()
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {

                        //Toast.makeText(this,getString(com.localvalu.deliverylogin.R.string.msg_no_internet_connection),Toast.LENGTH_LONG).show()
                    }

                    else
                    {

                        //Toast.makeText(this,getString(com.localvalu.deliverylogin.R.string.msg_unknown_error_try_again),Toast.LENGTH_LONG).show()
                    }
                }

            }
        });
    }

    private fun observeLogoutListener()
    {
        viewModel.logoutOutCome.observe(viewLifecycleOwner, Observer { outcome->
            Log.d(TAG, "observeLogoutListener: $outcome")
            when (outcome)
            {
                is Outcome.Progress->
                {

                }
                is Outcome.Success->
                {
                    if(outcome.data)
                    {
                        gotoLoginFragment()
                    }
                }
                is Outcome.Failure->
                {

                }
            }

        })
    }

    private fun gotoDestination(userType:Int)
    {
        Log.d(TAG, "gotoDestination")
        if(navigateActionId==R.id.action_mainFragment_to_actionLoginFragment)
        {
            gotoLoginFragment()
        }
        else
        {
            var bundle = Bundle()
            when(userType)
            {
                UserUtils.USER_TYPE_ADMIN->
                {
                    bundle = bundleOf(BundleUtils.BUNDLE_POOL_NAME to poolName)
                }
                UserUtils.USER_TYPE_DRIVER->
                {
                    bundle = bundleOf(BundleUtils.BUNDLE_DRIVER_ID to driverId)
                }
            }

            val navOptions: NavOptions = NavOptions.Builder()
                .setPopUpTo(R.id.mainFragment, true)
                .build();
            findNavController().navigate(
                navigateActionId,
                bundle,
                navOptions
            )
        }

    }

    private fun gotoLoginFragment()
    {
        Log.d(TAG, "gotoLoginFragment")
        val destinationId = R.id.action_mainFragment_to_actionLoginFragment
        val tempNavigationId = R.id.action_loginFragment_to_deliveryDashboardFragment;
        var bundle = bundleOf(BundleUtils.BUNDLE_NAVIGATE_ACTION_ID to tempNavigationId)
        val navOptions: NavOptions = NavOptions.Builder()
            .setPopUpTo(R.id.mainFragment, true)
            .build();
        findNavController().navigate(
            destinationId,
            bundle,
            navOptions
        )

    }

    override fun onSaveInstanceState(outState: Bundle)
    {
        super.onSaveInstanceState(outState)
        Log.i(TAG, "onSaveInstanceState")
    }

}