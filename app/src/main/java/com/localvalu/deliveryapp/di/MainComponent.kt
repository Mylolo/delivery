package com.localvalu.deliveryapp.di

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliveryapp.data.*
import com.localvalu.deliveryapp.ui.MainActivity
import com.localvalu.deliveryapp.ui.MainFragment
import com.localvalu.deliveryapp.ui.MainViewModelFactory
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.data.LoginDataContract
import com.localvalu.deliverylogin.data.LoginLocalData
import com.localvalu.deliverylogin.di.LoginComponent
import com.localvalu.deliverylogin.di.LoginModule
import com.localvalu.deliverylogin.di.LoginScope
import com.localvalu.deliverylogin.ui.login.LoginViewModelFactory
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@MainScope
@Component(dependencies = [CoreComponent::class],modules = [MainModule::class])
interface MainComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson(): Gson
    fun context():Context
    fun inject(mainActivity: MainActivity)
    fun inject(mainFragment: MainFragment)
}

@Module
class MainModule
{

    /*ViewModel*/
    @Provides
    @MainScope
    fun mainViewModelFactory(repository: MainDataContract.Repository, compositeDisposable: CompositeDisposable):
            MainViewModelFactory = MainViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @MainScope
    fun mainRepo(local: MainDataContract.Local,remote:MainDataContract.Remote,
                 scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            MainDataContract.Repository = MainRepository(local,remote, scheduler, compositeDisposable)

    @Provides
    @MainScope
    fun mainViewRemoteData(locationTrackService: LocationTrackService):
            MainDataContract.Remote = MainRemoteData(locationTrackService)

    @Provides
    @MainScope
    fun localData(sharedPreferences: SharedPreferences, scheduler: Scheduler,gson: Gson):
            MainDataContract.Local = MainLocalData(sharedPreferences, scheduler,gson)

    @Provides
    @MainScope
    fun sendLocationService(@Named("DotNetServer")retrofit: Retrofit): LocationTrackService = retrofit.create(
        LocationTrackService::class.java)


    @Provides
    @MainScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()
}