package com.localvalu.deliveryapp.di

import com.localvalu.deliverycore.application.CoreApp
import com.localvalu.deliverylogin.di.DaggerLoginComponent
import com.localvalu.deliverylogin.di.LoginComponent
import javax.inject.Singleton


@Singleton
object MainDH
{
    private var mainComponent: MainComponent? = null

    fun mainComponent(): MainComponent {
        if (mainComponent == null)
            mainComponent = DaggerMainComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return mainComponent as MainComponent
    }

    fun destroyMainComponent() {
        mainComponent = null
    }


}