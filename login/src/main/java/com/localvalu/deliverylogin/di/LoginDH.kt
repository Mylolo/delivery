package com.localvalu.deliverylogin.di

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton


@Singleton
object LoginDH
{
    private var loginComponent: LoginComponent? = null


    fun loginComponent(): LoginComponent {
        if (loginComponent == null)
            loginComponent = DaggerLoginComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return loginComponent as LoginComponent
    }

    fun destroyLoginComponent() {
        loginComponent = null
    }


}