package com.localvalu.deliverylogin.di

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class LoginScope