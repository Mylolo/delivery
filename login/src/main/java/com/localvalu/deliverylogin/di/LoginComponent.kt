package com.localvalu.deliverylogin.di

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.ui.login.LoginActivity
import com.localvalu.deliverylogin.data.*
import com.localvalu.deliverylogin.ui.login.LoginViewModelFactory
import com.localvalu.deliverylogin.ui.login.fragments.LoginFragment
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@LoginScope
@Component(dependencies = [CoreComponent::class],modules = [LoginModule::class])
interface LoginComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun loginService(): LoginService
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson():Gson
    fun inject(loginActivity: LoginActivity)
    fun inject(loginFragment: LoginFragment)
}

@Module
class LoginModule
{

    /*ViewModel*/
    @Provides
    @LoginScope
    fun loginViewModelFactory(repository: LoginDataContract.Repository, compositeDisposable: CompositeDisposable):
            LoginViewModelFactory = LoginViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @LoginScope
    fun loginRepo(local: LoginDataContract.Local, remote: LoginDataContract.Remote,
                  scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            LoginDataContract.Repository = LoginRepository(local, remote, scheduler, compositeDisposable)

    @Provides
    @LoginScope
    fun remoteData(loginService: LoginService): LoginDataContract.Remote = LoginRemoteData(loginService)

    @Provides
    @LoginScope
    fun localData(sharedPreferences: SharedPreferences, scheduler: Scheduler,gson: Gson):
            LoginDataContract.Local = LoginLocalData(sharedPreferences, scheduler,gson)

    @Provides
    @LoginScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @LoginScope
    fun loginService(@Named("DotNetServer")retrofit: Retrofit): LoginService = retrofit.create(LoginService::class.java)



}