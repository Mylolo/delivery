package com.localvalu.deliverylogin.`interface`

import com.localvalu.deliverylogin.di.LoginComponent

interface LoginComponentProvider
{
    fun provideLoginComponent(): LoginComponent
}