package com.localvalu.deliverylogin.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.base.model.DriverLoginResult
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.UserUtils

import com.localvalu.deliverylogin.data.LoginDataContract
import com.localvalu.deliverylogin.data.model.request.LoginRequest
import com.localvalu.deliverylogin.di.LoginDH
import io.reactivex.disposables.CompositeDisposable

class LoginViewModel(
    private val loginRepository: LoginDataContract.Repository,
    private val compositeDisposable: CompositeDisposable) : ViewModel()
{
    val loginOutcome: LiveData<Outcome<AppLoggedInUser>> by lazy {
        //Convert publish subject to livedata
        loginRepository.loginFetchOutcome.toLiveData(compositeDisposable)
    }

    val fcmOutCome: LiveData<Outcome<String>> by lazy {
        //Convert publish subject to livedata
        loginRepository.fcmFetchOutcome.toLiveData(compositeDisposable)
    }

    val logoutOutCome: LiveData<Outcome<Boolean>> by lazy {
        //Convert publish subject to livedata
        loginRepository.logoutOpOutcome.toLiveData(compositeDisposable)
    }

    // Create a LiveData with a String
    var validUserName: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    var validPassword: MutableLiveData<Boolean> = MutableLiveData<Boolean>()


    fun login(userType:Int,password: String, email: String,fcmId:String)
    {
        when(userType)
        {
            UserUtils.USER_TYPE_ADMIN->
                loginRepository.getPoolRemoteUser(LoginRequest("",password,email,fcmId))
            UserUtils.USER_TYPE_DRIVER->
                loginRepository.getDriverRemoteUser(LoginRequest("",password,email,fcmId))
        }
    }

    fun checkLogin()
    {
        if (loginOutcome.value == null)
            loginRepository.getLocalUser()
    }

    fun getFCMID()
    {
        if (fcmOutCome.value == null)
            loginRepository.getFCMID()
    }

    fun saveFCMID(fcmID:String)
    {
        loginRepository.saveFCMID(fcmID)
    }

    fun logout()
    {
        if (loginOutcome.value == null)
            loginRepository.logout()
    }

    // A placeholder username validation check
    public fun isUserNameValid(username: String)
    {
        if (username.contains('@'))
        {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
            validUserName.value = true
        }
        else
        {
            validUserName.value = false
        }
    }

    // A placeholder password validation check
    public fun isPasswordValid(password: String)
    {
        validPassword.value = password.length > 5
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        LoginDH.destroyLoginComponent()
    }
}
