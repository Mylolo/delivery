package com.localvalu.deliverylogin.ui.login.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.localvalu.deliverylogin.R
import kotlinx.android.synthetic.main.fragment_error.*

class ErrorFragment : Fragment()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_error,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        val errorString  = arguments?.getString("ErrorMessage","")
        tvError.setText(errorString)
    }

}