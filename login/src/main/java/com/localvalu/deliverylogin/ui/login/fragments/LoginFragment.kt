package com.localvalu.deliverylogin.ui.login.fragments

import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.localvalu.deliverycore.base.model.AppLoggedInUser
import com.localvalu.deliverycore.base.model.DriverLoginResult
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverycore.utils.UserUtils
import com.localvalu.deliverylogin.R
import com.localvalu.deliverylogin.di.LoginDH
import com.localvalu.deliverylogin.ui.login.LoginViewModel
import com.localvalu.deliverylogin.ui.login.LoginViewModelFactory
import kotlinx.android.synthetic.main.fragment_login.*
import java.io.IOException
import javax.inject.Inject

class LoginFragment : Fragment()
{
    private val TAG = "LoginFragment"

    private val component by lazy { LoginDH.loginComponent() }

    @Inject
    lateinit var viewModelFactory: LoginViewModelFactory

    private val viewModel: LoginViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    private var fcmID: String=""

    private var validUserName = false;

    private var validPassword = false;

    private var oneTimeVisit=false;

    private var navigateActionId=-1;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
        arguments?.getInt(BundleUtils.BUNDLE_NAVIGATE_ACTION_ID)?.let { navigateActionId = it}
        //Firebase
        FirebaseApp.initializeApp(requireContext());
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_login,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
        observeValidateListener()
        observeFCMDataListener()
        observerLoginListener()
        viewModel.getFCMID()
        btnDriverLogin.setOnClickListener {
            viewModel.login(UserUtils.USER_TYPE_DRIVER,etPassword.text.toString(),etUserName.text.toString(),fcmID)
            etUserName.setText("")
            etPassword.setText("")
        }
        btnAdminLogin.setOnClickListener {
            viewModel.login(UserUtils.USER_TYPE_ADMIN,etPassword.text.toString(),etUserName.text.toString(),fcmID)
            etUserName.setText("")
            etPassword.setText("")
        }
        btnForgetPassword.setOnClickListener {gotoForgetPasswordFragment()}
        /*etUserName.doAfterTextChanged {
            if(etUserName.text.toString().isNotEmpty())
                viewModel.isUserNameValid(etUserName.text.toString())
        }
        etPassword.doAfterTextChanged {
            if(etPassword.text.toString().isNotEmpty())
            viewModel.isPasswordValid(etPassword.text.toString())
        }*/
    }

    private fun setProperties()
    {
        val fontType = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular)
        etUserName.typeface = fontType
        tilUserName.typeface = fontType
        etPassword.inputType = (InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        tilPassword.typeface = fontType
        etPassword.typeface = fontType
    }

    private  fun observeValidateListener()
    {
        viewModel.validUserName?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult)
            {
                tilUserName.error = null
                tilUserName.isErrorEnabled = false
                validUserName=true;
                //enableDisableSignInButton()

            }
            else
            {
                tilUserName.isErrorEnabled = true;
                tilUserName.error = getString(R.string.invalid_username);
                validUserName=false;
                //enableDisableSignInButton()
            }
        })

        viewModel.validPassword?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult)
            {
                tilPassword.error = null
                tilPassword.isErrorEnabled = false
                validPassword=true;
                //enableDisableSignInButton()
            }
            else
            {
                tilPassword.isErrorEnabled = true;
                tilPassword.error = getString(R.string.invalid_password);
                validPassword=false;
                //enableDisableSignInButton()
            }
        })
    }

    /*private fun observeLoginDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.driverLoginOutCome.observe(viewLifecycleOwner, Observer<Outcome<DriverLoginResult>> { outcome ->
            Log.d(TAG, "observeLoginDataListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {
                        clLoginViewLayout.visibility = View.GONE
                        progressBarFragmentLogin.visibility = View.VISIBLE
                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "observeLoginDataListener: Successfully loaded data")
                    progressBarFragmentLogin.visibility = View.GONE
                    if(outcome.data.user.errorDetails.errorCode == 0)
                    {
                        /*val welComeString = StringBuilder();
                        welComeString.append(getString(R.string.welcome)).append(" ")
                            .append(outcome.data.user.name).append(" ");
                        Toast.makeText(context,welComeString.toString(),Toast.LENGTH_LONG).show()*/
                        gotoDestination(outcome.data.user.driverId)
                    }
                    else
                    {
                        Toast.makeText(context,""+outcome.data.user.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                        clLoginViewLayout.visibility = View.VISIBLE
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {
                        progressBarFragmentLogin.visibility = View.GONE
                        clLoginViewLayout.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_no_internet_connection),Toast.LENGTH_LONG).show()
                    }

                    else
                    {
                        progressBarFragmentLogin.visibility = View.GONE
                        clLoginViewLayout.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_unknown_error_try_again),Toast.LENGTH_LONG).show()
                    }
                }

            }
        })
    }*/

    private fun observerLoginListener()
    {
        viewModel.loginOutcome.observe(viewLifecycleOwner, Observer<Outcome<AppLoggedInUser>> { outcome ->
            Log.d(TAG, "observerLoginListener: $outcome")
            when (outcome)
            {
                is Outcome.Progress ->
                {
                    if (outcome.loading)
                    {
                        clLoginViewLayout.visibility = View.GONE
                        progressBarFragmentLogin.visibility = View.VISIBLE
                    }
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "observerLoginListener: Successfully loaded data")
                    if(outcome.data.userLoggedIn)
                    {
                        Log.d(TAG, "observerLoginListener: User Logged in")
                        Log.d(TAG, "observerLoginListener: User Logged in${outcome.data.userType}")

                        when(outcome.data.userType)
                        {
                            UserUtils.USER_TYPE_ADMIN->
                            {
                                if(outcome.data.poolUser.errorDetails.errorCode==0)
                                {
                                    gotoDestination(UserUtils.USER_TYPE_ADMIN,outcome.data.poolUser.poolName)
                                }
                                else
                                {
                                    Toast.makeText(requireContext(),outcome.data.poolUser.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                                    clLoginViewLayout.visibility = View.VISIBLE
                                    progressBarFragmentLogin.visibility = View.GONE
                                }

                            }
                            UserUtils.USER_TYPE_DRIVER->
                            {
                                if(outcome.data.driverUser.errorDetails.errorCode==0)
                                {
                                    gotoDestination(UserUtils.USER_TYPE_DRIVER,outcome.data.driverUser.driverId)
                                }
                                else
                                {
                                    Toast.makeText(requireContext(),outcome.data.driverUser.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                                    clLoginViewLayout.visibility = View.VISIBLE
                                    progressBarFragmentLogin.visibility = View.GONE
                                }

                            }
                        }
                    }
                    else
                    {
                        when(outcome.data.userType)
                        {
                            UserUtils.USER_TYPE_ADMIN->
                            {
                                Toast.makeText(context,""+outcome.data.poolUser.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                                clLoginViewLayout.visibility = View.VISIBLE
                                progressBarFragmentLogin.visibility = View.GONE
                            }
                            UserUtils.USER_TYPE_DRIVER->
                            {
                                Toast.makeText(context,""+outcome.data.driverUser.errorDetails.errorMessage,Toast.LENGTH_LONG).show()
                                clLoginViewLayout.visibility = View.VISIBLE
                                progressBarFragmentLogin.visibility = View.GONE
                            }
                        }

                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                    {
                        progressBarFragmentLogin.visibility = View.GONE
                        clLoginViewLayout.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_no_internet_connection),Toast.LENGTH_LONG).show()
                    }

                    else
                    {
                        progressBarFragmentLogin.visibility = View.GONE
                        clLoginViewLayout.visibility = View.VISIBLE
                        Toast.makeText(context,getString(R.string.msg_unknown_error_try_again),Toast.LENGTH_LONG).show()
                    }
                }

            }
        });
    }

    private fun observeFCMDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.fcmOutCome.observe(viewLifecycleOwner, Observer<Outcome<String>> { outcome ->
            Log.d(TAG, "observeFCMDataListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {

                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "observeDataListener: Successfully loaded data")

                    if (outcome.data.equals("NFL") || outcome.data.equals(""))
                    {
                        Log.d(TAG, "FCMID-NotFoundInLocal")
                        getFCMIDFromApi();
                    }
                    else if (outcome.data.equals("NF"))
                    {
                        Log.d(TAG, "FCMID-NotFound")
                        getFCMIDFromApi();
                    }
                    else
                    {
                        //Toast.makeText(context,"FCMID"+outcome.data,Toast.LENGTH_LONG).show()
                        fcmID = outcome.data
                    }
                }

                is Outcome.Failure ->
                {

                    Toast.makeText(
                        requireContext(), getString(R.string.msg_unknown_error_try_again).toString(),
                        Toast.LENGTH_LONG
                    ).show()
                }

            }
        })
    }

    private fun getFCMIDFromApi()
    {
        //Observe changes to the local
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful)
                {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                var token = task.result?.token
                if (token == null) token = "NF"
                //Log.d(TAG, "getFCMIDFromApi-FCMID-"+ token)
                fcmID = token
                viewModel.saveFCMID(token)
            })
    }

    private fun gotoDestination(userType:Int,requiredId: String)
    {
        var bundle = Bundle()
        when(userType)
        {
            UserUtils.USER_TYPE_ADMIN ->
            {
                bundle = bundleOf(BundleUtils.BUNDLE_POOL_NAME to requiredId)
            }
            UserUtils.USER_TYPE_DRIVER ->
            {
                bundle = bundleOf(BundleUtils.BUNDLE_DRIVER_ID to requiredId)
            }
        }
        findNavController().navigate(navigateActionId,bundle)
    }

    private fun gotoForgetPasswordFragment()
    {
        oneTimeVisit=true;
        val navGraphId = resources.getIdentifier("nav_graph","navigation","com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier("action_loginFragment_to_actionForgetPasswordFragment","id","com.localvalu.deliveryapp")
        val popUpToFragmentId = resources.getIdentifier("mainFragment","id","com.localvalu.deliveryapp")
        if(navGraphId!=0)
        {
            val navOptions: NavOptions = NavOptions.Builder()
                .setPopUpTo(popUpToFragmentId, true)
                .build();
            findNavController().navigate(actionId,null,navOptions)
        }
    }

    private fun enableDisableSignInButton()
    {
        btnDriverLogin.isEnabled = validUserName && validPassword;
    }

}