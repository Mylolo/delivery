package com.localvalu.deliverylogin.ui.login

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliverycore.base.model.DriverLoginResult
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.R
import com.localvalu.deliverylogin.di.LoginDH
import com.localvalu.deliverylogin.ui.login.fragments.LoginFragment
import kotlinx.android.synthetic.main.activity_login.*
import java.io.IOException
import java.lang.StringBuilder
import javax.inject.Inject

class LoginActivity : AppCompatActivity()
{

    private val component by lazy { LoginDH.loginComponent() }

    @Inject
    lateinit var viewModelFactory: LoginViewModelFactory

    private val viewModel: LoginViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    private var isLoginFragmentAdded:Boolean=false;

    private val context: Context by lazy { this }

    private val TAG = "LoginActivity"

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_login)
        this.getSupportActionBar()?.hide();
        component.inject(this)
        viewModel.checkLogin();
        //if(!isLoginFragmentAdded)initiateDataListener();
    }

    /*private fun initiateDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.driverLoginOutCome.observe(this, Observer<Outcome<DriverLoginResult>> { outcome ->
            Log.d(TAG, "initiateDataListener: $outcome")
            when (outcome)
            {

                is Outcome.Progress ->
                {
                    if (outcome.loading)
                        progressBar.visibility = View.VISIBLE
                    else progressBar.visibility = View.GONE
                }

                is Outcome.Success ->
                {
                    Log.d(TAG, "initiateDataListener: Successfully loaded data")
                    progressBar.visibility = View.GONE
                    if (outcome.data.user.errorDetails.errorCode != 0)
                    {
                        addLoginFragment();
                    }

                    else
                    {
                        val welComeString = StringBuilder();
                        welComeString.append(getString(R.string.welcome)).append(" ")
                            .append(outcome.data.user.name).append(" ");
                        Toast.makeText(context,welComeString.toString(),Toast.LENGTH_LONG).show()
                        addOTPFragment()
                    }
                }

                is Outcome.Failure ->
                {

                    if (outcome.e is IOException)
                        addErrorFragment(getString(R.string.msg_no_internet_connection))
                    else
                        addErrorFragment(getString(R.string.msg_unknown_error_try_again))
                }

            }
        })
    }*/

    fun addLoginFragment()
    {
        supportFragmentManager.beginTransaction().add(R.id.flContainer,LoginFragment(),"LoginFragment").commit()
        isLoginFragmentAdded=true;
    }

    fun addOTPFragment()
    {

    }

    fun addErrorFragment(errorMessage:String)
    {
        var bundle = Bundle()
        bundle.putString("ErrorMessage",errorMessage)
        val loginFragment:LoginFragment= LoginFragment();
        loginFragment.arguments=bundle;
        supportFragmentManager.beginTransaction().add(R.id.flContainer,loginFragment,"LoginFragment").commit()
    }
}
