package com.localvalu.deliverylogin.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliverylogin.data.LoginDataContract
import com.localvalu.deliverylogin.data.LoginRepository
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory(
    private val repository: LoginDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
            return LoginViewModel(repository, compositeDisposable) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
