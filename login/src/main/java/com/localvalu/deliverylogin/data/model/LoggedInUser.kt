package com.localvalu.deliverylogin.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser(@SerializedName("username") val username: String,
                        @SerializedName("id") val id: Int
):Serializable
