package com.localvalu.deliverylogin.data

import android.os.Handler
import android.os.Looper
import com.localvalu.deliverycore.base.model.*
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverycore.utils.UserUtils
import com.localvalu.deliverylogin.data.model.request.LoginRequest
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(
    private val local: LoginDataContract.Local,
    private val remote: LoginDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : LoginDataContract.Repository

{

    override val loginFetchOutcome: PublishSubject<Outcome<AppLoggedInUser>> =
        PublishSubject.create<Outcome<AppLoggedInUser>>()

    override val fcmFetchOutcome: PublishSubject<Outcome<String>> =
        PublishSubject.create<Outcome<String>>()

    override val logoutOpOutcome:PublishSubject<Outcome<Boolean>> =
        PublishSubject.create<Outcome<Boolean>>()


    override fun getLocalUser()
    {
        loginFetchOutcome.loading(true)
        //Observe changes to the local
        local.getLocalUser()
            .performOnBackOutOnMain(scheduler)
            .subscribe({appLoggedInUser -> loginFetchOutcome.success(appLoggedInUser)}
                , { error-> handleLoginError(error)})
            .addTo(compositeDisposable)
    }

    override fun logout()
    {
        logoutOpOutcome.loading(true)
        local.logout().performOnBackOutOnMain(scheduler).
        subscribe({logoutResult ->logoutOpOutcome.success(logoutResult) },{
                error->handleLogoutError(error)
        })
            .addTo(compositeDisposable);
    }

    override fun getFCMID()
    {
        fcmFetchOutcome.loading(true)
        //Observe changes to the local
        local.getFCMID()
            .performOnBackOutOnMain(scheduler)
            .subscribe({fcmID -> fcmFetchOutcome.success(fcmID)}
                , { error-> handleFCMError(error)})
            .addTo(compositeDisposable)
    }

    override fun getDriverRemoteUser(loginRequest: LoginRequest)
    {
        loginFetchOutcome.loading(true)
        Single.wrap(remote.getDriverUser(loginRequest)).performOnBackOutOnMain(scheduler)
            .doAfterSuccess {Handler(Looper.getMainLooper()).postDelayed(Runnable { getLocalUser() },500)}
            .subscribe({loginResponse-> saveDriverUser(loginResponse.driverLoginResult)},
                { error -> handleLoginError(error) })
            .addTo(compositeDisposable)
    }

    override fun getPoolRemoteUser(loginRequest: LoginRequest)
    {
        loginFetchOutcome.loading(true)
        Single.wrap(remote.getPoolUser(loginRequest)).performOnBackOutOnMain(scheduler)
            .doAfterSuccess {Handler(Looper.getMainLooper()).postDelayed(Runnable { getLocalUser() },500)}
            .subscribe({loginResponse-> savePoolUser(loginResponse.poolLoginResult)},
                { error -> handleLoginError(error) })
            .addTo(compositeDisposable)
    }

    override fun saveFCMID(fcmID: String)
    {
        local.saveFCMID(fcmID)
    }

    override fun saveDriverUser(driverLoginResult: DriverLoginResult)
    {
        local.saveUser(AppLoggedInUser(true,UserUtils.USER_TYPE_DRIVER,driverLoginResult.user,emptyPoolUser(),""))
        getFCMID()
    }

    override fun savePoolUser(poolLoginResult: PoolLoginResult)
    {
        local.saveUser(AppLoggedInUser(true,UserUtils.USER_TYPE_ADMIN,emptyDriverUser(),poolLoginResult.user,""))
        getFCMID()
    }

    override fun handleLoginError(error: Throwable)
    {
        loginFetchOutcome.failed(error)
    }

    override fun handleLogoutError(error: Throwable)
    {
        logoutOpOutcome.failed(error)
    }

    override fun handleFCMError(error: Throwable)
    {
        fcmFetchOutcome.failed(error)
    }

    private fun emptyDriverUser(): DriverUser
    {
        return DriverUser(
            "",
            "",
            "",
            "", "","","", errorDetails()
        )

    }

    private fun emptyPoolUser(): PoolUser
    {
        return PoolUser("",-1,errorDetails())
    }

    private fun errorDetails(): ErrorDetails
    {
        return ErrorDetails(
            -1,
            "Not LoggedIn"
        )
    }

}

