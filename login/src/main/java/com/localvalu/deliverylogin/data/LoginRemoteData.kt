package com.localvalu.deliverylogin.data

import com.localvalu.deliverylogin.data.model.request.LoginRequest


class LoginRemoteData(private val loginService: LoginService) : LoginDataContract.Remote

{
    override fun getDriverUser(loginRequest: LoginRequest) = loginService.driverLogin(loginRequest)
    override fun getPoolUser(loginRequest: LoginRequest) = loginService.poolLogin(loginRequest)
}