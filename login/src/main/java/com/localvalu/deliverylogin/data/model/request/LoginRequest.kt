package com.localvalu.deliverylogin.data.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(@SerializedName("sToken") var token: String,
                        @SerializedName("sPassword") val password: String,
                        @SerializedName("sEmail") val email: String,
                        @SerializedName("FCMId") val fcmId: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}