package com.localvalu.deliverylogin.data

import com.localvalu.deliverycore.base.model.DriverLogInResponse
import com.localvalu.deliverycore.base.model.PoolLogInResponse
import com.localvalu.deliverylogin.data.model.request.LoginRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService
{
    //@GET("users/{user}")
    //fun getUserInfo(@Path("user") userId:Int): Single<LoggedInUser>

    @POST("driverlogin")
    fun driverLogin(@Body loginRequest: LoginRequest): Single<DriverLogInResponse>

    @POST("Poollogin")
    fun poolLogin(@Body loginRequest: LoginRequest): Single<PoolLogInResponse>
}