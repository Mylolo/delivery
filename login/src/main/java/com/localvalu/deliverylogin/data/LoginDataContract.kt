package com.localvalu.deliverylogin.data

import com.localvalu.deliverycore.base.model.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.request.LoginRequest
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject

interface LoginDataContract
{
    interface Repository
    {
        val loginFetchOutcome: PublishSubject<Outcome<AppLoggedInUser>>
        val fcmFetchOutcome: PublishSubject<Outcome<String>>
        val logoutOpOutcome: PublishSubject<Outcome<Boolean>>
        fun getLocalUser()
        fun getFCMID()
        fun saveFCMID(fcmID:String)
        fun logout()
        fun getDriverRemoteUser(loginRequest: LoginRequest)
        fun getPoolRemoteUser(loginRequest: LoginRequest)
        fun saveDriverUser(loginResult: DriverLoginResult)
        fun savePoolUser(loginResult: PoolLoginResult)
        fun handleLoginError(error: Throwable)
        fun handleLogoutError(error: Throwable)
        fun handleFCMError(error: Throwable)
    }

    interface Local
    {
        fun getLocalUser(): Flowable<AppLoggedInUser>
        fun getFCMID():Single<String>
        fun saveFCMID(fcmID:String)
        fun saveUser(loggedInUser: AppLoggedInUser)
        fun logout():Single<Boolean>
    }

    interface Remote
    {
        fun getDriverUser(loginRequest: LoginRequest): Single<DriverLogInResponse>
        fun getPoolUser(loginRequest: LoginRequest): Single<PoolLogInResponse>
    }

}