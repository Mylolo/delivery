package com.localvalu.deliveryaccount.data.mobilenumbervalidation

import com.localvalu.deliveryaccount.data.model.request.MobileNumberValidationRequest
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.DriverMobileNumberValidationResponse
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface MobileNumberValidationDataContract
{
    interface Repository
    {
        //Mobile Number Validation
        val mobileNumberValidationFetchOutcome: PublishSubject<Outcome<DriverMobileNumberValidationResponse>>
        fun mobileNumberValidation(mobileNumberValidationRequest: MobileNumberValidationRequest)
        fun handleMobileNumberValidationError(error: Throwable)
        //Send sms
        val sendSMSFetchOutcome: PublishSubject<Outcome<SendSMSResponse>>
        fun sendSMS(mobileNumberValidationRequest: MobileNumberValidationRequest)
        fun handleSendSMSError(error: Throwable)

    }

    interface Remote
    {
        //Mobile Number Validation
        fun remoteMobileNumberValidation(mobileNumberValidationRequest: MobileNumberValidationRequest): Single<DriverMobileNumberValidationResponse>
        //Send sms
        fun remoteSendSMS(mobileNumberValidationRequest: MobileNumberValidationRequest): Single<SendSMSResponse>
    }

}

