package com.localvalu.deliveryaccount.data.model.request

import com.google.gson.annotations.SerializedName

data class OTPVerificationRequest(@SerializedName("Token") var token: String,
                                  @SerializedName("MobileNo") val mobileNumber: String,
                                  @SerializedName("OTP") val otp: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}