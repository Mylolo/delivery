package com.localvalu.deliveryaccount.data.profileinfo

import com.localvalu.deliveryaccount.data.model.request.ProfileRequest
import com.localvalu.deliveryaccount.data.model.response.profileresponse.DriverProfileResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface ProfileInfoService
{
    @POST("DriverProfile")
    fun profileInfo(@Body profileRequest: ProfileRequest): Single<DriverProfileResponse>
}