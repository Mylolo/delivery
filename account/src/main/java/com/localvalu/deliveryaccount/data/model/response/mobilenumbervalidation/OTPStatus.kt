package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class OTPStatus(@SerializedName("msg")  val message: String)
{

}