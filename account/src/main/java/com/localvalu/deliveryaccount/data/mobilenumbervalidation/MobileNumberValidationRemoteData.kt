package com.localvalu.deliveryaccount.data.mobilenumbervalidation

import com.localvalu.deliveryaccount.data.model.request.MobileNumberValidationRequest

class MobileNumberValidationRemoteData(private val mobileNumberValidationService: MobileNumberValidationService,
 private val sendSMSService: SendSMSService)
    : MobileNumberValidationDataContract.Remote

{
    //Mobile Number Validation
    override fun remoteMobileNumberValidation(mobileNumberValidaitonRequest: MobileNumberValidationRequest)
            = mobileNumberValidationService.validateMobileNumber(mobileNumberValidaitonRequest)
    //Send SMS
    override fun remoteSendSMS(mobileNumberValidaitonRequest: MobileNumberValidationRequest)
            = sendSMSService.sendTheSMS(mobileNumberValidaitonRequest)
}