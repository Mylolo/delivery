package com.localvalu.deliveryaccount.data.model.response.profileresponse

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class ProfileResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                              @SerializedName("Name") val name: String,
                                              @SerializedName("Email")  val email: String,
                                              @SerializedName("Mobileno")  val mobileNumber: String,
                                              @SerializedName("Gender")  val gender: String,
                                              @SerializedName("Dob")  val dob: String,
                                              @SerializedName("status")  val status: String,
                                              @SerializedName("message")  val message: String)
{

}