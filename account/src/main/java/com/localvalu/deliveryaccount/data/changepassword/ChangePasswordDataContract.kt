package com.localvalu.deliveryaccount.data.changepassword

import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest
import com.localvalu.deliveryaccount.data.model.response.changepassword.DriverChangePasswordResponse
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface ChangePasswordDataContract
{
    interface Repository
    {
        val changePasswordFetchOutcome: PublishSubject<Outcome<DriverChangePasswordResponse>>
        fun getRemoteChangePassword(changePasswordRequest:ChangePasswordRequest)
        fun handleChangePasswordError(error: Throwable)
    }

    interface Remote
    {
        fun changePassword(changePasswordRequest: ChangePasswordRequest): Single<DriverChangePasswordResponse>
    }

}

