package com.localvalu.deliveryaccount.data.forgetpassword

import com.localvalu.deliveryaccount.data.model.request.ForgetPasswordRequest

class ForgetPasswordRemoteData(private val forgetPasswordService: ForgetPasswordService) : ForgetPasswordDataContract.Remote

{
    override fun forgetPassword(forgetPasswordRequest: ForgetPasswordRequest) = forgetPasswordService.requestForgetPassword(forgetPasswordRequest)
}