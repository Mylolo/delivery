package com.localvalu.deliveryaccount.data.model.request

import com.google.gson.annotations.SerializedName

data class ForgetPasswordRequest(@SerializedName("Token") var token: String,
                                 @SerializedName("MobileNo") val mobileNumber: String,
                                 @SerializedName("TempPassword") val tempPassword: String)

{
    init
    {
        token="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}