package com.localvalu.deliveryaccount.data.forgetpassword

import com.localvalu.deliveryaccount.data.model.request.ForgetPasswordRequest
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.ForgetPasswordResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface ForgetPasswordDataContract
{
    interface Repository
    {
        val forgetPasswordFetchOutcome: PublishSubject<Outcome<ForgetPasswordResponse>>
        fun getRemoteForgetPassword(forgetPasswordRequest: ForgetPasswordRequest)
        fun handleForgetPasswordError(error: Throwable)
    }

    interface Remote
    {
        fun forgetPassword(forgetPasswordRequest: ForgetPasswordRequest): Single<ForgetPasswordResponse>
    }

}

