package com.localvalu.deliveryaccount.data.otpverification

import com.localvalu.deliveryaccount.data.model.request.OTPInsertionRequest
import com.localvalu.deliveryaccount.data.model.request.OTPVerificationRequest

class OTPVerificationRemoteData(private val otpVerificationService: OTPVerificationService) : OTPVerificationDataContract.Remote

{
    override fun verifyOTP(otpVerificationRequest: OTPVerificationRequest) = otpVerificationService.verifyTheOTP(otpVerificationRequest)
    override fun insertOTP(otpInsertionRequest: OTPInsertionRequest) = otpVerificationService.insertTheOTP(otpInsertionRequest)
}