package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class DriverMobileNumberValidationResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                              @SerializedName("DriverId") val driverId: Int,
                                              @SerializedName("status")  val status: String,
                                              @SerializedName("message")  val message: String)
{

}