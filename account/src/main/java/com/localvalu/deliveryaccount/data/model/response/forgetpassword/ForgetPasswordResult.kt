package com.localvalu.deliveryaccount.data.model.response.forgetpassword

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class ForgetPasswordResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                @SerializedName("ResMobileNo") val resMobileNumber: String,
                                @SerializedName("NewPassword") val newPassword: String,
                                @SerializedName("DriverId")  val driverId: String,
                                @SerializedName("TempPassword")  val tempPassword: String,
                                @SerializedName("status")  val status: String,
                                @SerializedName("message")  val message: String)
{

}