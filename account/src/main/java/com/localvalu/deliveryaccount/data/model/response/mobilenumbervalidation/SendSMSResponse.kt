package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPInsertionResult
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResult
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class SendSMSResponse(@SerializedName("OTPSmsStatusResult")
                                          val sendSMSResult: SendSMSResult
):Serializable
