package com.localvalu.deliveryaccount.data.profileinfo

import com.localvalu.deliveryaccount.data.model.request.ProfileRequest


class ProfileRemoteData(private val profileInfoService: ProfileInfoService) : ProfileInfoDataContract.Remote
{
    override fun getRemoteProfileData(profileRequest: ProfileRequest) = profileInfoService.profileInfo(profileRequest)
}