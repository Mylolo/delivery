package com.localvalu.deliveryaccount.data.model.response.changepassword

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class ChangePasswordResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                @SerializedName("status")  val status: String,
                                @SerializedName("message")  val message: String)
{

}