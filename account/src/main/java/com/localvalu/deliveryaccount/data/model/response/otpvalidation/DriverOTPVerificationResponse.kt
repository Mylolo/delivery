package com.localvalu.deliveryaccount.data.model.response.otpvalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPVerificationResult
import com.localvalu.deliverylogin.data.model.OTPVerificationResponse
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class DriverOTPVerificationResponse(@SerializedName("DriverAppResult")
                                          val otpVerificationResponse: OTPVerificationResponse):Serializable
