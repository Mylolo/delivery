package com.localvalu.deliveryaccount.data.model.response.otpvalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPInsertionResult
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class DriverOTPInsertionResponse(@SerializedName("DriverAppResult")
                                          val otpInsertionResponse: OTPInsertionResponse
):Serializable
