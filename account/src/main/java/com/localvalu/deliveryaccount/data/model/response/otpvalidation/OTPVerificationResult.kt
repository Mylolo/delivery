package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class OTPVerificationResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                                 @SerializedName("status")  val status: String,
                                 @SerializedName("message")  val message: String)
{

}