package com.localvalu.deliveryaccount.data.model.response.changepassword

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class DriverChangePasswordResponse(@SerializedName("DriverAppResult")
                                          val changePasswordResponse: ChangePasswordResponse
):Serializable
