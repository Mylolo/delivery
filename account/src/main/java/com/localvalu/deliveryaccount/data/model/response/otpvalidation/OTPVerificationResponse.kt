package com.localvalu.deliverylogin.data.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPVerificationResult
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class OTPVerificationResponse(@SerializedName("OTPVerificationResult")
                                          val otpValidationResult: OTPVerificationResult):Serializable
