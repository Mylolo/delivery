package com.localvalu.deliveryaccount.data.otpverification

import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest
import com.localvalu.deliveryaccount.data.model.request.OTPInsertionRequest
import com.localvalu.deliveryaccount.data.model.request.OTPVerificationRequest
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPInsertionResponse
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPVerificationResponse
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import com.localvalu.deliverylogin.data.model.OTPVerificationResponse
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface OTPVerificationDataContract
{
    interface Repository
    {
        val otpVerificationFetchOutcome: PublishSubject<Outcome<DriverOTPVerificationResponse>>
        val otpInsertionFetchOutcome:PublishSubject<Outcome<DriverOTPInsertionResponse>>
        fun getRemoteOTPVerification(optVerificationRequest:OTPVerificationRequest)
        fun getRemoteOTPInsertion(otpInsertionRequest: OTPInsertionRequest)
        fun handleOTPVerificationError(error: Throwable)
        fun handleOTPInsertionError(error: Throwable)
    }

    interface Remote
    {
        fun verifyOTP(optVerificationRequest: OTPVerificationRequest): Single<DriverOTPVerificationResponse>
        fun insertOTP(otpInsertionRequest: OTPInsertionRequest): Single<DriverOTPInsertionResponse>
    }

}

