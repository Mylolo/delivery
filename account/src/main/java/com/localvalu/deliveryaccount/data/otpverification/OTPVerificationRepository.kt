package com.localvalu.deliveryaccount.data.otpverification


import com.localvalu.deliveryaccount.data.model.request.OTPInsertionRequest
import com.localvalu.deliveryaccount.data.model.request.OTPVerificationRequest
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPInsertionResponse
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPVerificationResponse
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import com.localvalu.deliverylogin.data.model.OTPVerificationResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class OTPVerificationRepository(
    private val remote: OTPVerificationDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : OTPVerificationDataContract.Repository

{
    override val otpVerificationFetchOutcome: PublishSubject<Outcome<DriverOTPVerificationResponse>> =
        PublishSubject.create<Outcome<DriverOTPVerificationResponse>>()

    override val otpInsertionFetchOutcome: PublishSubject<Outcome<DriverOTPInsertionResponse>> =
        PublishSubject.create<Outcome<DriverOTPInsertionResponse>>()

    override fun getRemoteOTPVerification(otpVerificationRequest: OTPVerificationRequest)
    {
        otpVerificationFetchOutcome.loading(true)
        Single.wrap(remote.verifyOTP(otpVerificationRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({otpVerficationResponse ->  otpVerificationFetchOutcome.success(otpVerficationResponse)},
                { error -> handleOTPVerificationError(error) })
            .addTo(compositeDisposable)
    }

    override fun getRemoteOTPInsertion(otpInsertionRequest: OTPInsertionRequest)
    {
        otpInsertionFetchOutcome.loading(true)
        Single.wrap(remote.insertOTP(otpInsertionRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({otpInsertionResponse ->  otpInsertionFetchOutcome.success(otpInsertionResponse)},
                { error -> handleOTPInsertionError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleOTPVerificationError(error: Throwable)
    {
        otpVerificationFetchOutcome.failed(error)
    }

    override fun handleOTPInsertionError(error: Throwable)
    {
        otpVerificationFetchOutcome.failed(error)
    }

}

