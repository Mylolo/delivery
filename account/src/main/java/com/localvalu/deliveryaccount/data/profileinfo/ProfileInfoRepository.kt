package com.localvalu.deliveryaccount.data.profileinfo


import com.localvalu.deliveryaccount.data.model.request.ProfileRequest
import com.localvalu.deliveryaccount.data.model.response.profileresponse.DriverProfileResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class ProfileInfoRepository(
    private val remote: ProfileInfoDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : ProfileInfoDataContract.Repository

{
    override val profileInfoFetchOutcome: PublishSubject<Outcome<DriverProfileResponse>> =
        PublishSubject.create<Outcome<DriverProfileResponse>>()

    override fun fetchProfileInfo(profileRequest: ProfileRequest)
    {
        profileInfoFetchOutcome.loading(true)
        Single.wrap(remote.getRemoteProfileData(profileRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({profileResponse ->  profileInfoFetchOutcome.success(profileResponse)},
                { error -> handleProfileFetchError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleProfileFetchError(error: Throwable)
    {
        profileInfoFetchOutcome.failed(error)
    }

}

