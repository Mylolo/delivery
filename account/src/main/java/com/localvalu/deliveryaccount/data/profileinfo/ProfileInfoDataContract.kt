package com.localvalu.deliveryaccount.data.profileinfo

import com.localvalu.deliveryaccount.data.model.request.ProfileRequest
import com.localvalu.deliveryaccount.data.model.response.profileresponse.DriverProfileResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import com.localvalu.deliverycore.networkhandler.Outcome
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject


interface ProfileInfoDataContract
{
    interface Repository
    {
        val profileInfoFetchOutcome: PublishSubject<Outcome<DriverProfileResponse>>
        fun fetchProfileInfo(profileRequest: ProfileRequest)
        fun handleProfileFetchError(error: Throwable)
    }

    interface Remote
    {
        fun getRemoteProfileData(profileRequest: ProfileRequest): Single<DriverProfileResponse>
    }

}

