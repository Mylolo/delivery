package com.localvalu.deliveryaccount.data.mobilenumbervalidation

import com.localvalu.deliveryaccount.data.model.request.MobileNumberValidationRequest
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.DriverMobileNumberValidationResponse
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface MobileNumberValidationService
{
    @POST("DriverMobilenovalidationTest")
    fun validateMobileNumber(@Body mobileNumberValidationRequest: MobileNumberValidationRequest): Single<DriverMobileNumberValidationResponse>

    @POST("SmsNotification/sendSms")
    fun sendTheSMS(@Body mobileNumberValidationRequest: MobileNumberValidationRequest): Single<SendSMSResponse>
}