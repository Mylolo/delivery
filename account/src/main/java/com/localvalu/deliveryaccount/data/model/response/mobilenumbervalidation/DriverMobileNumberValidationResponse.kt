package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import java.io.Serializable

/**
 * Data class that captures mobile number validation response and process it
 */
data class DriverMobileNumberValidationResponse(@SerializedName("DriverAppResult")
                                          val mobileNumberValidationResponse: MobileNumberValidationResponse
):Serializable
