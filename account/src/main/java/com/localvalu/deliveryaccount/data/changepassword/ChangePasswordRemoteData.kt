package com.localvalu.deliveryaccount.data.changepassword

import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest

class ChangePasswordRemoteData(private val changePasswordService: ChangePasswordService) : ChangePasswordDataContract.Remote

{
    override fun changePassword(changePasswordRequest: ChangePasswordRequest) = changePasswordService.changeThePassword(changePasswordRequest)
}