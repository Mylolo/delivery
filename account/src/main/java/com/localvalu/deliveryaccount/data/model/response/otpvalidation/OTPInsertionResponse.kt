package com.localvalu.deliverylogin.data.model

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPInsertionResult
import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class OTPInsertionResponse(@SerializedName("OTPinsertionResult")
                                          val otpInsertionResult: OTPInsertionResult
):Serializable
