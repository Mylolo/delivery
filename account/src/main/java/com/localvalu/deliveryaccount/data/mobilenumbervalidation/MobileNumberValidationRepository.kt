package com.localvalu.deliveryaccount.data.mobilenumbervalidation

import com.localvalu.deliveryaccount.data.model.request.MobileNumberValidationRequest
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.DriverMobileNumberValidationResponse
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class MobileNumberValidationRepository(
    private val remote: MobileNumberValidationDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : MobileNumberValidationDataContract.Repository

{

    override val mobileNumberValidationFetchOutcome: PublishSubject<Outcome<DriverMobileNumberValidationResponse>> =
        PublishSubject.create<Outcome<DriverMobileNumberValidationResponse>>()

    override fun mobileNumberValidation(mobileNumberValidationRequest: MobileNumberValidationRequest)
    {
        mobileNumberValidationFetchOutcome.loading(true)
        Single.wrap(remote.remoteMobileNumberValidation(mobileNumberValidationRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({driverMobileNumberValidationResponse ->  mobileNumberValidationFetchOutcome.success(driverMobileNumberValidationResponse)},
                { error -> handleMobileNumberValidationError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleMobileNumberValidationError(error: Throwable)
    {
        mobileNumberValidationFetchOutcome.failed(error)
    }

    override val sendSMSFetchOutcome: PublishSubject<Outcome<SendSMSResponse>> =
        PublishSubject.create<Outcome<SendSMSResponse>>()

    override fun sendSMS(mobileNumberValidationRequest: MobileNumberValidationRequest)
    {
        sendSMSFetchOutcome.loading(true)
        Single.wrap(remote.remoteSendSMS(mobileNumberValidationRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({sendSMSResponse ->  sendSMSFetchOutcome.success(sendSMSResponse)},
                { error -> handleSendSMSError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleSendSMSError(error: Throwable)
    {
        sendSMSFetchOutcome.failed(error)
    }

}