package com.localvalu.deliveryaccount.data.changepassword

import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest
import com.localvalu.deliveryaccount.data.model.response.changepassword.DriverChangePasswordResponse
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface ChangePasswordService
{
    @POST("DriverPasswordchange")
    fun changeThePassword(@Body changePasswordRequest: ChangePasswordRequest): Single<DriverChangePasswordResponse>
}