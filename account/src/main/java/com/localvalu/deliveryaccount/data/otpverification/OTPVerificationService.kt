package com.localvalu.deliveryaccount.data.otpverification

import com.localvalu.deliveryaccount.data.model.request.OTPInsertionRequest
import com.localvalu.deliveryaccount.data.model.request.OTPVerificationRequest
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPInsertionResponse
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPVerificationResponse
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import com.localvalu.deliverylogin.data.model.OTPVerificationResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface OTPVerificationService
{
    @POST("OTPVerification")
    fun verifyTheOTP(@Body otpValidationRequest: OTPVerificationRequest): Single<DriverOTPVerificationResponse>

    @POST("OTPinsertion")
    fun insertTheOTP(@Body otpInsertionRequest: OTPInsertionRequest): Single<DriverOTPInsertionResponse>
}