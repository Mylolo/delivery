package com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation

import com.google.gson.annotations.SerializedName
import com.localvalu.deliverycore.base.model.ErrorDetails

data class SendSMSResult(@SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
                         @SerializedName("OTPStatus") val otpStatus: OTPStatus,
                         @SerializedName("status")  val status: String)
{

}