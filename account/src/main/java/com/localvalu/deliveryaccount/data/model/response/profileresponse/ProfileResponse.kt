package com.localvalu.deliveryaccount.data.model.response.profileresponse

import com.google.gson.annotations.SerializedName
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.OTPInsertionResult
import java.io.Serializable

data class ProfileResponse(@SerializedName("GetDriverprofileResult")
                                val profileResult: ProfileResult
): Serializable