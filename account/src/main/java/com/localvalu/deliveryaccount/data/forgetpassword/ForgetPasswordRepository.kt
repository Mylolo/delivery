package com.localvalu.deliveryaccount.data.forgetpassword

import com.localvalu.deliveryaccount.data.model.request.ForgetPasswordRequest
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.data.model.ForgetPasswordResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class ForgetPasswordRepository(
    private val remote: ForgetPasswordDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : ForgetPasswordDataContract.Repository

{

    override val forgetPasswordFetchOutcome: PublishSubject<Outcome<ForgetPasswordResponse>> =
        PublishSubject.create<Outcome<ForgetPasswordResponse>>()

    override fun getRemoteForgetPassword(forgetPasswordRequest: ForgetPasswordRequest)
    {
        forgetPasswordFetchOutcome.loading(true)
        Single.wrap(remote.forgetPassword(forgetPasswordRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({forgetPasswordResponse ->  forgetPasswordFetchOutcome.success(forgetPasswordResponse)},
                { error -> handleForgetPasswordError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleForgetPasswordError(error: Throwable)
    {
        forgetPasswordFetchOutcome.failed(error)
    }

}

