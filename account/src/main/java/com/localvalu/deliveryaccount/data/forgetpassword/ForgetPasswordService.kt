package com.localvalu.deliveryaccount.data.forgetpassword

import com.localvalu.deliveryaccount.data.model.request.ForgetPasswordRequest
import com.localvalu.deliverylogin.data.model.ForgetPasswordResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface ForgetPasswordService
{
    @POST("DriverForgotPassword")
    fun requestForgetPassword(@Body forgetPasswordRequest: ForgetPasswordRequest): Single<ForgetPasswordResponse>
}