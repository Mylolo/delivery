package com.localvalu.deliveryaccount.data.changepassword

import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest
import com.localvalu.deliveryaccount.data.model.response.changepassword.DriverChangePasswordResponse
import com.localvalu.deliverycore.extensions.*
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

import io.reactivex.subjects.PublishSubject


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class ChangePasswordRepository(
    private val remote: ChangePasswordDataContract.Remote,
    private val scheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable) : ChangePasswordDataContract.Repository

{
    override val changePasswordFetchOutcome: PublishSubject<Outcome<DriverChangePasswordResponse>> =
        PublishSubject.create<Outcome<DriverChangePasswordResponse>>()

    override fun getRemoteChangePassword(changePasswordRequest: ChangePasswordRequest)
    {
        changePasswordFetchOutcome.loading(true)
        Single.wrap(remote.changePassword(changePasswordRequest)).performOnBackOutOnMain(scheduler)
            .subscribe({driverChangePasswordResponse ->  changePasswordFetchOutcome.success(driverChangePasswordResponse)},
                { error -> handleChangePasswordError(error) })
            .addTo(compositeDisposable)
    }

    override fun handleChangePasswordError(error: Throwable)
    {
        changePasswordFetchOutcome.failed(error)
    }

}

