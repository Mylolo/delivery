package com.localvalu.deliveryaccount.di

import com.localvalu.deliverycore.application.CoreApp
import javax.inject.Singleton


@Singleton
object AccountDH
{
    private var accountComponent: AccountComponent? = null


    fun accountComponent(): AccountComponent {
        if (accountComponent == null)
           accountComponent = DaggerAccountComponent.builder().coreComponent(CoreApp.coreComponent).build()
        return accountComponent as AccountComponent
    }

    fun destroyAccountComponent() {
        accountComponent = null
    }


}