package com.localvalu.deliveryaccount.di

import com.localvalu.deliveryaccount.data.profileinfo.ProfileInfoDataContract
import com.localvalu.deliveryaccount.data.profileinfo.ProfileInfoRepository
import com.localvalu.deliveryaccount.data.profileinfo.ProfileInfoService
import com.localvalu.deliveryaccount.data.profileinfo.ProfileRemoteData
import com.localvalu.deliveryaccount.ui.profile.ProfileViewModelFactory
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ProfileModule
{

    /*ViewModel*/
    @Provides
    @AccountScope
    fun profileViewModelFactory(repository: ProfileInfoDataContract.Repository,
                                        compositeDisposable: CompositeDisposable):
            ProfileViewModelFactory = ProfileViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @AccountScope
    fun profileInfoRepo(remote: ProfileInfoDataContract.Remote,
                            scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            ProfileInfoDataContract.Repository = ProfileInfoRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AccountScope
    fun profileInfoRemoteData(profileInfoService: ProfileInfoService):
            ProfileInfoDataContract.Remote = ProfileRemoteData(profileInfoService)

    @Provides
    @AccountScope
    fun compositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @AccountScope
    fun profileInfoService(@Named("DotNetServer")retrofit: Retrofit): ProfileInfoService = retrofit.create(ProfileInfoService::class.java)

}