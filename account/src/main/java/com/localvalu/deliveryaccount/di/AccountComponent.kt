package com.localvalu.deliveryaccount.di

import android.content.SharedPreferences
import com.google.gson.Gson
import com.localvalu.deliveryaccount.ui.changepassword.ChangePasswordFragment
import com.localvalu.deliveryaccount.ui.forgetpassword.ForgetPasswordFragment
import com.localvalu.deliveryaccount.ui.otpverification.OTPFragment
import com.localvalu.deliveryaccount.ui.profile.ProfileFragment
import com.localvalu.deliverycore.di.CoreComponent
import com.localvalu.deliverycore.networkhandler.Scheduler
import com.squareup.picasso.Picasso
import dagger.Component
import dagger.Module

@AccountScope
@Component(dependencies = [CoreComponent::class],modules = [AccountModule::class,
    OTPVerificationModule::class,ForgetPasswordModule::class,ProfileModule::class,
    OTPVerificationModule::class,ChangePasswordModule::class,MobileNumberValidationModule::class])
interface AccountComponent
{
    //Expose to dependent components
    fun sharedPreferences(): SharedPreferences
    fun picasso(): Picasso
    fun scheduler(): Scheduler
    fun gson():Gson
    fun inject(profileFragment: ProfileFragment)
    fun inject(forgetPasswordFragment: ForgetPasswordFragment)
    fun inject(otpFragment: OTPFragment)
    fun inject(changePasswordFragment: ChangePasswordFragment)
}

@Module
class AccountModule
{


}