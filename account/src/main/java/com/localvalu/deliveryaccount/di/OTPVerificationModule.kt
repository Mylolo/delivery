package com.localvalu.deliveryaccount.di

import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationDataContract
import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationRemoteData
import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationRepository
import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationService
import com.localvalu.deliveryaccount.ui.otpverification.OTPVerificationViewModelFactory
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class OTPVerificationModule
{
    /*ViewModel*/
    @Provides
    @AccountScope
    fun otpVerificationViewModelFactory(repository: OTPVerificationDataContract.Repository,
                                        compositeDisposable: CompositeDisposable):
            OTPVerificationViewModelFactory = OTPVerificationViewModelFactory(repository,compositeDisposable)


    /*Repository*/
    @Provides
    @AccountScope
    fun otpVerificationRepo(remote: OTPVerificationDataContract.Remote,
                            scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            OTPVerificationDataContract.Repository = OTPVerificationRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AccountScope
    fun otpVerificationRemoteData(otpVerificationService: OTPVerificationService):
            OTPVerificationDataContract.Remote = OTPVerificationRemoteData(otpVerificationService)

    @Provides
    @AccountScope
    fun otpVerificationService(@Named("DotNetServer")retrofit: Retrofit): OTPVerificationService = retrofit.create(
        OTPVerificationService::class.java)
}