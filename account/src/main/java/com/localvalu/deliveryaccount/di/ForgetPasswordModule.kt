package com.localvalu.deliveryaccount.di

import com.localvalu.deliveryaccount.data.*
import com.localvalu.deliveryaccount.data.forgetpassword.ForgetPasswordDataContract
import com.localvalu.deliveryaccount.data.forgetpassword.ForgetPasswordRemoteData
import com.localvalu.deliveryaccount.data.forgetpassword.ForgetPasswordRepository
import com.localvalu.deliveryaccount.data.forgetpassword.ForgetPasswordService
import com.localvalu.deliveryaccount.ui.forgetpassword.ForgetPasswordViewModelFactory
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ForgetPasswordModule
{
    /*ViewModel*/
    @Provides
    @AccountScope
    fun forgetPasswordViewModelFactory(repository: ForgetPasswordDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            ForgetPasswordViewModelFactory = ForgetPasswordViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @AccountScope
    fun forgetPasswordRepo(remote: ForgetPasswordDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            ForgetPasswordDataContract.Repository = ForgetPasswordRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AccountScope
    fun forgetPasswordRemoteData(forgetPasswordService: ForgetPasswordService):
            ForgetPasswordDataContract.Remote = ForgetPasswordRemoteData(forgetPasswordService)

    @Provides
    @AccountScope
    fun forgetPasswordService(@Named("DotNetServer")retrofit: Retrofit): ForgetPasswordService = retrofit.create(ForgetPasswordService::class.java)
}