package com.localvalu.deliveryaccount.di

import com.localvalu.deliveryaccount.data.mobilenumbervalidation.*
import com.localvalu.deliveryaccount.ui.mobilenumbervalidation.MobileNumberValidationViewModelFactory
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class MobileNumberValidationModule
{
    /*ViewModel*/
    @Provides
    @AccountScope
    fun mobileNumberValidationViewModelFactory(repository: MobileNumberValidationDataContract.Repository,
                                               compositeDisposable: CompositeDisposable):
            MobileNumberValidationViewModelFactory = MobileNumberValidationViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @AccountScope
    fun mobileNumberValidationRepo(remote: MobileNumberValidationDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            MobileNumberValidationDataContract.Repository = MobileNumberValidationRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AccountScope
    fun mobileNumberValidationRemoteData(mobileNumberValidationService: MobileNumberValidationService,
    sendSMSService: SendSMSService):
            MobileNumberValidationDataContract.Remote = MobileNumberValidationRemoteData(mobileNumberValidationService,sendSMSService)

    @Provides
    @AccountScope
    fun mobileNumberValidationService(@Named("DotNetServer")retrofit: Retrofit): MobileNumberValidationService = retrofit.create(
        MobileNumberValidationService::class.java)

    @Provides
    @AccountScope
    fun sendSMSService(@Named("PhpServer")retrofit: Retrofit): SendSMSService = retrofit.create(
        SendSMSService::class.java)

}