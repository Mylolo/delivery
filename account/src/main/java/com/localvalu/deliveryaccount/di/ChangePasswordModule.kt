package com.localvalu.deliveryaccount.di

import com.localvalu.deliveryaccount.data.changepassword.ChangePasswordDataContract
import com.localvalu.deliveryaccount.data.changepassword.ChangePasswordRemoteData
import com.localvalu.deliveryaccount.data.changepassword.ChangePasswordRepository
import com.localvalu.deliveryaccount.data.changepassword.ChangePasswordService
import com.localvalu.deliveryaccount.ui.changepassword.ChangePasswordViewModelFactory
import com.localvalu.deliverycore.networkhandler.Scheduler
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ChangePasswordModule
{
    /*ViewModel*/
    @Provides
    @AccountScope
    fun changePasswordViewModelFactory(repository: ChangePasswordDataContract.Repository,
                                       compositeDisposable: CompositeDisposable):
            ChangePasswordViewModelFactory = ChangePasswordViewModelFactory(repository,compositeDisposable)

    /*Repository*/
    @Provides
    @AccountScope
    fun changePasswordRepo(remote: ChangePasswordDataContract.Remote,
                                   scheduler: Scheduler, compositeDisposable: CompositeDisposable
    ):
            ChangePasswordDataContract.Repository = ChangePasswordRepository( remote, scheduler, compositeDisposable)

    @Provides
    @AccountScope
    fun changePasswordRemoteData(changePasswordService: ChangePasswordService):
            ChangePasswordDataContract.Remote = ChangePasswordRemoteData(changePasswordService)

    @Provides
    @AccountScope
    fun changePasswordService(@Named("DotNetServer")retrofit: Retrofit): ChangePasswordService = retrofit.create(ChangePasswordService::class.java)
}