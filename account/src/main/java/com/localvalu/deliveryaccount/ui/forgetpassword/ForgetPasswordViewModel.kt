package com.localvalu.deliveryaccount.ui.forgetpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryaccount.data.forgetpassword.ForgetPasswordDataContract
import com.localvalu.deliveryaccount.data.model.request.ForgetPasswordRequest
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.ValidationUtils
import com.localvalu.deliverylogin.data.model.ForgetPasswordResponse
import io.reactivex.disposables.CompositeDisposable

class ForgetPasswordViewModel(
    private val forgetPasswordRepository: ForgetPasswordDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    val forgetPasswordOutcome: LiveData<Outcome<ForgetPasswordResponse>> by lazy {
        //Convert publish subject to livedata
        forgetPasswordRepository.forgetPasswordFetchOutcome.toLiveData(compositeDisposable)
    }

    // Create a LiveData with a String
    var validMobileNumber: MutableLiveData<Boolean> = MutableLiveData<Boolean>()


    fun forgetPassword(mobileNumber: String, tempPassword: String, fcmId: String)
    {
        forgetPasswordRepository.getRemoteForgetPassword(
            ForgetPasswordRequest(
                "",
                mobileNumber,
                tempPassword
            )
        )
    }

    fun isClientValidMobileNumber(mobileNumber: String)
    {
        val validationUtils:ValidationUtils = ValidationUtils()
        validMobileNumber.value = validationUtils.isValidMobileNo(mobileNumber)
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        AccountDH.destroyAccountComponent()
    }
}