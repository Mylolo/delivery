package com.localvalu.deliveryaccount.ui.mobilenumbervalidation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryaccount.data.mobilenumbervalidation.MobileNumberValidationDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ForgetPasswordViewModel.
 * Required given ForgetPasswordViewModel has a non-empty constructor
 */
class MobileNumberValidationViewModelFactory(
    private val repository: MobileNumberValidationDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return MobileNumberValidationViewModel(repository, compositeDisposable) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
