package com.localvalu.deliveryaccount.ui.otpverification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryaccount.data.model.request.OTPVerificationRequest
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPInsertionResponse
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPVerificationResponse
import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationDataContract
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import com.localvalu.deliverylogin.data.model.OTPVerificationResponse
import io.reactivex.disposables.CompositeDisposable

class OTPVerificationViewModel(
    private val otpVerificationRepository: OTPVerificationDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    val otpVerificationOutcome: LiveData<Outcome<DriverOTPVerificationResponse>> by lazy {
        //Convert publish subject to livedata
        otpVerificationRepository.otpVerificationFetchOutcome.toLiveData(compositeDisposable)
    }

    val otpInsertionOutcome: LiveData<Outcome<DriverOTPInsertionResponse>> by lazy {
        //Convert publish subject to livedata
        otpVerificationRepository.otpInsertionFetchOutcome.toLiveData(compositeDisposable)
    }

    // Create a LiveData with a String
    var validOTP: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    fun otpVerification(mobileNumber: String,otp:String)
    {
        otpVerificationRepository.getRemoteOTPVerification(OTPVerificationRequest("",mobileNumber,otp))
    }

    fun otpInsertion(mobileNumber: String,otp:String)
    {
        otpVerificationRepository.getRemoteOTPVerification(OTPVerificationRequest("",mobileNumber,otp))
    }

    fun isClientOTPVerification(otpText:String)
    {
        validOTP.value=otpText.isEmpty()
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        AccountDH.destroyAccountComponent()
    }
}