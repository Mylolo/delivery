package com.localvalu.deliveryaccount.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryaccount.data.model.request.ProfileRequest
import com.localvalu.deliveryaccount.data.model.response.profileresponse.DriverProfileResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import com.localvalu.deliveryaccount.data.profileinfo.ProfileInfoDataContract
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import io.reactivex.disposables.CompositeDisposable

class ProfileViewModel(
    private val profileInfoRepository: ProfileInfoDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    val profileOutcome: LiveData<Outcome<DriverProfileResponse>> by lazy {
        //Convert publish subject to livedata
        profileInfoRepository.profileInfoFetchOutcome.toLiveData(compositeDisposable)
    }

    fun getProfile(driverId:String)
    {
        profileInfoRepository.fetchProfileInfo(ProfileRequest("",driverId))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        AccountDH.destroyAccountComponent()
    }
}