package com.localvalu.deliveryaccount.ui.mobilenumbervalidation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryaccount.data.mobilenumbervalidation.MobileNumberValidationDataContract
import com.localvalu.deliveryaccount.data.model.request.MobileNumberValidationRequest
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.DriverMobileNumberValidationResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.ValidationUtils
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResponse
import io.reactivex.disposables.CompositeDisposable

class MobileNumberValidationViewModel(
    private val mobileNumberValidationRepository: MobileNumberValidationDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    // Create a LiveData with a String
    var validMobileNumber: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    val mobileNumberValidationOutcome: LiveData<Outcome<DriverMobileNumberValidationResponse>> by lazy {
        //Convert publish subject to livedata
        mobileNumberValidationRepository.mobileNumberValidationFetchOutcome.toLiveData(compositeDisposable)
    }

    val sendSMSOutcome: LiveData<Outcome<SendSMSResponse>> by lazy {
        //Convert publish subject to livedata
        mobileNumberValidationRepository.sendSMSFetchOutcome.toLiveData(compositeDisposable)
    }

    fun isClientValidMobileNumber(mobileNumber: String)
    {
        val validationUtils: ValidationUtils = ValidationUtils()
        validMobileNumber.value = validationUtils.isValidMobileNo(mobileNumber)
    }

    fun mobileNumberValidation(mobileNumber: String)
    {
        mobileNumberValidationRepository.mobileNumberValidation(MobileNumberValidationRequest("",mobileNumber))
    }

    fun sendSMS(mobileNumber: String)
    {
        mobileNumberValidationRepository.sendSMS(MobileNumberValidationRequest("",mobileNumber))
    }

    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        AccountDH.destroyAccountComponent()
    }
}