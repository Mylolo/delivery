package com.localvalu.deliveryaccount.ui.otpverification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryaccount.data.otpverification.OTPVerificationDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ForgetPasswordViewModel.
 * Required given ForgetPasswordViewModel has a non-empty constructor
 */
class OTPVerificationViewModelFactory(
    private val repository: OTPVerificationDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return OTPVerificationViewModel(repository, compositeDisposable) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
