package com.localvalu.deliveryaccount.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.localvalu.deliveryaccount.R
import com.localvalu.deliveryaccount.ui.profile.ProfileFragment

class AccountActivity : AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        addProfileFragment()
    }

    fun addProfileFragment()
    {
        supportFragmentManager.beginTransaction().add(R.id.flContainer,ProfileFragment(),"ProfileFragment").commit()
    }
}
