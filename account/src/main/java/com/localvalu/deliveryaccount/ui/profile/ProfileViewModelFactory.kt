package com.localvalu.deliveryaccount.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.localvalu.deliveryaccount.data.profileinfo.ProfileInfoDataContract
import io.reactivex.disposables.CompositeDisposable

/**
 * ViewModel provider factory to instantiate ProfileViewModel.
 * Required given ProfileViewModel has a non-empty constructor
 */
class ProfileViewModelFactory(
    private val repository: ProfileInfoDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory
{

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T
    {
        //if (modelClass.isAssignableFrom(LoginViewModel::class.java))
        //{
        return ProfileViewModel(repository, compositeDisposable) as T
        //}
        //throw IllegalArgumentException("Unknown ViewModel class")
    }

}
