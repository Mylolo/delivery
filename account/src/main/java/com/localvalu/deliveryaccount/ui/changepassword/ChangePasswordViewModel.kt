package com.localvalu.deliveryaccount.ui.changepassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.localvalu.deliveryaccount.data.changepassword.ChangePasswordDataContract
import com.localvalu.deliveryaccount.data.model.PasswordValidation
import com.localvalu.deliveryaccount.data.model.request.ChangePasswordRequest
import com.localvalu.deliveryaccount.data.model.response.changepassword.DriverChangePasswordResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.extensions.toLiveData
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.ValidationUtils
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import io.reactivex.disposables.CompositeDisposable

class ChangePasswordViewModel(
    private val changePasswordRepository: ChangePasswordDataContract.Repository,
    private val compositeDisposable: CompositeDisposable
) : ViewModel()
{
    val changePasswordOutcome: LiveData<Outcome<DriverChangePasswordResponse>> by lazy {
        //Convert publish subject to livedata
        changePasswordRepository.changePasswordFetchOutcome.toLiveData(compositeDisposable)
    }

    // Create a LiveData with a boolean
    var validPassword: MutableLiveData<PasswordValidation> = MutableLiveData<PasswordValidation>()
    var validConfirmPassword: MutableLiveData<PasswordValidation> = MutableLiveData<PasswordValidation>()

    fun isValidPassword(textBoxNumber:Int,password: String)
    {
        val validationUtils: ValidationUtils = ValidationUtils()
        when(textBoxNumber)
        {
            0 ->  validPassword.value = PasswordValidation(validationUtils.checkPasswordRegex(password),0)
            1 ->  validConfirmPassword.value = PasswordValidation( validationUtils.checkPasswordRegex(password),0)
        }
    }

    fun passwordMatch(password:String,confirmPassword: String)
    {
        val validationUtils: ValidationUtils = ValidationUtils()
        validConfirmPassword.value = PasswordValidation(validationUtils.isMatchingPassword(password,confirmPassword),1);
    }

    fun changePassword(mobileNumber:String,password:String)
    {
        changePasswordRepository.getRemoteChangePassword(ChangePasswordRequest("",mobileNumber,password))
    }


    override fun onCleared()
    {
        super.onCleared()
        //clear the disposables when the viewmodel is cleared
        compositeDisposable.clear()
        AccountDH.destroyAccountComponent()
    }
}