package com.localvalu.deliveryaccount.ui.forgetpassword

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.localvalu.deliveryaccount.R
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.DriverMobileNumberValidationResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliveryaccount.ui.mobilenumbervalidation.MobileNumberValidationViewModel
import com.localvalu.deliveryaccount.ui.mobilenumbervalidation.MobileNumberValidationViewModelFactory
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverylogin.data.model.MobileNumberValidationResponse
import com.localvalu.deliveryaccount.data.model.response.mobilenumbervalidation.SendSMSResponse
import kotlinx.android.synthetic.main.fragment_forget_password.*
import kotlinx.android.synthetic.main.fragment_forget_password.etMobileNumber
import kotlinx.android.synthetic.main.fragment_forget_password.tilMobileNumber
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.IOException
import javax.inject.Inject

class ForgetPasswordFragment : Fragment()
{
    private val TAG = "ForgetPassword"

    private val component by lazy { AccountDH.accountComponent() }

    @Inject
    lateinit var mobileNumberValidationFactory: MobileNumberValidationViewModelFactory

    private val mobileNumberValidationViewModel: MobileNumberValidationViewModel by lazy {
        ViewModelProvider(this, mobileNumberValidationFactory).get(MobileNumberValidationViewModel::class.java)
    }

    private var validInputs=false;

    private var oneTimeVisit=false;

    private var strMobileNumber="";

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_forget_password,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
        initMobileNumberClientValidateListener()
        etMobileNumber.doAfterTextChanged { mobileNumberValidationViewModel.isClientValidMobileNumber(etMobileNumber.text.toString()) }
    }

    private fun setProperties()
    {
        val fontType = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular)
        etMobileNumber.typeface = fontType;
        tilMobileNumber.typeface = fontType
        btnSendForgetPassword.setOnClickListener { v->
            oneTimeVisit=false
            initMobileNumberValidateListener();
            strMobileNumber = etMobileNumber.text.toString().trim();
            if(strMobileNumber.first()=='+')
            {
                strMobileNumber = strMobileNumber.substring(1,strMobileNumber.length);
            }
            /*else if(strMobileNumber.first()== '0')
            {
                val strTempNumber:StringBuilder = StringBuilder();
                strTempNumber.append("44").append(strMobileNumber.substring(1,strMobileNumber.length))
                strMobileNumber=strTempNumber.toString()
            }*/
            mobileNumberValidationViewModel.mobileNumberValidation(strMobileNumber)
        }
    }

    private  fun initMobileNumberClientValidateListener()
    {
        mobileNumberValidationViewModel.validMobileNumber?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult)
            {
                tilMobileNumber.error = null
                tilMobileNumber.isErrorEnabled = false
                validInputs=true
            }
            else
            {
                tilMobileNumber.isErrorEnabled = true;
                tilMobileNumber.error = getString(R.string.lbl_invalid_mobile_number);
                validInputs=false
            }
            disableEnableSendButton()
        })
    }

    private fun initMobileNumberValidateListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        mobileNumberValidationViewModel.mobileNumberValidationOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<DriverMobileNumberValidationResponse>> { outcome ->
                Log.d(TAG, "initMobileNumberValidateListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            cnlForgetPasswordContent.visibility = View.GONE
                            progressBarForgetPassword.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initiateLoginDataListener: Successfully loaded data")
                        progressBarForgetPassword.visibility = View.GONE
                        if (outcome.data.mobileNumberValidationResponse.driverMobileNumberValidationResult.errorDetails.errorCode == 0)
                        {
                            initSendSMSListener()
                            mobileNumberValidationViewModel.sendSMS(strMobileNumber)
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.mobileNumberValidationResponse.driverMobileNumberValidationResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            cnlForgetPasswordContent.visibility = View.VISIBLE
                            //gotoOTPFragment("+44 77839592211")
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarForgetPassword.visibility = View.GONE
                            cnlForgetPasswordContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        else
                        {
                            progressBarProfile.visibility = View.GONE
                            cnlProfileContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_unknown_error_try_again),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            })
    }

    private fun initSendSMSListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        mobileNumberValidationViewModel.sendSMSOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<SendSMSResponse>> { outcome ->
                Log.d(TAG, "initSendSMSListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            cnlForgetPasswordContent.visibility = View.GONE
                            progressBarForgetPassword.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initiateLoginDataListener: Successfully loaded data")
                        progressBarForgetPassword.visibility = View.GONE
                        if (outcome.data.sendSMSResult.errorDetails.errorCode == 0)
                        {
                            gotoOTPFragment(etMobileNumber.text.toString())
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.sendSMSResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            cnlForgetPasswordContent.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarForgetPassword.visibility = View.GONE
                            cnlForgetPasswordContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        else
                        {
                            progressBarProfile.visibility = View.GONE
                            cnlProfileContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_unknown_error_try_again),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            })
    }

    private fun disableEnableSendButton()
    {
        btnSendForgetPassword.isEnabled = validInputs
    }

    private fun gotoOTPFragment(mobileNumber:String)
    {
        Log.d(TAG,"gotoOTPFragment")

        oneTimeVisit=true;
        val navGraphId = resources.getIdentifier("nav_graph","navigation","com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier("action_ForgetPassword_to_actionOTPFragment","id","com.localvalu.deliveryapp")
        var bundle = bundleOf(BundleUtils.BUNDLE_MOBILE_NUMBER to mobileNumber)
        if(navGraphId!=0)
        {
            findNavController().navigate(actionId,bundle)
        }

    }


}