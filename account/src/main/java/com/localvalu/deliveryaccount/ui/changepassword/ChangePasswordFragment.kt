package com.localvalu.deliveryaccount.ui.changepassword

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.localvalu.deliveryaccount.R
import com.localvalu.deliveryaccount.data.model.response.changepassword.DriverChangePasswordResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliveryaccount.ui.otpverification.OTPFragment
import com.localvalu.deliveryaccount.ui.otpverification.OTPVerificationViewModel
import com.localvalu.deliveryaccount.ui.otpverification.OTPVerificationViewModelFactory
import com.localvalu.deliverycore.base.MainHandler
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverylogin.data.model.ChangePasswordResponse
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.fragment_otp_verification.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.IOException
import java.lang.StringBuilder
import javax.inject.Inject


class ChangePasswordFragment : Fragment()
{
    private val TAG = "ChangePassword"

    private val component by lazy { AccountDH.accountComponent() }

    @Inject
    lateinit var changePasswordViewModelFactory: ChangePasswordViewModelFactory

    private val changePasswordViewModel: ChangePasswordViewModel by lazy {
        ViewModelProvider(this, changePasswordViewModelFactory).get(ChangePasswordViewModel::class.java)
    }

    private var validInputs=false;

    private lateinit var mobileNumber:String;

    private lateinit var mainHandler: MainHandler;

    companion object
    {
        fun newInstance(mobileNumberParam: String): OTPFragment
        {
            val fragment = OTPFragment()
            val args = Bundle()
            args.putString(BundleUtils.BUNDLE_MOBILE_NUMBER, mobileNumberParam)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_change_password,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()

    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        mainHandler = context as MainHandler
    }

    private fun setProperties()
    {
        val fontType = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular)
        etPassword.typeface = fontType;
        etConfirmPassword.typeface = fontType

        applyTextChangeListener(etPassword,0,etPassword.text.toString());
        applyTextChangeListener(etConfirmPassword,1,etConfirmPassword.text.toString());

        initPasswordClientValidateListener()

        btnSubmitChangePassword.setOnClickListener { View.OnClickListener {v->
            initChangePasswordListener()
            changePasswordViewModel.changePassword(mobileNumber,etPassword.text.toString())
        } }

        Log.d(TAG, "arguments ")
        mobileNumber = arguments?.getString(BundleUtils.BUNDLE_MOBILE_NUMBER).toString()
    }

    private fun applyTextChangeListener(textView: TextInputEditText, textBoxNumber:Int, passwordText:String)
    {
        textView.doAfterTextChanged { changePasswordViewModel.
        isValidPassword(textBoxNumber,textView.text.toString())
            if(textView.equals(etConfirmPassword))
                changePasswordViewModel.passwordMatch(etPassword.text.toString(),
                etConfirmPassword.text.toString())
        }
    }

    private fun initPasswordClientValidateListener()
    {

        changePasswordViewModel.validPassword?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult.validPassword)
            {
                tilPassword.error = null
                tilPassword.isErrorEnabled = false
                validInputs=true
            }
            else if(!validResult.validPassword && validResult.messageCode==0)
            {
                tilPassword.isErrorEnabled = true;
                tilPassword.error = getString(R.string.lbl_invalid_password);
                validInputs=false
            }
        })

        changePasswordViewModel.validConfirmPassword?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult.validPassword)
            {
                tilConfirmPassword.error = null
                tilConfirmPassword.isErrorEnabled = false
                validInputs=true
            }
            else if(!validResult.validPassword)
            {
                tilConfirmPassword.isErrorEnabled = true;
                when(validResult.messageCode)
                {
                    0->tilConfirmPassword.error = getString(R.string.lbl_invalid_password);
                    1->tilConfirmPassword.error = getString(R.string.lbl_password_no_match);
                }
                validInputs=false
            }
            disableEnableVerifyButton()
        })
    }

    private fun disableEnableVerifyButton()
    {
        btnSubmitChangePassword.isEnabled = validInputs
    }

    private fun initChangePasswordListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        changePasswordViewModel.changePasswordOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<DriverChangePasswordResponse>> { outcome ->
                Log.d(TAG, "initChangePasswordListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            cnlContentChangePassword.visibility = View.GONE
                            progressBarChangePassword.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initChangePasswordListener: Successfully loaded data")
                        progressBarProfile.visibility = View.GONE
                        if (outcome.data.changePasswordResponse.changePasswordResult.errorDetails.errorCode == 0)
                        {
                            cnlContentChangePassword.visibility = View.VISIBLE
                            progressBarChangePassword.visibility = View.GONE
                            mainHandler.logOut()
                            gotoMainFragment();
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.changePasswordResponse.changePasswordResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            cnlContent.visibility = View.VISIBLE
                            progressBarChangePassword.visibility = View.GONE
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarChangePassword.visibility = View.GONE
                            cnlContentChangePassword.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        else
                        {
                            progressBarChangePassword.visibility = View.GONE
                            cnlContentChangePassword.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_unknown_error_try_again),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            })
    }

    private fun gotoMainFragment()
    {
        Log.d(TAG,"gotoProfileFragment")
        val popUpToFragmentId = resources.getIdentifier("mainFragment","id","com.localvalu.deliveryapp")
        findNavController().popBackStack(popUpToFragmentId, true);
        findNavController().navigate(popUpToFragmentId);
    }

}