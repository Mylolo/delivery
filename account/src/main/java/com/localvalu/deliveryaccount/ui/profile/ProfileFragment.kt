package com.localvalu.deliveryaccount.ui.profile

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.snackbar.Snackbar
import com.localvalu.deliveryaccount.R
import com.localvalu.deliveryaccount.data.model.response.profileresponse.DriverProfileResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverycore.utils.transformation.RoundedTransformation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.toolbar.toolbar
import kotlinx.android.synthetic.main.toolbar_profile.*
import java.io.IOException
import javax.inject.Inject


class ProfileFragment : Fragment()
{
    private val TAG = "ProfileFragment"

    private val component by lazy { AccountDH.accountComponent() }

    @Inject
    lateinit var contextTool: Context

    @Inject
    lateinit var viewModelFactory: ProfileViewModelFactory

    private val viewModel: ProfileViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(ProfileViewModel::class.java)
    }

    private var driverId = "-1"

    companion object
    {
        fun newInstance(driverId: String): ProfileFragment
        {
            val fragment = ProfileFragment()
            val args = Bundle()
            args.putString(BundleUtils.BUNDLE_DRIVER_ID, driverId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this);
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setAppBar()
        Log.d(TAG, "arguments ")
        setProperties();
        driverId = arguments?.getString(BundleUtils.BUNDLE_DRIVER_ID).toString()
        initProfileDataListener();
        if (!driverId.equals("-1")) viewModel.getProfile(driverId);

    }

    private fun setProperties()
    {
        btnRetry.setOnClickListener{
            viewModel.getProfile(driverId);
        }
        //this.component.picasso().load(R.drawable.profile_img).transform(RoundedTransformation(30f,0f)).into(ivProfile);
    }

    private fun setAppBar()
    {
        val navController = findNavController()
        val fragmentId = resources.getIdentifier("driverDashboardFragment","id","com.localvalu.deliveryapp")
        val drawerLayoutID = resources.getIdentifier("drawer_layout","id","com.localvalu.deliveryapp")
        val drawerLayout = activity?.findViewById<DrawerLayout>(drawerLayoutID);
        val appBarConfiguration = AppBarConfiguration(navController.graph,drawerLayout)
        toolbar.setupWithNavController(navController, appBarConfiguration)
        val porterDuffColorFilter = PorterDuffColorFilter(ContextCompat.getColor(requireContext(),R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        toolbar.navigationIcon?.colorFilter = porterDuffColorFilter;
        tvToolBarTitle.text = getString(R.string.lbl_profile)
    }

    private fun initProfileDataListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        viewModel.profileOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<DriverProfileResponse>> { outcome ->
                Log.d(TAG, "initProfileDataListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            showInitialProgress()
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initiateLoginDataListener: Successfully loaded data")

                        if (outcome.data.profileResponse.profileResult.errorDetails.errorCode == 0)
                        {
                            etName.setText(outcome.data.profileResponse.profileResult.name);
                            etEmail.setText(outcome.data.profileResponse.profileResult.email);
                            etMobileNumber.setText(outcome.data.profileResponse.profileResult.mobileNumber);
                            etDOB.setText(outcome.data.profileResponse.profileResult.dob);
                            if (outcome.data.profileResponse.profileResult.equals("Female"))
                                rgGender.check(R.id.rbFemale)
                            else if (outcome.data.profileResponse.profileResult.equals("Male"))
                                rgGender.check(R.id.rbMale)
                            showContent();
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.profileResponse.profileResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            showInitialError(outcome.data.profileResponse.profileResult.errorDetails.errorMessage,false)
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarProfile.visibility = View.GONE
                            cnlProfileContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                            showInitialError(getString(R.string.msg_no_internet_connection),true)
                        }
                        else
                        {
                            progressBarProfile.visibility = View.GONE
                            cnlProfileContent.visibility = View.VISIBLE
                            showInitialError(getString(R.string.msg_no_internet_connection),true)
                        }
                    }

                }
            })
    }

    private fun showContent()
    {
        nestedScrollView.visibility = View.VISIBLE
        progressBarProfile.visibility = View.GONE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialProgress()
    {
        nestedScrollView.visibility = View.GONE
        progressBarProfile.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
    }

    private fun showInitialError(message:String, retry: Boolean)
    {
        nestedScrollView.visibility = View.GONE
        progressBarProfile.visibility = View.VISIBLE
        errorContainer.visibility = View.GONE
        tvError.text = message
        if(retry) btnRetry.visibility = View.VISIBLE
        else btnRetry.visibility=View.GONE
    }

    private fun showErrorMessage(message:String,retry: Boolean,apiResponseFrom:Int)
    {
        val mySnackbar = Snackbar.make(clProfile,
            message, Snackbar.LENGTH_INDEFINITE)
        mySnackbar.setBackgroundTint(ContextCompat.getColor(contextTool,R.color.colorOnPrimary))
        if(retry) mySnackbar.setAction(R.string.lbl_retry, View.OnClickListener {v->
            when(apiResponseFrom)
            {
                1-> if (!driverId.equals("-1")) viewModel.getProfile(driverId);
            }
            mySnackbar.dismiss()
        })
        else mySnackbar.setAction(R.string.lbl_ok, View.OnClickListener {v->
            mySnackbar.dismiss()
        })
        mySnackbar.show()

    }

    override fun onDestroy()
    {
        AccountDH.destroyAccountComponent()
        super.onDestroy()
    }

}