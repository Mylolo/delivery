package com.localvalu.deliveryaccount.ui.otpverification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.localvalu.deliveryaccount.R
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPInsertionResponse
import com.localvalu.deliveryaccount.data.model.response.otpvalidation.DriverOTPVerificationResponse
import com.localvalu.deliveryaccount.data.model.response.profileresponse.ProfileResponse
import com.localvalu.deliveryaccount.di.AccountDH
import com.localvalu.deliveryaccount.ui.profile.ProfileFragment
import com.localvalu.deliverycore.networkhandler.Outcome
import com.localvalu.deliverycore.utils.BundleUtils
import com.localvalu.deliverylogin.data.model.OTPInsertionResponse
import kotlinx.android.synthetic.main.fragment_otp_verification.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.IOException
import java.lang.StringBuilder
import javax.inject.Inject


class OTPFragment : Fragment()
{
    private val TAG = "OTPFragment"

    private val component by lazy { AccountDH.accountComponent() }

    @Inject
    lateinit var otpVerificationViewModelFactory: OTPVerificationViewModelFactory

    private val otpVerificationViewModel: OTPVerificationViewModel by lazy {
        ViewModelProvider(this, otpVerificationViewModelFactory).get(OTPVerificationViewModel::class.java)
    }

    private var validInputs=false;

    private lateinit var strOTP:String;

    private lateinit var mobileNumber:String;

    private var oneTimeVisit=false;

    companion object
    {
        fun newInstance(mobileNumberParam: String): OTPFragment
        {
            val fragment = OTPFragment()
            val args = Bundle()
            args.putString(BundleUtils.BUNDLE_MOBILE_NUMBER, mobileNumberParam)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        return inflater.inflate(R.layout.fragment_otp_verification,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()

    }

    private fun setProperties()
    {
        val fontType = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular)
        etOTP.typeface = fontType;


        etOTP.doAfterTextChanged { otpVerificationViewModel.
        isClientOTPVerification(etOTP.text.toString())
        }

        initOTPClientValidateListener()


        btnVerifyOtp.setOnClickListener {
            strOTP = etOTP.text.toString()
            initOTPInsertionListener()
            initOTPVerificationListener()
            otpVerificationViewModel.otpInsertion(mobileNumber,strOTP.toString())
        }

        Log.d(TAG, "arguments ")
        mobileNumber = arguments?.getString(BundleUtils.BUNDLE_MOBILE_NUMBER).toString()
    }

    private fun initOTPClientValidateListener()
    {
        otpVerificationViewModel.validOTP?.observe(viewLifecycleOwner, Observer { validResult ->
            if(validResult)
            {
                tilOTP.isErrorEnabled = true;
                tilOTP.error = getString(R.string.lbl_invalid_verification_code);
                validInputs=false
            }
            else
            {
                tilOTP.error = null
                tilOTP.isErrorEnabled = false
                validInputs=true
            }

            disableEnableVerifyButton()
        })

    }

    private fun disableEnableVerifyButton()
    {
        btnVerifyOtp.isEnabled = validInputs
    }

    private fun initOTPInsertionListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        otpVerificationViewModel.otpInsertionOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<DriverOTPInsertionResponse>> { outcome ->
                Log.d(TAG, "initOTPInsertionListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            cnlContent.visibility = View.GONE
                            progressBarOTP.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initOTPInsertionListener: Successfully loaded data")
                        progressBarProfile.visibility = View.GONE
                        if (outcome.data.otpInsertionResponse.otpInsertionResult.errorDetails.errorCode == 0)
                        {

                            cnlContent.visibility = View.VISIBLE
                            progressBarOTP.visibility = View.GONE
                            otpVerificationViewModel.otpVerification(mobileNumber,strOTP.toString())
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.otpInsertionResponse.otpInsertionResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            cnlContent.visibility = View.VISIBLE
                            progressBarOTP.visibility = View.GONE
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarOTP.visibility = View.GONE
                            cnlContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        else
                        {
                            progressBarOTP.visibility = View.GONE
                            cnlContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_unknown_error_try_again),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            })
    }

    private fun initOTPVerificationListener()
    {
        //Observe the outcome and update state of the screen  accordingly
        otpVerificationViewModel.otpVerificationOutcome.observe(
            viewLifecycleOwner,
            Observer<Outcome<DriverOTPVerificationResponse>> { outcome ->
                Log.d(TAG, "initOTPInsertionListener: $outcome")
                when (outcome)
                {

                    is Outcome.Progress ->
                    {
                        if (outcome.loading)
                        {
                            cnlContent.visibility = View.GONE
                            progressBarOTP.visibility = View.VISIBLE
                        }
                    }

                    is Outcome.Success ->
                    {
                        Log.d(TAG, "initOTPInsertionListener: Successfully loaded data")
                        progressBarProfile.visibility = View.GONE
                        if (outcome.data.otpVerificationResponse.otpValidationResult.errorDetails.errorCode == 0)
                        {

                            cnlContent.visibility = View.VISIBLE
                            progressBarOTP.visibility = View.GONE
                            gotoChangePasswordFragment()
                        }
                        else
                        {
                            Toast.makeText(
                                context, "" + outcome.data.otpVerificationResponse.otpValidationResult.errorDetails.errorMessage,
                                Toast.LENGTH_LONG
                            ).show()
                            cnlContent.visibility = View.VISIBLE
                            progressBarOTP.visibility = View.GONE
                        }
                    }

                    is Outcome.Failure ->
                    {

                        if (outcome.e is IOException)
                        {
                            progressBarOTP.visibility = View.GONE
                            cnlContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_no_internet_connection),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        else
                        {
                            progressBarOTP.visibility = View.GONE
                            cnlContent.visibility = View.VISIBLE
                            Toast.makeText(
                                context, getString(R.string.msg_unknown_error_try_again),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            })
    }

    private fun gotoChangePasswordFragment()
    {
        Log.d(TAG,"gotoChangePasswordFragment")
        oneTimeVisit=true;
        val navGraphId = resources.getIdentifier("nav_graph","navigation","com.localvalu.deliveryapp")
        val actionId = resources.getIdentifier("action_otpFragment_to_actionChangePasswordFragment","id","com.localvalu.deliveryapp")
        var bundle = bundleOf(BundleUtils.BUNDLE_MOBILE_NUMBER to mobileNumber)
        if(navGraphId!=0)
        {
            findNavController().navigate(actionId,bundle)
        }
    }

}